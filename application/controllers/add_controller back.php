<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class add_controller extends CI_Controller {

	public function __Construct()
	{
	   parent::__Construct ();
	}

	public function admin_add_customer_form()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/add_customer', $data);
	}

	public function admin_add_customer()
	{	
		$id = 'CUST'.date('ymdhis');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['c_picture']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['c_picture']['tmp_name'];
        	$folder="./media/customer/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'c_id' 				=> 'CUST'.date('ymdhis'),
		'c_firstname' 		=> $this->input->post('c_firstname'),
		'c_middlename' 		=> $this->input->post('c_middlename'),
		'c_lastname' 		=> $this->input->post('c_lastname'),
		'c_address' 		=> $this->input->post('c_address'),
		'c_city' 			=> $this->input->post('c_city'),
		'c_region'		 	=> $this->input->post('c_region'),
		'c_province' 		=> $this->input->post('c_province'),
		'c_zip' 			=> $this->input->post('c_zip'),
		'c_gender' 			=> $this->input->post('c_gender'),
		'c_email' 			=> $this->input->post('c_email'),
		'c_contact_no' 		=> $this->input->post('c_contact_no'),
		'c_password' 		=> $this->input->post('c_password'),
		'c_status' 			=> 'Disabled',
		'c_birthdate' 		=> $this->input->post('c_birthdate'),
		'c_picture' 		=> $pic,
		'c_registry_date' 	=> date('Y-m-d'),
		'c_registry_time' 	=> date('h:i:s')
		);
		$this->customers_model->admin_add_customer($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['message'] = 'New Customer Registered';
		$this->load->view('admin/admin', $data);
	}

	public function admin_add_customer_form_sec()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/addSec_customer', $data);
	}

	public function adminSec_add_customer()
	{	
		$id = 'CUST'.date('ymdhis');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['c_picture']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['c_picture']['tmp_name'];
        	$folder="./media/customer/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'c_id' 				=> 'CUST'.date('ymdhis'),
		'c_firstname' 		=> $this->input->post('c_firstname'),
		'c_middlename' 		=> $this->input->post('c_middlename'),
		'c_lastname' 		=> $this->input->post('c_lastname'),
		'c_address' 		=> $this->input->post('c_address'),
		'c_city' 			=> $this->input->post('c_city'),
		'c_region' 			=> $this->input->post('c_region'),
		'c_province' 		=> $this->input->post('c_province'),
		'c_zip' 			=> $this->input->post('c_zip'),
		'c_gender' 			=> $this->input->post('c_gender'),
		'c_email' 			=> $this->input->post('c_email'),
		'c_status' 			=> 'Disabled',
		'c_contact_no' 		=> $this->input->post('c_contact_no'),
		'c_password' 		=> $this->input->post('c_password'),
		'c_birthdate' 		=> $this->input->post('c_birthdate'),
		'c_picture' 		=> $pic,
		'c_registry_date' 	=> date('Y-m-d'),
		'c_registry_time' 	=> date('h:i:s')
		);
		$this->customers_model->admin_add_customer($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_customers'] 	= $this->customers_model->get_customers();
		$data['message'] 		= 'New Customer Registered';
		$this->load->view('admin/customer_sec', $data);
	}

	public function admin_add_employee_form()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_jobpos'] = $this->employees_model->get_jobpos();
		$this->load->view('admin/forms/add_employee', $data);
	}

	public function admin_add_employee()
	{
		$id = 'EMP'.date('ymdhis');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['e_picture']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['e_picture']['tmp_name'];
        	$folder="./media/employee/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'e_id' 				=> 'EMP'.date('ymdhis'),
		'e_firstname' 		=> $this->input->post('e_firstname'),
		'e_middlename' 		=> $this->input->post('e_middlename'),
		'e_lastname' 		=> $this->input->post('e_lastname'),
		'e_address' 		=> $this->input->post('e_address'),
		'e_city' 			=> $this->input->post('e_city'),
		'e_region' 			=> $this->input->post('e_region'),
		'e_province' 		=> $this->input->post('e_province'),
		'e_zip' 			=> $this->input->post('e_zip'),
		'e_gender' 			=> $this->input->post('e_gender'),
		'e_email' 			=> $this->input->post('e_email'),
		'e_contact_no' 		=> $this->input->post('e_contact_no'),
		'e_password' 		=> $this->input->post('e_password'),
		'e_birthdate' 		=> $this->input->post('e_birthdate'),
		'e_picture' 		=> $pic,
		'e_position' 		=> $this->input->post('e_position'),
		'e_registry_date' 	=> date('Y-m-d'),
		'e_registry_time' 	=> date('h:i:s')
		);
		$this->employees_model->admin_add_employee($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['message'] = 'New Employee Registered';
		$this->load->view('admin/admin', $data);
	}

	public function admin_add_employee_form_sec()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_jobpos'] = $this->employees_model->get_jobpos();
		$this->load->view('admin/forms/addSec_employee', $data);
	}

	public function adminSec_add_employee()
	{
		$id = 'EMP'.date('ymdhis');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['e_picture']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['e_picture']['tmp_name'];
        	$folder="./media/employee/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'e_id' 				=> 'EMP'.date('ymdhis'),
		'e_firstname' 		=> $this->input->post('e_firstname'),
		'e_middlename'		=> $this->input->post('e_middlename'),
		'e_lastname'		=> $this->input->post('e_lastname'),
		'e_address' 		=> $this->input->post('e_address'),
		'e_city' 			=> $this->input->post('e_city'),
		'e_region' 			=> $this->input->post('e_region'),
		'e_province' 		=> $this->input->post('e_province'),
		'e_zip' 			=> $this->input->post('e_zip'),
		'e_gender' 			=> $this->input->post('e_gender'),
		'e_email' 			=> $this->input->post('e_email'),
		'e_contact_no' 		=> $this->input->post('e_contact_no'),
		'e_password' 		=> $this->input->post('e_password'),
		'e_birthdate' 		=> $this->input->post('e_birthdate'),
		'e_picture' 		=> $pic,
		'e_position' 		=> $this->input->post('e_position'),
		'e_registry_date' 	=> date('Y-m-d'),
		'e_registry_time' 	=> date('h:i:s')
		);
		$this->employees_model->admin_add_employee($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_employees'] 	= $this->employees_model->get_employees();
		$data['get_jobpos'] 	= $this->employees_model->get_jobpos();
		$data['message'] 		= 'New Employee Registered';
		$this->load->view('admin/employee_sec', $data);
	}

	public function admin_add_jobpos_form()
	{
		$this->load->view('admin/forms/add_jobpos');
	}

	public function admin_jobpos()
	{
		$data = array(
		'jp_id' 			=> 'JPOS'.date('ymdhis'), 
		'jp_name' 			=> $this->input->post('jp_name'),
		'jp_salary' 		=> $this->input->post('jp_salary'),
		'jp_type' 			=> $this->input->post('jp_type'),
		'jp_inputdate' 		=> date('Y-m-d'),
		'jp_inputtime' 		=> date('h:i:s')
		);
		$this->employees_model->admin_add_jobpos($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['message'] = 'New Job Position Added';
		$this->load->view('admin/admin', $data);
	}

	public function admin_add_jobpos_form_sec()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/addSec_jobpos', $data);
	}

	public function adminSec_jobpos()
	{
		$data = array(
		'jp_id' 			=> 'JPOS'.date('ymdhis'), 
		'jp_name' 			=> $this->input->post('jp_name'),
		'jp_type' 			=> $this->input->post('jp_type'),
		'jp_salary' 		=> $this->input->post('jp_salary'),
		'jp_inputdate' 		=> date('Y-m-d'),
		'jp_inputtime' 		=> date('h:i:s')
		);
		$this->employees_model->admin_add_jobpos($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_employees'] 	= $this->employees_model->get_employees();
		$data['get_jobpos'] 	= $this->employees_model->get_jobpos();
		$data['message'] 		= 'New Job Position Added';
		$this->load->view('admin/employee_sec', $data);
	}

	public function admin_add_product_form()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/add_product', $data);
	}

	public function admin_add_product()
	{

				error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('i_serial_no');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['i_img']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['i_img']['tmp_name'];
        	$folder="./media/inventory/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'i_serial_no' 		=> $this->input->post('i_serial_no'),
		'i_product_name' 	=> $this->input->post('i_product_name'),
		'i_brand'			=> $this->input->post('i_brand'),
		'i_price' 			=> $this->input->post('i_price'),
		'i_category' 		=> $this->input->post('i_category'),
		'i_date_used' 		=> $this->input->post('i_date_used'),
		'i_status' 			=> $this->input->post('i_status'),
		'i_receipt_no' 		=> $this->input->post('i_receipt_no'),
		'i_bought_date' 	=> $this->input->post('i_bought_date'),
		'i_bought_time' 	=> $this->input->post('i_bought_time'),
		'i_company_name' 	=> $this->input->post('i_company_name'),
		'i_address' 		=> $this->input->post('i_address'),
		'i_city' 			=> $this->input->post('i_city'),
		'i_contact_no' 		=> $this->input->post('i_contact_no'),
		'i_firstname' 		=> $this->input->post('i_firstname'),
		'i_lastname' 		=> $this->input->post('i_lastname'),
		'i_img' 			=> $pic,
		'i_input_date' 		=> date('Y-m-d'),
		'i_input_time' 		=> date('h:i:s')
		);
		$inventory_tbl 		   = $this->inventory_model->get_inventory();
		$overall_inventory_tbl = $this->inventory_model->get_overall_inventory();
		$overall_inventory_data= array(
			'oi_id'			=>	'OI'.date('Ymdhis'),
			'oi_name'		=>	$this->input->post('i_product_name')
			);
		// foreach ($inventory_tbl as $i) {
		// 	foreach ($overall_inventory_tbl as $oi) {
		// 		if($i->i_product_name !== $oi->oi_name) {
					$this->inventory_model->admin_add_overall_inventory($overall_inventory_data);
		// 		}
		// 	}
		// }
		$this->inventory_model->admin_add_product($data);
		$data['get_overall_inventory'] 		 = $this->inventory_model->get_overall_inventory();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['message'] = 'New Product Added';
		$this->load->view('admin/admin', $data);
	}

	public function admin_add_product_form_sec()
	{
		
				error_reporting('E_Warning' | 'E_Parse');
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/addSec_product', $data);
	}

	public function adminSec_add_product()
	{
		$id = $this->input->post('i_serial_no');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['i_img']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['i_img']['tmp_name'];
        	$folder="./media/inventory/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'i_serial_no' 		=> $this->input->post('i_serial_no'),
		'i_product_name' 	=> $this->input->post('i_product_name'),
		'i_brand' 			=> $this->input->post('i_brand'),
		'i_price' 			=> $this->input->post('i_price'),
		'i_category' 		=> $this->input->post('i_category'),
		'i_date_used' 		=> $this->input->post('i_date_used'),
		'i_status' 			=> $this->input->post('i_status'),
		'i_receipt_no' 		=> $this->input->post('i_receipt_no'),
		'i_bought_date' 	=> $this->input->post('i_bought_date'),
		'i_bought_time' 	=> $this->input->post('i_bought_time'),
		'i_company_name' 	=> $this->input->post('i_company_name'),
		'i_address' 		=> $this->input->post('i_address'),
		'i_city' 			=> $this->input->post('i_city'),
		'i_contact_no' 		=> $this->input->post('i_contact_no'),
		'i_firstname'	 	=> $this->input->post('i_firstname'),
		'i_lastname' 		=> $this->input->post('i_lastname'),
		'i_img' 			=> $pic,
		'i_input_date' 		=> date('Y-m-d'),
		'i_input_time' 		=> date('h:i:s')
		);
		$datacheck	 				= $this->input->post('i_product_name');
		// $this->inventory_model->check_inventory($datacheck);
		// $this->inventory_model->check_overall_inventory($datacheck);
		$check_inventory 			= $this->inventory_model->check_inventory($datacheck);
		$check_overall_inventory    = $this->inventory_model->check_overall_inventory($datacheck);
		// $inventory_tbl 		   		= $this->inventory_model->get_inventory();
		// $overall_inventory_tbl 		= $this->inventory_model->get_overall_inventory();
		// echo '<pre>';
		// print_r($check_inventory);
		// print_r($check_overall_inventory);
		// echo '</pre>';

		
		if($check_inventory == $check_overall_inventory) {
			echo 'Equal';
		}
		else if ($check_inventory !== $check_overall_inventory) { 
			echo 'not equal'; 
			$overall_inventory_data		= array(
			'oi_id'			=>	'OI'.date('Ymdhis'),
			'oi_name'		=>	$this->input->post('i_product_name')
			);
			$this->inventory_model->admin_add_overall_inventory($overall_inventory_data);
		}
		// echo $check_inventory.'<br>';
		// echo $check_overall_inventory;
		// if (strcmp($check_inventory,$check_overall_inventory) !== '0') { 
		// 	echo 'not equal'; 
		// 	$overall_inventory_data		= array(
		// 	'oi_id'			=>	'OI'.date('Ymdhis'),
		// 	'oi_name'		=>	$this->input->post('i_product_name')
		// 	);
		// 	$this->inventory_model->admin_add_overall_inventory($overall_inventory_data);
		// }
		$this->inventory_model->admin_add_product($data);
		$data['get_overall_inventory'] 		 = $this->inventory_model->get_overall_inventory();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_inventory'] 	= $this->inventory_model->get_inventory();
		$data['message'] 		= 'New Product Added';
		$this->load->view('admin/inventory_sec', $data);
	}

	public function admin_add_service_form()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/add_service', $data);
	}

	public function admin_service()
	{
		$id = 'SER'.date('ymdhis');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['s_picture']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['s_picture']['tmp_name'];
        	$folder="./media/service/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		's_id' 				=> 'SER'.date('ymdhis'), 
		's_name' 			=> $this->input->post('s_name'),
		's_description' 	=> $this->input->post('s_description'),
		's_category' 		=> $this->input->post('s_category'),
		's_price' 			=> $this->input->post('s_price'),
		's_picture' 		=> $pic,
		's_status' 			=> $this->input->post('s_status'),
		's_inputdate' 		=> date('Y-m-d'),
		's_inputtime' 		=> date('h:i:s')
		);
		$this->services_model->admin_add_service($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['message'] = 'New Service Added';
		$this->load->view('admin/admin', $data);
	}

	public function admin_add_service_form_sec()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/addSec_service', $data);
	}

	public function adminSec_service()
	{
		$id = 'SER'.date('ymdhis');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['s_picture']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['s_picture']['tmp_name'];
        	$folder="./media/service/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		's_id' 				=> 'SER'.date('ymdhis'), 
		's_name' 			=> $this->input->post('s_name'),
		's_description' 	=> $this->input->post('s_description'),
		's_category' 		=> $this->input->post('s_category'),
		's_price' 			=> $this->input->post('s_price'),
		's_picture' 		=> $pic,
		's_status' 			=> $this->input->post('s_status'),
		's_inputdate' 		=> date('Y-m-d'),
		's_inputtime' 		=> date('h:i:s')
		);
		$this->services_model->admin_add_service($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_services'] = $this->services_model->get_services();
		$data['get_haircolor'] = $this->services_model->get_haircolor();
		$data['get_promos'] = $this->services_model->get_promos();
		$data['message'] = 'New Service Added';
		$this->load->view('admin/service_sec', $data);
	}


	public function admin_add_hair_color_form()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/add_haircolor', $data);
	}

	public function admin_add_hair_color_form_sec()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/addSec_haircolor', $data);
	}

	public function admin_hair_color()
	{
		$id = $this->input->post('hc_id');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['hc_photo']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['hc_photo']['tmp_name'];
        	$folder="./media/haircolor/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'hc_id' 			=> $this->input->post('hc_id'),
		'hc_name' 			=> $this->input->post('hc_name'),
		'hc_photo' 			=> $pic
		);
		$this->services_model->admin_add_haircolor($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['message'] = 'New Hair Color Added';
		$this->load->view('admin/admin', $data);
	}

	public function adminSec_hair_color()
	{
		$id = $this->input->post('hc_id');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['hc_photo']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['hc_photo']['tmp_name'];
        	$folder="./media/haircolor/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'hc_id' 			=> $this->input->post('hc_id'),
		'hc_name' 			=> $this->input->post('hc_name'),
		'hc_photo' 			=> $pic
		);
		$this->services_model->admin_add_haircolor($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_services'] 	= $this->services_model->get_services();
		$data['get_haircolor'] 	= $this->services_model->get_haircolor();
		$data['get_promos'] 	= $this->services_model->get_promos();
		$data['message'] 		= 'New Hair Color Added';
		$this->load->view('admin/service_sec', $data);
	}

	public function admin_add_promo_form()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/add_promo', $data);
	}

	public function admin_add_promo_form_sec()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/addSec_promo', $data);
	}

	
	public function admin_promo()
	{
		$id = 'PRO'.date('ymdhis');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['pr_img']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['pr_img']['tmp_name'];
        	$folder="./media/promo/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'pr_id' 			=> 'PRO'.date('ymdhis'), 
		'pr_name' 			=> $this->input->post('pr_name'),
		'pr_description' 	=> $this->input->post('pr_description'),
		'pr_price' 			=> $this->input->post('pr_price'),
		'pr_status' 		=> $this->input->post('pr_status'),
		'pr_img' 			=> $pic,
		'pr_inputdate' 		=> date('Y-m-d'),
		'pr_inputtime' 		=> date('h:i:s'),
		'pr_inputby' 		=> 'EMP'
		);
		$this->services_model->admin_add_promos($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['message'] = 'New Promo Added';
		$this->load->view('admin/admin', $data);
	}


	public function adminSec_promo()
	{
		$id = 'PRO'.date('ymdhis');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['pr_img']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['pr_img']['tmp_name'];
        	$folder="./media/promo/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'pr_id' 			=> 'PRO'.date('ymdhis'), 
		'pr_name' 			=> $this->input->post('pr_name'),
		'pr_description' 	=> $this->input->post('pr_description'),
		'pr_price' 			=> $this->input->post('pr_price'),
		'pr_status' 		=> $this->input->post('pr_status'),
		'pr_img' 			=> $pic,
		'pr_inputdate' 		=> date('Y-m-d'),
		'pr_inputtime' 		=> date('h:i:s'),
		'pr_inputby' 		=> 'EMP'
		);
		$this->services_model->admin_add_promos($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_services']  = $this->services_model->get_services();
		$data['get_haircolor'] = $this->services_model->get_haircolor();
		$data['get_promos']    = $this->services_model->get_promos();
		$data['message'] 	   = 'New Promo Added';
		$this->load->view('admin/service_sec', $data);
	}

	public function admin_tr_to_sales()
	{
		$data = array(
		's_date' 			=> $this->input->post('s_date'), 
		's_inputtime' 		=> date('h:i:s'),
		's_sale' 			=> $this->input->post('s_sale')
		);
		$this->sales_model->admin_add_sale($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_sales'] = $this->sales_model->get_sales();
		$data['get_transactions'] = $this->transactions_model->get_transactions();
		$data['total_expenditures_inventory'] = $this->inventory_model->total_expenditures_inventory();
		$data['add_transactions'] = $this->transactions_model->add_transactions();
		$data['message'] = 'New Sale Added';
		$this->load->view('admin/sales_sec', $data);
	}

	public function admin_reserve_form()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/add_reservation', $data);
	}

	public function admin_add_reservation()
	{
		$status 		  = 'Reserved';
		$checkindate 	  = $this->input->post('checkindate');
		$checkintime 	  = $this->input->post('checkintime');
		$checkin     	  = $checkindate." ".$checkintime;
		$reservationkey   = $this->input->post('reserveval[]');

		$reservationname  = $this->services_model->get_services_name($reservationkey);
		$reservationtotalprice = $this->services_model->get_services_totalprice($reservationkey);
		$reservationprice = $this->services_model->get_services_price($reservationkey);
		foreach ($reservationname as $key) {
			$names[] = $key->s_name;
		}
		foreach ($reservationtotalprice as $key) {
			$totalprice = $key->s_price;
		}
		foreach ($reservationprice as $key) {
			$price[] = $key->s_price;
		}
		$reservationid 		= implode(',', $reservationkey);
		$service_names 		= implode(',', $names);
		$service_price 		= implode(',', $price);
		$data = array(
			'tr_id' 			=> 'TR'.date('ymdhis'),
			'tr_customer_id' 	=> $this->input->post('tr_customer_id'),
			'tr_customer_name' 	=> $this->input->post('tr_customer_name'),
			'tr_service_ids' 	=> $reservationid,
			'tr_total_price' 	=> $totalprice,
			'tr_prices' 	    => $service_price,
			'tr_service_names' 	=> $service_names,
			'tr_status' 		=> $status,
			'tr_checkin' 		=> $checkin,
			'tr_date' 			=> date('Y-m-d'),
			'tr_time' 			=> date('h:i:s')
		);
		$this->transactions_model->add_transactions($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_contents_header_bg'] = $this->ui_model->get_page_contents_header_bg();
		$data['get_page_contents'] = $this->ui_model->get_page_contents();
		$data['get_other_services'] = $this->ui_model->get_other_services();
		$data['get_color'] = $this->ui_model->get_color();
		$data['get_hairstyle'] = $this->ui_model->get_hairstyle();
		$data['get_aboutus_cp'] = $this->ui_model->get_aboutus_cp();
		$data['get_aboutus_v']  = $this->ui_model->get_aboutus_v();
		$data['get_aboutus_m']  = $this->ui_model->get_aboutus_m();
		$data['get_services_title']  = $this->ui_model->get_services();
		$data['get_services_info']  = $this->ui_model->get_services();
		$data['message'] = 'Reservation Successful!';
		$this->load->view('temp1/index', $data);
	}
}
