<?php

// session_start(); //we need to start session in order to access it through CI

Class admin extends CI_Controller {

public function __construct() {
parent::__construct();
}

// Show login page
public function index() {
	$table_data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
	$table_data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
	$this->load->view('admin/login', $table_data);
}

// Check for user login process
public function admin_login_process() {
	$this->form_validation->set_rules('e_email', 'Email', 'trim|required|xss_clean');
	$this->form_validation->set_rules('e_password', 'Password', 'trim|required|xss_clean');

	if ($this->form_validation->run() == FALSE) 
	{
		if(isset($this->session->userdata['logged_in'])){
		$this->load->view('admin/admin');
		}
		else
		{
		$this->load->view('admin/login');
		}
	} 
	else 
	{
		$data = array(
		'e_email' => $this->input->post('e_email'),
		'e_password' => $this->input->post('e_password')
		);
		$result = $this->admin_login_model->login($data);
		if ($result == TRUE) 
		{
			$e_email = $this->input->post('e_email');
			$result = $this->admin_login_model->read_user_information($e_email);
			if ($result != false) 
			{
				$session_data = array(
				'e_id' 		  => $result[0]->e_id,
				'e_firstname' 		  => $result[0]->e_firstname,
				'e_picture' 		  => $result[0]->e_picture,
				);
				$table_data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
				$table_data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
				$table_data['get_sales'] = $this->sales_model->get_sales();
				$table_data['get_sales_tbl'] = $this->sales_model->get_sales();
				$table_data['total_sales'] = $this->sales_model->total_sales();
				$table_data['total_sales_current_month'] = $this->sales_model->total_sales_current_month();
				$table_data['count_feedbacks'] = $this->feedback_model->count_feedbacks();
				$table_data['count_customers'] = $this->customers_model->count_customers();
				// Add user data in session
				$this->session->set_userdata('logged_in', $session_data);
				$table_data['curpage']= 'dashboard';
				$this->load->view('admin/admin', $table_data);
			}
		} 
		else 
		{	
			$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
			$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
			$data['message'] = 'Email or Password Invalid';
		
			$this->load->view('admin/login', $data);
		}
	}
}
public function myprofile()
	{
		// $table_data['get_transactions'] = $this->transactions_model->get_transactions();
		
		// $table_data['add_transactions'] = $this->transactions_model->add_transactions();



		$table_data = array('e_id' =>  '',
				'e_firstname' => '',
				'e_middlename' => '',
				'e_lastname' => '',
				'e_address' => '',
				'e_zip' => '',
				'e_gender' => '',
				'e_email' => '',
				'e_contact_no' => '',
				'e_picture' => '',
				'e_password' =>'',
				'e_birthdate' => '',
				'e_position' => '',
				'e_registry_date' => '',
				'e_registry_time' => '',
				'e_update_date' => '',
				'e_update_time' => ''
				);
		$select = '*';
		$from = 'employee';
		$where = array('e_firstname'=> $this->session->get_userdata['logged_in']['e_firstname']);
		$fastfetch['db'] = $this->inventory_model->fastfetch($select, $from, $where, null);
		foreach ($fastfetch['db'] as $row) 
		{
			$table_data['e_id'] = $row->e_id;
			$table_data['e_firstname'] = $row->e_firstname;
			$table_data['e_middlename'] = $row->e_middlename;
			$table_data['e_lastname'] = $row->e_lastname;
			$table_data['e_address'] = $row->e_address;
			$table_data['e_gender'] = $row->e_gender;
			$table_data['e_email'] = $row->e_email;
			$table_data['e_contact_no'] = $row->e_contact_no;
			$table_data['e_picture'] = $row->e_picture;
			$table_data['e_picture'] = $row->e_picture;
			$table_data['e_password'] = $row->e_password;
			$table_data['e_birthdate'] = $row->e_birthdate;
			$table_data['e_position'] = $row->e_position;
			$table_data['e_registry_date'] = $row->e_registry_date;
			$table_data['e_registry_time'] = $row->e_registry_time;
			$table_data['e_update_date'] = $row->e_update_date;
			$table_data['e_update_time'] = $row->e_update_time;
		}
		$table_data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$table_data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$table_data['curpage']='myprofile';
		$this->load->view('admin/myprofile', $table_data);
	}
// Logout from admin page
public function logout() {

	// Removing session data
	$sess_array = array(
	'e_email' => ''
	);
	$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
	$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
	$this->session->unset_userdata('logged_in', $sess_array);
	$data['message_display'] = 'Successfully Logout';
	$this->load->view('admin/login', $data);
}

}

?>