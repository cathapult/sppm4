<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller {

	public function __Construct()
	{
	   parent::__Construct ();
	}

	public function cms()
	{	
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_content_logo'] = $this->cms_model->get_page_content_logo();
		$data['get_page_content_c'] = $this->cms_model->get_page_content_c();
		$data['get_page_content_a'] = $this->cms_model->get_page_content_a();
		$data['get_page_contents_s'] = $this->cms_model->get_page_contents_s();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_page_contents'] = $this->cms_model->get_page_contents();
		$data['curpage']='cms';
		$this->load->view('admin/cms_sec', $data);
	}
}
