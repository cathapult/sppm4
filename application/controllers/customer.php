<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function __Construct()
	{
	   parent::__Construct ();
	}

	public function customer()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_customers'] = $this->customers_model->get_customers();
		$data['curpage'] = 'customer';
		$this->load->view('admin/customer_sec', $data);
	}
}
