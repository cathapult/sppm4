<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class customer_ui extends CI_Controller {

	public function __Construct()
	{
	   parent::__Construct ();
	}
		
	public function index()
	{
		// $data['get_services_name'] = $this->services_model->get_services_name();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_contents_header_bg'] = $this->ui_model->get_page_contents_header_bg();
		$data['get_page_contents'] = $this->ui_model->get_page_contents();
		$data['get_other_services'] = $this->ui_model->get_other_services();
		$data['get_color'] = $this->ui_model->get_color();
		$data['get_hairstyle'] = $this->ui_model->get_hairstyle();
		$data['get_aboutus_cp'] = $this->ui_model->get_aboutus_cp();
		$data['get_aboutus_v']  = $this->ui_model->get_aboutus_v();
		$data['get_aboutus_m']  = $this->ui_model->get_aboutus_m();
		$data['get_services_title']  = $this->ui_model->get_services();
		$data['get_services_info']  = $this->ui_model->get_services();
		$this->load->view('temp1/index', $data);
	}

	public function register_customer() {
		$id = 'CUST'.date('ymdhis');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['c_picture']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['c_picture']['tmp_name'];
        	$folder="./media/customer/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'c_id' 				=> 'CUST'.date('ymdhis'),
		'c_firstname' 		=> $this->input->post('c_firstname'),
		'c_middlename' 		=> $this->input->post('c_middlename'),
		'c_lastname' 		=> $this->input->post('c_lastname'),
		'c_address' 		=> $this->input->post('c_address'),
		'c_city' 			=> $this->input->post('c_city'),
		'c_region'		 	=> $this->input->post('c_region'),
		'c_province' 		=> $this->input->post('c_province'),
		'c_zip' 			=> $this->input->post('c_zip'),
		'c_gender' 			=> $this->input->post('c_gender'),
		'c_email' 			=> $this->input->post('c_email'),
		'c_contact_no' 		=> $this->input->post('c_contact_no'),
		'c_password' 		=> $this->input->post('c_password'),
		'c_birthdate' 		=> $this->input->post('c_birthdate'),
		'c_status' 			=> 'Disabled',
		'c_picture' 		=> $pic,
		'c_registry_date' 	=> date('Y-m-d'),
		'c_registry_time' 	=> date('h:i:s')
		);
		$this->customers_model->admin_add_customer($data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_contents_header_bg'] = $this->ui_model->get_page_contents_header_bg();
		$data['get_page_contents'] = $this->ui_model->get_page_contents();
		$data['get_other_services'] = $this->ui_model->get_other_services();
		$data['get_color'] = $this->ui_model->get_color();
		$data['get_hairstyle'] = $this->ui_model->get_hairstyle();
		$data['get_aboutus_cp'] = $this->ui_model->get_aboutus_cp();
		$data['get_aboutus_v']  = $this->ui_model->get_aboutus_v();
		$data['get_aboutus_m']  = $this->ui_model->get_aboutus_m();
		$data['get_services_title']  = $this->ui_model->get_services();
		$data['get_services_info']  = $this->ui_model->get_services();
		$data['message'] = 'Resgistration Successful';
		$this->load->view('temp1/index', $data);

	}
	// Check for user login process
	public function login_process() {
		$this->form_validation->set_rules('c_email', 'Email', 'trim|required|xss_clean');
		$this->form_validation->set_rules('c_password', 'Password', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) 
		{
			if(isset($this->session->userdata['logged_in'])){
			$this->load->view('temp1/index');
			}
			else
			{
			$this->load->view('temp1/index');
			}
		} 
		else 
		{
			$data = array(
			'c_email' => $this->input->post('c_email'),
			'c_password' => $this->input->post('c_password')
			);
			$result = $this->ui_model->login($data);
			if ($result == TRUE) 
			{
				$c_email = $this->input->post('c_email');
				$result = $this->ui_model->read_user_information($c_email);
				if ($result != false) 
				{
					$session_data = array(
					'c_id' 		  => $result[0]->c_id,
					'c_firstname' 		  => $result[0]->c_firstname,
					'c_middlename' 		  => $result[0]->c_middlename,
					'c_lastname' 		  => $result[0]->c_lastname,
					'c_picture' 		  => $result[0]->c_picture,
					);
					$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
					$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
					$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
					$data['get_page_contents_header_bg'] = $this->ui_model->get_page_contents_header_bg();
					$data['get_page_contents'] = $this->ui_model->get_page_contents();
					$data['get_other_services'] = $this->ui_model->get_other_services();
					$data['get_color'] = $this->ui_model->get_color();
					$data['get_hairstyle'] = $this->ui_model->get_hairstyle();
					$data['get_aboutus_cp'] = $this->ui_model->get_aboutus_cp();
					$data['get_aboutus_v']  = $this->ui_model->get_aboutus_v();
					$data['get_aboutus_m']  = $this->ui_model->get_aboutus_m();
					$data['get_services_title']  = $this->ui_model->get_services();
					$data['get_services_info']  = $this->ui_model->get_services();
					$data['message'] = 'Successfully Login';

					// Add user data in session
					$this->session->set_userdata('logged_in', $session_data);
					$this->load->view('temp1/index', $data);
				}
			} 
			else 
			{
				$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
				$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
				$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
				$data['get_page_contents_header_bg'] = $this->ui_model->get_page_contents_header_bg();
				$data['get_page_contents'] = $this->ui_model->get_page_contents();
				$data['get_other_services'] = $this->ui_model->get_other_services();
				$data['get_color'] = $this->ui_model->get_color();
				$data['get_hairstyle'] = $this->ui_model->get_hairstyle();
				$data['get_aboutus_cp'] = $this->ui_model->get_aboutus_cp();
				$data['get_aboutus_v']  = $this->ui_model->get_aboutus_v();
				$data['get_aboutus_m']  = $this->ui_model->get_aboutus_m();
				$data['get_services_title']  = $this->ui_model->get_services();
				$data['get_services_info']  = $this->ui_model->get_services();
				$data['message'] = 'Invalid Email or Password';
				// $data = array(
				// 'message' => 'Invalid Username or Password'
				// );
				$this->load->view('temp1/index', $data);
			}
		}
	}

	// Logout from admin page
	public function logout() {

		// Removing session data
		$sess_array = array(
		'c_email' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);		
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_contents_header_bg'] = $this->ui_model->get_page_contents_header_bg();
		$data['get_page_contents'] = $this->ui_model->get_page_contents();
		$data['get_other_services'] = $this->ui_model->get_other_services();
		$data['get_color'] = $this->ui_model->get_color();
		$data['get_hairstyle'] = $this->ui_model->get_hairstyle();
		$data['get_aboutus_cp'] = $this->ui_model->get_aboutus_cp();
		$data['get_aboutus_v']  = $this->ui_model->get_aboutus_v();
		$data['get_aboutus_m']  = $this->ui_model->get_aboutus_m();
		$data['get_services_title']  = $this->ui_model->get_services();
		$data['get_services_info']  = $this->ui_model->get_services();
		$data['message'] = 'Successfully Logout';
		$this->load->view('temp1/index', $data);
	}
}
