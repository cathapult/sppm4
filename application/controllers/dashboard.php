<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __Construct()
	{
	   parent::__Construct ();
	   error_reporting('E_Warning' | 'E_Parse');
	}

	public function index()
	{
		$table_data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$table_data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$table_data['get_sales'] = $this->sales_model->get_sales();
		$table_data['get_sales_tbl'] = $this->sales_model->get_sales();
		$table_data['total_sales'] = $this->sales_model->total_sales();
		$table_data['total_sales_current_month'] = $this->sales_model->total_sales_current_month();
		$table_data['count_feedbacks'] = $this->feedback_model->count_feedbacks();
		$table_data['count_customers'] = $this->customers_model->count_customers();
		$table_data['curpage']= 'dashboard';
		$this->load->view('admin/admin', $table_data);
	}
};
