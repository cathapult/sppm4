<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	public function __Construct()
	{
	   parent::__Construct ();
	   $this->load->helper('url');
	   $this->load->database(); 
	   $this->load->model('employees_model');
	}

	public function employee()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_employees'] = $this->employees_model->get_employees();
		$data['get_jobpos'] = $this->employees_model->get_jobpos();
		$data['curpage'] = 'employee';
		$this->load->view('admin/employee_sec', $data);
	}
}
