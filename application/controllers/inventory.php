<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {

	public function __Construct()
	{
	   parent::__Construct ();
	   $this->load->helper('url');
	   $this->load->database(); 
	   $this->load->model('inventory_model');
	}

	public function inventory()
	{
		$data['itemquantityandname'] = array ( array (
								'i_product_name' => '',
								'i_product_count' => ''
								) );
		// $data['count_inventory'] = $this->inventory_model->count_inventory();
		$data['get_overall_inventory'] = $this->inventory_model->get_overall_inventory();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_inventory'] = $this->inventory_model->get_inventory();
		

		$i=0;
		$get_inventory_count['db'] = $this->inventory_model->get_inventory_count('i_product_name', 'inventory', null, null);
		foreach ($get_inventory_count['db'] as $row) 
		{
			$count = 0;	
			$data['itemquantityandname'][$i]['i_product_name']  = $row->i_product_name;
			$where = array('i_product_name'		=> $row->i_product_name);
			$get_inventory_notDistinct['db'] = $this->inventory_model->get_inventory_notDistinct('*', 'inventory', $where, null);
			foreach ($get_inventory_notDistinct['db'] as $row2) 
			{	
				$count++;
			}
			$data['itemquantityandname'][$i]['i_product_count']  = $count;
			$i++;

		}
		$data['curpage'] = 'inventory';
		$this->load->view('admin/inventory_sec', $data);
		
	}
}
