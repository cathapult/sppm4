<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __Construct()
	{
	   parent::__Construct ();
	   error_reporting('E_Warning' | 'E_Parse');
	}

	public function myprofile()
	{
		$table_data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$table_data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();

		$table_data['curpage']= 'myprofile';
		$this->load->view('admin/admin', $table_data);
	}
};
