<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {

	public function __Construct()
	{
	   parent::__Construct ();
	   $this->load->helper('url');
	   $this->load->database(); 
	   $this->load->model('sales_model');
	}

	public function sales()
	{
		// $table_data['get_transactions'] = $this->transactions_model->get_transactions();
		$table_data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$table_data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$table_data['get_sales'] = $this->sales_model->get_sales();
		$table_data['get_transactions'] = $this->transactions_model->get_transactions();
		$table_data['get_transactions_date'] = $this->transactions_model->get_transactions();
		$table_data['total_expenditures_inventory'] = $this->inventory_model->total_expenditures_inventory();
		// $table_data['add_transactions'] = $this->transactions_model->add_transactions();
		$table_data['curpage']='sales';
		$this->load->view('admin/sales_sec', $table_data);
	}

}
