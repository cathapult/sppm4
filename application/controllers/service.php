<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {

	public function __Construct()
	{
	   parent::__Construct ();
	}

	public function service()
	{
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_services'] = $this->services_model->get_services();
		$data['get_haircolor'] = $this->services_model->get_haircolor();
		$data['get_promos'] = $this->services_model->get_promos();
		$data['curpage']='service';
		$this->load->view('admin/service_sec', $data);
	}
}
