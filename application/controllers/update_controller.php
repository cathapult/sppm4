<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class update_controller extends CI_Controller {

	public function __Construct()
	{
	   parent::__Construct ();
	}

	public function admin_update_customer_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['admin_get_customer_id'] = $this->customers_model->admin_get_customer_id($id);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$this->load->view('admin/forms/update_customer', $data);
	}

	public function admin_update_customer()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('c_id');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['c_picture']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['c_picture']['tmp_name'];
        	$folder="./media/customer/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'c_firstname' 		=> $this->input->post('c_firstname'),
		'c_middlename' 		=> $this->input->post('c_middlename'),
		'c_lastname' 		=> $this->input->post('c_lastname'),
		'c_address' 		=> $this->input->post('c_address'),
		'c_city'	 		=> $this->input->post('c_city'),
		'c_region' 			=> $this->input->post('c_region'),
		'c_province' 		=> $this->input->post('c_province'),
		'c_zip' 			=> $this->input->post('c_zip'),
		'c_gender' 			=> $this->input->post('c_gender'),
		'c_email' 			=> $this->input->post('c_email'),
		'c_contact_no' 		=> $this->input->post('c_contact_no'),
		'c_status' 			=> $this->input->post('c_status'),
		'c_password' 		=> $this->input->post('c_password'),
		'c_birthdate' 		=> $this->input->post('c_birthdate'),
		'c_picture' 		=> $pic,
		'c_update_date' 	=> date('Y-m-d'),
		'c_update_time' 	=> date('h:i:s')
		);
		$this->customers_model->admin_update_customer($id,$data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_customers'] = $this->customers_model->get_customers();
		$data['message'] = 'Customer Updated';
		$this->load->view('admin/customer_sec', $data);
	}

	public function admin_update_employee_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['admin_get_employee_id'] = $this->employees_model->admin_get_employee_id($id);
		$data['get_jobpos'] = $this->employees_model->get_jobpos();
		$this->load->view('admin/forms/update_employee', $data);
	}

	public function admin_update_employee()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('e_id');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['e_picture']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['e_picture']['tmp_name'];
        	$folder="./media/employee/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'e_firstname' 		=> strip_tags($this->input->post('e_firstname')),
		'e_middlename' 		=> $this->input->post('e_middlename'),
		'e_lastname' 		=> $this->input->post('e_lastname'),
		'e_address' 		=> $this->input->post('e_address'),
		'e_city' 			=> $this->input->post('e_city'),
		'e_region' 			=> $this->input->post('e_region'),
		'e_province' 		=> $this->input->post('e_province'),
		'e_zip' 			=> $this->input->post('e_zip'),
		'e_gender' 			=> $this->input->post('e_gender'),
		'e_email' 			=> $this->input->post('e_email'),
		'e_contact_no' 		=> $this->input->post('e_contact_no'),
		'e_password' 		=> $this->input->post('e_password'),
		'e_birthdate' 		=> $this->input->post('e_birthdate'),
		'e_picture' 		=> $pic,
		'e_position' 		=> $this->input->post('e_position'),
		'e_update_date' 	=> date('Y-m-d'),
		'e_update_time' 	=> date('h:i:s')
		);
		$this->employees_model->admin_update_employee($id,$data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_employees'] 	= $this->employees_model->get_employees();
		$data['get_jobpos'] = $this->employees_model->get_jobpos();
		$data['message'] 		= 'Employee Updated';
		$this->load->view('admin/employee_sec', $data);
	}

	public function admin_update_jobpos_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['admin_get_jobpos_id'] = $this->employees_model->admin_get_jobpos_id($id);
		$this->load->view('admin/forms/update_jobpos', $data);
	}

	public function admin_update_jobpos()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('jp_id');
		$data = array(
		'jp_name' 			=> $this->input->post('jp_name'),
		'jp_type' 			=> $this->input->post('jp_type'),
		'jp_salary' 		=> $this->input->post('jp_salary'),
		'jp_inputdate' 		=> date('Y-m-d'),
		'jp_inputtime' 		=> date('h:i:s')
		);
		$this->employees_model->admin_update_jobpos($id,$data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_employees'] 	= $this->employees_model->get_employees();
		$data['get_jobpos'] 	= $this->employees_model->get_jobpos();
		$data['message'] 		= 'Job Position Updated';
		$this->load->view('admin/employee_sec', $data);
	}

	public function admin_update_product_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['admin_get_product_id'] = $this->inventory_model->admin_get_product_id($id);
		$this->load->view('admin/forms/update_product', $data);
	}

	public function admin_update_product()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('i_serial_no');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['i_img']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['i_img']['tmp_name'];
        	$folder="./media/inventory/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'i_product_name' 	=> $this->input->post('i_product_name'),
		'i_brand' 			=> $this->input->post('i_brand'),
		'i_price' 			=> $this->input->post('i_price'),
		'i_category' 		=> $this->input->post('i_category'),
		'i_date_used' 		=> $this->input->post('i_date_used'),
		'i_status' 			=> $this->input->post('i_status'),
		'i_receipt_no' 		=> $this->input->post('i_receipt_no'),
		'i_bought_date' 	=> $this->input->post('i_bought_date'),
		'i_bought_time' 	=> $this->input->post('i_bought_time'),
		'i_company_name' 	=> $this->input->post('i_company_name'),
		'i_address' 		=> $this->input->post('i_address'),
		'i_city' 			=> $this->input->post('i_city'),
		'i_contact_no' 		=> $this->input->post('i_contact_no'),
		'i_firstname' 		=> $this->input->post('i_firstname'),
		'i_lastname' 		=> $this->input->post('i_lastname'),
		'i_img' 			=> $pic,
		'i_update_date' 	=> date('Y-m-d'),
		'i_update_time' 	=> date('h:i:s')
		);
		$this->inventory_model->admin_update_product($id,$data);
		$data['get_overall_inventory'] 		 = $this->inventory_model->get_overall_inventory();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_inventory'] 	= $this->inventory_model->get_inventory();
		$data['message'] 		= 'Product Updated';
		$this->load->view('admin/inventory_sec', $data);
	}

	public function admin_update_service_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['admin_get_service_id'] = $this->services_model->admin_get_service_id($id);
		$this->load->view('admin/forms/update_service', $data);
	}

	public function admin_update_service()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('s_id');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['s_picture']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['s_picture']['tmp_name'];
        	$folder="./media/service/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		's_name' 			=> $this->input->post('s_name'),
		's_description' 	=> $this->input->post('s_description'),
		's_category' 		=> $this->input->post('s_category'),
		's_price' 			=> $this->input->post('s_price'),
		's_picture' 		=> $pic,
		's_status' 			=> $this->input->post('s_status'),
		's_updatedate' 		=> date('Y-m-d'),
		's_updatetime' 		=> date('h:i:s')
		);
		$this->services_model->admin_update_service($id,$data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_services'] 	= $this->services_model->get_services();
		$data['get_haircolor'] 	= $this->services_model->get_haircolor();
		$data['get_promos'] 	= $this->services_model->get_promos();
		$data['message'] 		= 'Employee Updated';
		$this->load->view('admin/service_sec', $data);
	}

	public function admin_update_haircolor_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['admin_get_haircolor_id'] = $this->services_model->admin_get_haircolor_id($id);
		$this->load->view('admin/forms/update_haircolor', $data);
	}

	public function admin_update_haircolor()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('hc_id');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['hc_photo']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['hc_photo']['tmp_name'];
        	$folder="./media/haircolor/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'hc_name' 			=> $this->input->post('hc_name'),
		'hc_photo' 			=> $pic
		);
		$this->services_model->admin_update_haircolor($id,$data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_services'] 	= $this->services_model->get_services();
		$data['get_haircolor'] 	= $this->services_model->get_haircolor();
		$data['get_promos'] 	= $this->services_model->get_promos();
		$data['message'] 		= 'Hair Color Updated';
		$this->load->view('admin/service_sec', $data);
	}

	public function admin_update_promo_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['admin_get_promo_id'] = $this->services_model->admin_get_promo_id($id);
		$this->load->view('admin/forms/update_promo', $data);
	}

	public function admin_update_promo()
	{
		error_reporting('E_Warning' | 'E_Parse');

		$id = $this->input->post('pr_id');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['pr_img']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['pr_img']['tmp_name'];
        	$folder="./media/promo/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'pr_name' 			=> $this->input->post('pr_name'),
		'pr_description' 	=> $this->input->post('pr_description'),
		'pr_price' 			=> $this->input->post('pr_price'),
		'pr_status' 		=> $this->input->post('pr_status'),
		'pr_img' 			=> $pic,
		'pr_updatedate' 	=> date('Y-m-d'),
		'pr_updatetime' 	=> date('h:i:s'),
		'pr_updateby' 		=> 'EMP'
		);
		$this->services_model->admin_update_promos($id,$data);
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_services'] 	= $this->services_model->get_services();
		$data['get_haircolor'] 	= $this->services_model->get_haircolor();
		$data['get_promos'] 	= $this->services_model->get_promos();
		$data['message'] 		= 'Promo Updated';
		$this->load->view('admin/service_sec', $data);
	}
	

	public function admin_update_page_contents_about_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_content_logo'] = $this->cms_model->get_page_content_logo();
		$data['get_page_content_c'] = $this->cms_model->get_page_content_c();
		$data['get_page_content_a'] = $this->cms_model->get_page_content_a();
		$data['get_page_contents_s'] = $this->cms_model->get_page_contents_s();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['admin_get_page_content_id'] = $this->cms_model->admin_get_page_content_id($id);
		$this->load->view('admin/forms/update_page_contents_about', $data);
	}

	public function admin_update_page_contents_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_content_logo'] = $this->cms_model->get_page_content_logo();
		$data['get_page_content_c'] = $this->cms_model->get_page_content_c();
		$data['get_page_content_a'] = $this->cms_model->get_page_content_a();
		$data['get_page_contents_s'] = $this->cms_model->get_page_contents_s();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['admin_get_page_content_id'] = $this->cms_model->admin_get_page_content_id($id);
		$this->load->view('admin/forms/update_page_contents', $data);
	}

	public function admin_update_page_pictures_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_content_logo'] = $this->cms_model->get_page_content_logo();
		$data['get_page_content_c'] = $this->cms_model->get_page_content_c();
		$data['get_page_content_a'] = $this->cms_model->get_page_content_a();
		$data['get_page_contents_s'] = $this->cms_model->get_page_contents_s();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['admin_get_page_content_id'] = $this->cms_model->admin_get_page_content_id($id);
		$this->load->view('admin/forms/update_page_pictures', $data);
	}

	public function admin_update_page_pictures_gallery_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_content_logo'] = $this->cms_model->get_page_content_logo();
		$data['get_page_content_c'] = $this->cms_model->get_page_content_c();
		$data['get_page_content_a'] = $this->cms_model->get_page_content_a();
		$data['get_page_contents_s'] = $this->cms_model->get_page_contents_s();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['admin_get_page_content_id'] = $this->cms_model->admin_get_page_content_id($id);
		$this->load->view('admin/forms/update_page_pictures_gallery', $data);
	}

	public function admin_update_page_maincontents_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_content_logo'] = $this->cms_model->get_page_content_logo();
		$data['get_page_content_c'] = $this->cms_model->get_page_content_c();
		$data['get_page_content_a'] = $this->cms_model->get_page_content_a();
		$data['get_page_contents_s'] = $this->cms_model->get_page_contents_s();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['admin_get_page_content_id'] = $this->cms_model->admin_get_page_content_id($id);
		$this->load->view('admin/forms/update_page_maincontents', $data);
	}

	public function admin_update_reservation_form()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->uri->segment(3);
		$data['get_transactions'] = $this->transactions_model->get_transactions();
		$data['get_transactions_id'] = $this->transactions_model->get_transaction_id($id);
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_content_logo'] = $this->cms_model->get_page_content_logo();
		$data['get_page_content_c'] = $this->cms_model->get_page_content_c();
		$data['get_page_content_a'] = $this->cms_model->get_page_content_a();
		$data['get_page_contents_s'] = $this->cms_model->get_page_contents_s();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		// $data['admin_get_page_content_id'] = $this->cms_model->admin_get_page_content_id($id);
		$this->load->view('admin/forms/update_reservation', $data);
	}

	public function admin_update_page_pictures()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('pc_id');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['pc_img']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['pc_img']['tmp_name'];
        	$folder="./media/pagecontents/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		// 'pc_description' 		=> $this->input->post('pc_description'),
		'pc_img' 				=> $pic,
		// 'pc_division' 			=> $this->input->post('pc_division'),
		// 'pc_state' 				=> $this->input->post('pc_state'),
		'pc_updatedate' 		=> date('Y-m-d'),
		'pc_updatetime' 		=> date('h:i:s')
		);
		$this->cms_model->admin_update_page_content($id,$data);
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_content_logo'] = $this->cms_model->get_page_content_logo();
		$data['get_page_content_c'] = $this->cms_model->get_page_content_c();
		$data['get_page_content_a'] = $this->cms_model->get_page_content_a();
		$data['get_page_contents_s'] = $this->cms_model->get_page_contents_s();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_page_contents'] = $this->cms_model->get_page_contents();
		$data['message'] = 'Page Content Updated';
		$this->load->view('admin/cms_sec', $data);
	}
	
	public function admin_update_page_pictures_gallery()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('pc_id');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['pc_img']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['pc_img']['tmp_name'];
        	$folder="./media/gallery/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		// 'pc_description' 		=> $this->input->post('pc_description'),
		'pc_img' 				=> $pic,
		// 'pc_division' 			=> $this->input->post('pc_division'),
		// 'pc_state' 				=> $this->input->post('pc_state'),
		'pc_updatedate' 		=> date('Y-m-d'),
		'pc_updatetime' 		=> date('h:i:s')
		);
		$this->cms_model->admin_update_page_content($id,$data);
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_content_logo'] = $this->cms_model->get_page_content_logo();
		$data['get_page_content_c'] = $this->cms_model->get_page_content_c();
		$data['get_page_content_a'] = $this->cms_model->get_page_content_a();
		$data['get_page_contents_s'] = $this->cms_model->get_page_contents_s();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_page_contents'] = $this->cms_model->get_page_contents();
		$data['message'] = 'Page Content Updated';
		$this->load->view('admin/cms_sec', $data);
	}

	public function admin_update_page_maincontents()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('pc_id');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['pc_img']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['pc_img']['tmp_name'];
        	$folder="./media/pagecontents/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'pc_description' 		=> $this->input->post('pc_description'),
		// 'pc_img' 				=> $pic,
		// 'pc_division' 			=> $this->input->post('pc_division'),
		// 'pc_state' 				=> $this->input->post('pc_state'),
		'pc_updatedate' 		=> date('Y-m-d'),
		'pc_updatetime' 		=> date('h:i:s')
		);
		$this->cms_model->admin_update_page_content($id,$data);
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_content_logo'] = $this->cms_model->get_page_content_logo();
		$data['get_page_content_c'] = $this->cms_model->get_page_content_c();
		$data['get_page_content_a'] = $this->cms_model->get_page_content_a();
		$data['get_page_contents_s'] = $this->cms_model->get_page_contents_s();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_page_contents'] = $this->cms_model->get_page_contents();
		$data['message'] = 'Page Content Updated';
		$this->load->view('admin/cms_sec', $data);
	}

	public function admin_update_page_contents()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('pc_id');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['pc_img']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['pc_img']['tmp_name'];
        	$folder="./media/pagecontents/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'pc_description' 		=> $this->input->post('pc_description'),
		// 'pc_img' 				=> $pic,
		// 'pc_division' 			=> $this->input->post('pc_division'),
		// 'pc_state' 				=> $this->input->post('pc_state'),
		'pc_updatedate' 		=> date('Y-m-d'),
		'pc_updatetime' 		=> date('h:i:s')
		);
		$this->cms_model->admin_update_page_content($id,$data);
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_content_logo'] = $this->cms_model->get_page_content_logo();
		$data['get_page_content_c'] = $this->cms_model->get_page_content_c();
		$data['get_page_content_a'] = $this->cms_model->get_page_content_a();
		$data['get_page_contents_s'] = $this->cms_model->get_page_contents_s();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_page_contents'] = $this->cms_model->get_page_contents();
		$data['message'] = 'Page Content Updated';
		$this->load->view('admin/cms_sec', $data);
	}

	public function admin_update_page_contents_about()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('pc_id');
		if(isset($_POST['submit']))
		{
        	$pic = $id.".".pathinfo($_FILES['pc_img']['name'],PATHINFO_EXTENSION);
        	$pic_loc = $_FILES['pc_img']['tmp_name'];
        	$folder="./media/pagecontents/";
        	move_uploaded_file($pic_loc,$folder.$pic);
    	}
		$data = array(
		'pc_description' 		=> $this->input->post('pc_description'),
		// 'pc_img' 				=> $pic,
		// 'pc_division' 			=> $this->input->post('pc_division'),
		// 'pc_state' 				=> $this->input->post('pc_state'),
		'pc_updatedate' 		=> date('Y-m-d'),
		'pc_updatetime' 		=> date('h:i:s')
		);
		$this->cms_model->admin_update_page_content($id,$data);
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_content_logo'] = $this->cms_model->get_page_content_logo();
		$data['get_page_content_c'] = $this->cms_model->get_page_content_c();
		$data['get_page_content_a'] = $this->cms_model->get_page_content_a();
		$data['get_page_contents_s'] = $this->cms_model->get_page_contents_s();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_page_contents'] = $this->cms_model->get_page_contents();
		$data['message'] = 'Page Content Updated';
		$this->load->view('admin/cms_sec', $data);
	}

	public function admin_update_reservation()
	{
		error_reporting('E_Warning' | 'E_Parse');
		$id = $this->input->post('tr_id');
		$status = $this->input->post('tr_status');
		$checkout = date('Y-m-d')." ".$this->input->post('tr_checkout');
		$data = array(
			'tr_id' 			=> $id,
			'tr_status' 		=> $status,
			'tr_checkout' 		=> $checkout
		);
		$this->transactions_model->update_transaction($id,$data);
		$data['get_sales'] = $this->sales_model->get_sales();
		$data['get_transactions_date'] = $this->transactions_model->get_transactions();
		$data['total_expenditures_inventory'] = $this->inventory_model->total_expenditures_inventory();
		$data['get_transactions'] = $this->transactions_model->get_transactions();
		$data['get_page_contents_storename'] = $this->cms_model->get_page_contents_storename();
		$data['get_page_contents_storelogo'] = $this->cms_model->get_page_contents_storelogo();
		$data['get_page_contents_g'] = $this->cms_model->get_page_contents_g();
		$data['get_page_contents_header_bg'] = $this->ui_model->get_page_contents_header_bg();
		$data['get_page_contents'] = $this->ui_model->get_page_contents();
		$data['get_other_services'] = $this->ui_model->get_other_services();
		$data['get_color'] = $this->ui_model->get_color();
		$data['get_hairstyle'] = $this->ui_model->get_hairstyle();
		$data['get_aboutus_cp'] = $this->ui_model->get_aboutus_cp();
		$data['get_aboutus_v']  = $this->ui_model->get_aboutus_v();
		$data['get_aboutus_m']  = $this->ui_model->get_aboutus_m();
		$data['get_services_title']  = $this->ui_model->get_services();
		$data['get_services_info']  = $this->ui_model->get_services();
		$data['message'] = 'Reservation Successful!';
		$this->load->view('admin/sales_sec', $data);
	}
}


