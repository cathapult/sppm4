<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class main_model extends CI_Model{

	function __construct() {
        // Call the Model constructor
        parent::__construct();

    }

    /************************************************************************
    * Function name: Insert  
    * Description: 
    *   This function inserts data to a database table
    *   It returns true or false depending if the process succeeded
    * Developer: Catherine Mae Liwag 
    ************************************************************************/

	function dbinsert($data=array(), $table=null) {
        return ($this->db->insert($table, $data)) ? TRUE : FALSE;
    }

    /************************************************************************
    * Function name: Fetch  
    * Description: 
    *   This function Fetches data from a database table
    *   It returns all acquired data
    * Developer: Catherine Mae Liwag 
    ************************************************************************/

    function fetch($field=null, $table=null, $where=array(), $join=array(), $joinType=null, $field_to_where=null, $or=array(), $distinct=FALSE, $where_not_in=array(), $order_by=null) {
        $this->db->select($field);

        if ($distinct) $this->db->distinct();

        $this->db->from($table);

        if ($where) $this->db->where($where);
        if ($or) $this->db->where_in($field_to_where, $or);
        if ($where_not_in) {
            foreach ($where_not_in as $key => $value) {
                $this->db->where_not_in($key, $value);
            }
        }

        if ($join) {
            if ($joinType) {
                foreach ($join as $key => $value) {
                    $this->db->join($key, $value, $joinType);
                }
            } else {
                foreach ($join as $key => $value) {
                    $this->db->join($key, $value);
                }
            }
        }

        if ($order_by) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value); 
            }
        }
        
        $query = $this->db->get();
        return $query->result();
    }

    /************************************************************************
    * Function name: Fast Fetch  
    * Description: 
    *   This function fetches data from a database table
    *   It has less Parameters than the Fetch Function
    *   It returns all acquired data
    * Developer: Catherine Mae Liwag 
    ************************************************************************/

    function fastfetch($field=null, $table=null, $where=array(), $order_by=null) {
        $this->db->select($field);
        $this->db->from($table);
        if ($where) $this->db->where($where);
        if ($order_by) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value); 
            }
        }
        
        $query = $this->db->get();
        return $query->result();
    }
    /************************************************************************
    * Function name: Fast Fetch  Distinct
    * Description: 
    *   This function fetches data from a database table
    *   It has less Parameters than the Fetch Function
    *   It returns all acquired data
    * Developer: Catherine Mae Liwag 
    ************************************************************************/

    function fastfetchdistinct($field=null, $table=null, $where=array(), $order_by=null) {
        $this->db->distinct();
        $this->db->select($field);
        $this->db->from($table);
        if ($where) $this->db->where($where);
        if ($order_by) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value); 
            }
        }
        
        $query = $this->db->get();
        return $query->result();
    }

    /************************************************************************
    * Function name: Update  
    * Description: 
    *   This function updates the data of a database table
    *   It returns true or false depending if the process succeeded
    * Developer: Catherine Mae Liwag 
    ************************************************************************/

    function dbupdate($where=array(), $data = array(), $table=null) {
        if ($where) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        
        return ($this->db->update($table, $data)) ? TRUE : FALSE;
    }

    /************************************************************************
    * Function name: Delete  
    * Description: 
    *   This function deletes data from a database table
    *   It returns true or false depending if the process succeeded
    * Developer: Catherine Mae Liwag 
    ************************************************************************/

    function dbdelete($where=null, $table=null) {
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        
        return ($this->db->delete($table, $where)) ? TRUE : FALSE;
    }
	
}

?>