<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cms_model extends CI_Model {

	public function __construct(){
		parent:: __construct();
	}
  
	function get_page_contents(){
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_page_content_a(){
      $id = array('PCA01','PCA02','PCA03');
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where_in('pc_id',$id);
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_page_content_logo(){
      $id = array('PCC01','PCC03');
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where_in('pc_id',$id);
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_page_content_c(){
      $id = array('PCC02');
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where_in('pc_id',$id);
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_page_contents_s(){
      $id = array('PCS01','PCS02','PCS03','PCS04','PCS05','PCS06','PCS07','PCS08');
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where_in('pc_id',$id);
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_page_contents_g(){
      $id = array('PCG01','PCG02','PCG03','PCG04','PCG05','PCG06');
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where_in('pc_id',$id);
	  $query = $this->db->get();
	  return $query->result();
    }

    public function get_page_contents_storename(){
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where('pc_id', 'PCC02');
	  $query = $this->db->get();
	  return $query->result();
    }

    public function get_page_contents_storelogo(){
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where('pc_id', 'PCC01');
	  $query = $this->db->get();
	  return $query->result();
    }

    function admin_get_page_content_id($data){
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where('pc_id', $data);
	  $query = $this->db->get();
	  return $query->result();
	}

	function admin_update_page_content($id,$data){
	  $this->db->where('pc_id', $id);
	  $this->db->update('page_contents', $data);
	}
}