<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class customers_model extends CI_Model {

	public function __construct(){
		parent:: __construct();
	}
  
	function get_customers(){
	  $this->db->select("*");
	  $this->db->from('customer');
	  $query = $this->db->get();
	  return $query->result();	  
    }

    function count_customers(){
	  $this->db->select("*");
	  $this->db->from('customer');
	  $query = $this->db->get();
	  return $query->num_rows();	  
    }
    
    function admin_add_customer($data){
	  $this->db->insert('customer', $data);
	}

	function admin_get_customer_id($data){
	  $this->db->select("*");
	  $this->db->from('customer');
	  $this->db->where('c_id', $data);
	  $query = $this->db->get();
	  return $query->result();
	}

	function admin_update_customer($id,$data){
	  $this->db->where('c_id', $id);
	  $this->db->update('customer', $data);
	}
}