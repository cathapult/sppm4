<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class employees_model extends CI_Model {

	public function __construct(){
		parent:: __construct();
	}
  
	function get_employees(){
	  $this->db->select("*");
	  $this->db->from('employee');
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_jobpos(){
	  $this->db->select("*");
	  $this->db->from('job_positions');
	  $query = $this->db->get();
	  return $query->result();
    }

    function admin_get_jobpos_id($data){
	  $this->db->select("*");
	  $this->db->from('job_positions');
	  $this->db->where('jp_id', $data);
	  $query = $this->db->get();
	  return $query->result();
	}
	
    function admin_add_jobpos($data){
	  $this->db->insert('job_positions', $data);
	}

	function admin_update_jobpos($id,$data){
	  $this->db->where('jp_id', $id);
	  $this->db->update('job_positions', $data);
	}

    function admin_add_employee($data){
	  $this->db->insert('employee', $data);
	}

	function admin_get_employee_id($data){
	  $this->db->select("*");
	  $this->db->from('employee');
	  $this->db->where('e_id', $data);
	  $query = $this->db->get();
	  return $query->result();
	}

	function admin_update_employee($id,$data){
	  $this->db->where('e_id', $id);
	  $this->db->update('employee', $data);
	}
}