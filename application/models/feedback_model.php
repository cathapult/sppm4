<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback_model extends CI_Model {

	public function __construct(){
		parent:: __construct();
	}
  
	function get_feedbacks(){
	  $this->db->select("*");
	  $this->db->from('feedbacks');
	  $query = $this->db->get();
	  return $query->result();
    }

    function count_feedbacks(){
      $status = 'Not Read';
      $this->db->select("*");
	  $this->db->from('feedbacks');
	  $this->db->where('fb_status', $status);
	  $query = $this->db->get();
	  return $query->num_rows();
    }
}