<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class inventory_model extends CI_Model {

	public function __construct(){
		parent:: __construct();
	}
  
	function get_inventory(){
	  $this->db->select("*");
	  $this->db->from('inventory');
	  $query = $this->db->get();
	  return $query->result();
    }
    function fastfetch($field=null, $table=null, $where=array(), $order_by=null) {
        $this->db->select($field);
        $this->db->from($table);
        if ($where) $this->db->where($where);
        if ($order_by) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value); 
            }
        }
        
        $query = $this->db->get();
        return $query->result();
    }
    function get_inventory_count($field=null, $table=null, $where=array(), $order_by=null) {
        $this->db->distinct();
        $this->db->select($field);
        $this->db->from($table);
        if ($where) $this->db->where($where);
        if ($order_by) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value); 
            }
        }
        $query = $this->db->get();
        return $query->result();
    }
    function get_inventory_notDistinct($field=null, $table=null, $where=array(), $order_by=null) {
        $this->db->select($field);
        $this->db->from($table);
        if ($where) $this->db->where($where);
        if ($order_by) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value); 
            }
        }
        $query = $this->db->get();
        return $query->result();
    }
    function check_inventory($datacheck){
	  $this->db->select("i_product_name");
	  $this->db->from('inventory');
	  $this->db->where('i_product_name', $datacheck);
	  $query = $this->db->get();
	  return $query->result();
    }
    function check_overall_inventory($datacheck){
	  $this->db->select("oi_name");
	  $this->db->from('overall_inventory');
	  $this->db->where('oi_name', $datacheck);
	  $query = $this->db->get();
	  return $query->result();
    }
   //  function count_inventory($count){
	  // $this->db->select("*");
	  // $this->db->from('inventory');
	  // $this->db->where('i_product_name', $count)
	  // $query = $this->db->get();
	  // return $query->num_rows();	  
   //  }
    public function total_expenditures_inventory(){
      $this->db->select_sum('i_price');
      $this->db->from('inventory');
      $query = $this->db->get();
      return $query->result();
	}

    function admin_add_product($data){
	  $this->db->insert('inventory', $data);
	}

	function admin_get_product_id($data){
	  $this->db->select("*");
	  $this->db->from('inventory');
	  $this->db->where('i_serial_no', $data);
	  $query = $this->db->get();
	  return $query->result();
	}

	function admin_update_product($id,$data){
	  $this->db->where('i_serial_no', $id);
	  $this->db->update('inventory', $data);
	}

	function get_overall_inventory() { 
	  $this->db->select("*");
	  $this->db->from('overall_inventory');
	  $query = $this->db->get();
	  return $query->result();
	}

	function admin_add_overall_inventory($overall_inventory_data) {
	  $this->db->insert('overall_inventory', $overall_inventory_data);
	}
}