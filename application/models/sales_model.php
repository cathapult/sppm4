<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sales_model extends CI_Model {

	public function __construct(){
		parent:: __construct();
	}
  
	function get_sales(){
	  $this->db->select("*");
	  $this->db->from('sales');
	  $query = $this->db->get();
	  return $query->result();
    }

    public function total_sales(){
      $this->db->select_sum('s_sale');
      $this->db->from('sales');
      $query = $this->db->get();
      return $query->result();
	}

	public function total_sales_current_month(){
	  $date = date('m');
	  $this->db->select_sum('s_sale');
	  $this->db->from('sales');
	  $this->db->where('MID(s_date,6,2)', $date);
      $query = $this->db->get();
      return $query->result();
	}

	function admin_add_sale($data){
	  $this->db->insert('sales', $data);
	}
}
