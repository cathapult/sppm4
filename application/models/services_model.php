<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class services_model extends CI_Model {

	public function __construct(){
		parent:: __construct();
	}
  
	function get_services(){
	  $this->db->select("*");
	  $this->db->from('services');
	  $this->db->order_by('s_category','asc');
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_services_name($reservationkey){
	  $this->db->select("s_name");
	  $this->db->from('services');
	  $this->db->where_in('s_id',$reservationkey);
	  $query = $this->db->get();
	  // $result = $query->result();
	  return $query->result();
    }

    function get_services_price($reservationkey){
	  $this->db->select("s_price");
	  $this->db->from('services');
	  $this->db->where_in('s_id',$reservationkey);
	  $query = $this->db->get();
	  // $result = $query->result();
	  return $query->result();
    }

    function get_services_totalprice($reservationkey){
      $this->db->select_sum('s_price');
	  // $this->db->select("s_price");
	  $this->db->from('services');
	  $this->db->where_in('s_id',$reservationkey);
	  $query = $this->db->get();
	  return $query->result();
    }

    function admin_add_service($data){
	  $this->db->insert('services', $data);
	}

	function admin_get_service_id($data){
	  $this->db->select("*");
	  $this->db->from('services');
	  $this->db->where('s_id', $data);
	  $query = $this->db->get();
	  return $query->result();
	}

	function admin_update_service($id,$data){
	  $this->db->where('s_id', $id);
	  $this->db->update('services', $data);
	}

	function get_haircolor(){
	  $this->db->select("*");
	  $this->db->from('hair_colors');
	  $query = $this->db->get();
	  return $query->result();
    }

    function admin_get_haircolor_id($data){
	  $this->db->select("*");
	  $this->db->from('hair_colors');
	  $this->db->where('hc_id', $data);
	  $query = $this->db->get();
	  return $query->result();
	}

    function admin_add_haircolor($data){
	  $this->db->insert('hair_colors', $data);
	}

	function admin_update_haircolor($id,$data){
	  $this->db->where('hc_id', $id);
	  $this->db->update('hair_colors', $data);
	}

    function get_promos(){
	  $this->db->select("*");
	  $this->db->from('promos');
	  $query = $this->db->get();
	  return $query->result();
    }

    function admin_get_promo_id($data){
	  $this->db->select("*");
	  $this->db->from('promos');
	  $this->db->where('pr_id', $data);
	  $query = $this->db->get();
	  return $query->result();
	}

    function admin_add_promos($data){
	  $this->db->insert('promos', $data);
	}

	function admin_update_promos($id,$data){
	  $this->db->where('pr_id', $id);
	  $this->db->update('promos', $data);
	}
}