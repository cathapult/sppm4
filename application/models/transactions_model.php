<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class transactions_model extends CI_Model {

	public function __construct(){
		parent:: __construct();
	}
  
	function get_transactions(){
	  $date = date('Y-m-d'); 
	  $this->db->select("*");
	  $this->db->from('transactions');
	  $this->db->where('tr_date', $date);
	  $query = $this->db->get();
	  return $query->result();
    }

    function add_transactions($data)
    {
      $this->db->insert('transactions', $data);
	}

	function update_transaction($id,$data){
	  $this->db->where('tr_id', $id);
	  $this->db->update('transactions', $data);
	}

	function get_transaction_id($id){
	  $this->db->select("*");
	  $this->db->from('transactions');
	  $this->db->where('tr_id', $id);
	  $query = $this->db->get();
	  return $query->result();
	}

}
