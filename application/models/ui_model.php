<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ui_model extends CI_Model {

	public function __construct(){
		parent:: __construct();
	}
  	// Read data using c_email and c_password
	public function login($data) {

		$condition = "c_email =" . "'" . $data['c_email'] . "' AND " . "c_password =" . "'" . $data['c_password'] . "'";
		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where($condition);
		$this->db->where('c_status', 'Active');
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
		return true;
		} else {
		return false;
		}
	}

	// Read data from database to show data in admin page
	public function read_user_information($c_email) {

		$condition = "c_email =" . "'" . $c_email . "'";
		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
		return $query->result();
		} else {
		return false;
		}
	}

	function get_aboutus_cp(){
	  $aboutus = 'ABOUT US';
	  $title = 'Company Profile';
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where('pc_division',$aboutus);
	  $this->db->where('pc_title',$title);
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_aboutus_v(){
	  $aboutus = 'ABOUT US';
	  $title = 'Vision';
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where('pc_division',$aboutus);
	  $this->db->where('pc_title',$title);
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_aboutus_m(){
	  $aboutus = 'ABOUT US';
	  $title = 'Mission';
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where('pc_division',$aboutus);
	  $this->db->where('pc_title',$title);
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_services(){
      $service = 'SERVICES';
      $state = 'SHOW';
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where('pc_division',$service);
	  $this->db->where('pc_state',$state);
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_hairstyle() {
      $category = array('Long Hairstyle', 'Medium Cut Hairstyle', 'Short Hairstyle');
      $this->db->select("*");
	  $this->db->from('services');
	  $this->db->where_in('s_category',$category);
	  $this->db->where('s_status','Available');
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_color() {
      $this->db->select("*");
	  $this->db->from('hair_colors');
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_other_services() {
      $category = array('Nails','Massage and Spa','Treatment','Form','Hair Styling','Make up');
      $this->db->select("*");
	  $this->db->from('services');
	  $this->db->where_in('s_category',$category);
	  $this->db->where('s_status','Available');
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_page_contents(){
	  $this->db->select("*");
	  $this->db->from('page_contents');
	  $this->db->where('pc_id','PCC03');
	  $query = $this->db->get();
	  return $query->result();
    }

    function get_page_contents_header_bg(){
	  $this->db->select("pc_img","pc_id");
	  $this->db->from('page_contents');
	  $this->db->where('pc_id','PCC03');
	  $query = $this->db->get();
	  return $query->result();
    }
}