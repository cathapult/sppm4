<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Dashboard</title>
	<?php include('design/css.php') ;?>
</head>

<body>

<div id="wrapper">
<?php include('blocks/navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Dashboard</p>
            </div>
        </div>
        <?php if (isset($message)) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-info alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="fa fa-info-circle"></i>  <strong><?php echo $message;?></strong> 
                </div>
            </div>
        </div>
        <!-- /.row -->
        <?php } ?>
        <div class="row">
            <div class="col-lg-2">
                <div class="panel panel-dark-purple">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-universal-access fa-4x dark-purple"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"></div>
                                <div>Web Visitors</div>
                            </div>
                        </div>
                    </div>
                    <!-- <a href="#"> -->
                        <div class="panel-footer panel-dark-purple panel-dark-purple-bordertop">
                            <span class="pull-left">26</span>
                            <!-- <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span> -->
                            <div class="clearfix"></div>
                        </div>
                    <!-- </a> -->
                </div>
            </div>
            <div class="col-lg-2">
                <div class="panel panel-dark-purple">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-4x dark-purple"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"></div>
                                <div>Customers</div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer panel-dark-purple panel-dark-purple-bordertop">
                        <?php $total_customers = number_format($count_customers); ?>
                        <span class="pull-left"><?php echo $total_customers;?></span>
                        <span class="pull-right"><a href="<?php echo base_url();?>customer/customer">View <i class="fa fa-arrow-circle-right"></i></a></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="panel panel-dark-purple">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-money fa-4x dark-purple"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                
                                <div class="huge"></div>
                                <div>Income of the Month</div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer panel-dark-purple panel-dark-purple-bordertop">
                        <?php $tscm = number_format($total_sales_current_month[0]->s_sale); ?>
                        <span class="pull-left">PHP</span>
                        <span class="pull-right"><?php echo $tscm;?></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="panel panel-dark-purple">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-money fa-4x dark-purple"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"></div>
                                <div>Total Income</div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer panel-dark-purple panel-dark-purple-bordertop">
                        <?php $total_income = number_format($total_sales[0]->s_sale); ?>
                        <span class="pull-left">PHP</span>
                        <span class="pull-right"><?php echo $total_income;?></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="panel panel-dark-purple">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-cubes fa-4x dark-purple"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">26</div>
                                <div>Supplies Needed</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer panel-dark-purple panel-dark-purple-bordertop">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="panel panel-dark-purple">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-exclamation-circle fa-4x dark-purple"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"></div>
                                <div>Reports</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer panel-dark-purple panel-dark-purple-bordertop">
                            <?php $total_feedbacks = number_format($count_feedbacks); ?>
                            <span class="pull-left"><?php echo $total_feedbacks;?></span>
                            <span class="pull-right">View <i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
 
        <br>
        
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Transactions Table for the month of <?php echo date('F');?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Sale</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($get_sales_tbl as $gst) { 
                                    $year = substr($gst->s_date,0,4);
                                    $month = substr($gst->s_date,5,2);
                                    $day = substr($gst->s_date,8,2);
                                    if (date('m') == $month) { 
                                ?>
                                    <tr>
                                        <td><?php echo $gst->s_date;?></td>
                                        <td>PHP <?php echo number_format($gst->s_sale);?></td>
                                    </tr>
                                    <?php } }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-cubes fa-fw"></i> Supplies To Be Bought</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Date/Time</th>
                                        <th>Product Name</th>
                                        <th>Number of Supplies</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo date('m-d-Y');?>/<?php echo date('m:i:s');?></td>
                                        <td>Johnson's Powder</td>
                                        <td>3</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="#">View All Supplies <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>        
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Transactions Chart For This Month</h3>
                    </div>
                    <div class="panel-body">
                        <div id="LineChartMonthlySale" style="height: 400px; width: 100%;"></div>
                        <div class="text-right">
                            <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function() {
        var chart = new CanvasJS.Chart("LineChartMonthlySale",
        {

            title:{
                text: "Sales For The Month of <?php echo date('F');?>",
                fontColor: "white",
                fontSize: 30
            },
                        animationEnabled: true,
            axisX:{

                gridColor: "Silver",
                tickColor: "silver",
                labelFontColor: "white",
                valueFormatString: "DD/MMM"

            },                        
                        toolTip:{
                          shared:true
                        },
            theme: "theme2",
            backgroundColor: "#520e25",
            axisY: {
                valueFormatString: "PHP 0",
                labelFontColor: "white",
                gridColor: "Silver",
                tickColor: "silver"
            },
            legend:{
                verticalAlign: "center",
                horizontalAlign: "right"
            },
            data: [
            {        
                type: "line",
                showInLegend: true,
                lineThickness: 2,
                name: "Sales",
                markerType: "square",
                color: "#F08080",
                dataPoints: [
                <?php 
                    foreach ($get_sales as $sales) { 
                        $year = substr($sales->s_date,0,4);
                        $month = substr($sales->s_date,5,2);
                        $day = substr($sales->s_date,8,2);
                        if (date('m') == substr($sales->s_date,5,2)) { ?>
                { x: new Date(<?php echo $year;?>,0,<?php echo $day;?>), y: <?php echo $sales->s_sale;?> },
                <?php } }?>
                ]
            }

            
            ],
          legend:{
            cursor:"pointer",
            itemclick:function(e){
              if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
              }
              else{
                e.dataSeries.visible = true;
              }
              chart.render();
            }
          }
        });
        chart.render();

        var chart = new CanvasJS.Chart("PieChartItemsMostUsed", {
            title:{
                text: "Products Mostly Used",
                fontColor: "white"
            },
                    animationEnabled: true,
            legend:{
                verticalAlign: "center",
                horizontalAlign: "left",
                fontSize: 20,
                fontColor: "white",
                fontFamily: "Helvetica"        
            },
            theme: "theme1",
            backgroundColor: "#520e25",
            data: [
            {        
                type: "pie",   
                indexLabelFontColor: "white",    
                indexLabelFontFamily: "Helvetica",       
                indexLabelFontSize: 20,
                indexLabel: "{label} {y}%",
                startAngle:-20,      
                showInLegend: true,
                toolTipContent:"{legendText} {y}%",
                dataPoints: [
                    {  y: <?php echo '72.3';?>, legendText:"Tresemme", label: "Tresemme" },
                    {  y: 8.16, legendText:"Generic Shampoo", label: "Generic Shampoo" },
                    {  y: 4.67, legendText:"Bigen", label: "Bigen" },
                    {  y: 1.67, legendText:"Matrix" , label: "Matrix"},       
                    {  y: 0.98, legendText:"Others" , label: "Others"}
                ]
            }
            ]
        });
        chart.render();

        var chart = new CanvasJS.Chart("DonutChartMostHairstyle",
        {
            title:{
                text: "Most Hairstyle",
                verticalAlign: 'top',
                horizontalAlign: 'left',
                fontColor: "white"
            },
                    animationEnabled: true,
                    backgroundColor: "#520e25",
            data: [
            {        
                type: "doughnut",
                startAngle:20,
                toolTipContent: "{label}: {y} - <strong>#percent%</strong>",
                indexLabel: "{label} #percent%",
                indexLabelFontColor: "white",    
                dataPoints: [
                    {  y: 67, label: "Undercut" },
                    {  y: 28, label: "Flattop" },
                    {  y: 10, label: "Mohawk" },
                    {  y: 7,  label: "Barbers Cut"},
                    {  y: 4,  label: "Others"}
                ]
            }
            ]
        });
        chart.render();

    }
</script>

<!-- END OF MAIN MODY -->
    
</div> 
<!-- /#page-content-wrapper -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>
