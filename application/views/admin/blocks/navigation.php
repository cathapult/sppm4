<?php
if (isset($this->session->userdata['logged_in'])) {
$e_id = ($this->session->userdata['logged_in']['e_id']);
$e_firstname = ($this->session->userdata['logged_in']['e_firstname']);
$e_picture = ($this->session->userdata['logged_in']['e_picture']);
} else {
header("location: login");
}
?>
<!-- Sidebar -->
<div id="sidebar-wrapper">
    <div class="sidebar-nav">
    	<h1 class="sidebar-logo text-center"><?php echo $get_page_contents_storename[0]->pc_description;?></h1>	
        <?php if($e_picture == NULL) { ?>
    	<img src="<?php echo base_url();?>media/default-user.png" class="sidebar-img img-circle text-center" width="100" height="100">
        <?php } elseif ($e_picture !== NULL) { ?>
        <img src="<?php echo base_url();?>media/employee/<?php echo $e_picture;?>" class="sidebar-img img-circle text-center" width="100" height="100">
        <?php } ?>
    	<h4 class="user-welcome text-center">Welcome <?php echo $e_firstname;?>!</h4>
        <div class="sidebar-btns text-center">
            <button type="button" class="btn btn-default" style="color:#520e25" onclick="location.href='<?php echo base_url();?>update_controller/admin_update_employee_form/<?php echo $e_id;?>';">Edit Profile</button>
            <button type="button" class="btn btn-purple btn-sm" onclick="location.href='<?php echo base_url();?>admin/logout';">Signout</button>
        </div><ul style="margin-left:-30px">
        <li class="<?php if($curpage=='dashboard') echo 'active';?>"><a href="<?php echo base_url();?>dashboard"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li>
         <li class="<?php if($curpage=='myprofile') echo 'active';?>"><a href="<?php echo base_url();?>admin/myprofile"><i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li>
		<li class="<?php if($curpage=='sales') echo 'active';?>"><a href="<?php echo base_url();?>sales/sales"><i class="fa fa-line-chart" aria-hidden="true"></i> Sales Section</a></li>
		<li class="<?php if($curpage=='employee') echo 'active';?>"><a href="<?php echo base_url();?>employee/employee"><i class="fa fa-id-card-o" aria-hidden="true"></i> Employee Section</a></li>
		<li class="<?php if($curpage=='inventory') echo 'active';?>"><a href="<?php echo base_url();?>inventory/inventory"><i class="fa fa-cubes" aria-hidden="true"></i> Inventory Section</a></li>
		<li class="<?php if($curpage=='service') echo 'active';?>"><a href="<?php echo base_url();?>service/service"><i class="fa fa-scissors" aria-hidden="true"></i> Service Section</a></li>
		<li class="<?php if($curpage=='customer') echo 'active';?>"><a href="<?php echo base_url();?>customer/customer"><i class="fa fa-users" aria-hidden="true"></i> Customer Section</a></li>
		<li class="<?php if($curpage=='cms') echo 'active';?>"><a href="<?php echo base_url();?>cms/cms"><i class="fa fa-th" aria-hidden="true"></i> Content Management</a></li></ul>
		<!-- <a href="<?php echo base_url();?>settings/settings"><i class="fa fa-cogs" aria-hidden="true"></i> Settings</a>
		<a href="<?php echo base_url();?>help/help"><i class="fa fa-info-circle" aria-hidden="true"></i> Help</a> -->
    </div>
    <div class="sidenav-footer text-center">
        <p>Copyright &copy; 2016</p>
    </div>
</div>
<!-- /#sidebar-wrapper -->

<!-- <a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a> -->