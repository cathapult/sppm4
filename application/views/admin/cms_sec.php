<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Content Management</title>
	<?php include('design/css.php') ;?>
</head>

<body>
<div id="wrapper">
<?php include('blocks/navigation.php') ;?>
<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a> <i class="fa fa-th" aria-hidden="true"></i> Content Management Section</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Content Management Section</p>
            </div>
        </div>

        <?php if (isset($message)) { ?>
            <div class="row" id="alert-row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-check-circle" aria-hidden="true"></i> <strong><?php echo $message;?></strong> 
                    </div>
                </div>
            </div>
        <?php } ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel-title">
                                    <h4><i class="fa fa-th" aria-hidden="true"></i> Logo</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">    
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="20%">Logo</th>
                                        <th width="65%">Update Date & Time</th>
                                        <th width="15%">Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($get_page_content_logo as $page_contents) {?>
                                    <tr>
                                        <td>
                                            <?php if($page_contents->pc_img == NULL){
                                                echo "No File";
                                            } 
                                            else { ?>
                                            <img src="<?php echo base_url();?>/media/pagecontents/<?php echo $page_contents->pc_img;?>" width="80px" height="80px">
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $page_contents->pc_updatedate;?> <?php echo $page_contents->pc_updatetime;?></td>
                                        <td><a href="<?php echo base_url();?>update_controller/admin_update_page_pictures_form/<?php echo $page_contents->pc_id;?>" class="btn btn-dark-purple" id="menu-toggle">Update</a></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="#">PAGINATION</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel-title">
                                    <h4><i class="fa fa-th" aria-hidden="true"></i> Main Content Section</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">    
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="20%">Title</th>
                                        <th width="45%">Content</th>
                                        <th width="20%">Update Date & Time</th>
                                        <th width="15%">Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($get_page_content_c as $page_contents) {?>
                                    <tr>
                                        <td><?php echo $page_contents->pc_title;?></td>
                                        <td><?php echo $page_contents->pc_description;?></td>
                                        <td><?php echo $page_contents->pc_updatedate;?> <?php echo $page_contents->pc_updatetime;?></td>
                                        <td><a href="<?php echo base_url();?>update_controller/admin_update_page_maincontents_form/<?php echo $page_contents->pc_id;?>" class="btn btn-dark-purple" id="menu-toggle">Update</a></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="#">PAGINATION</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel-title">
                                    <h4><i class="fa fa-th" aria-hidden="true"></i> Content About Us Section</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">    
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="15%">Module</th>
                                        <th width="15%">Title</th>
                                        <th width="40%">Content</th>
                                        <th width="15%">Update Date & Time</th>
                                        <th width="15%">Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($get_page_content_a as $page_contents_about) {?>
                                    <tr>
                                        <td><?php echo $page_contents_about->pc_division;?></td>
                                        <td><?php echo $page_contents_about->pc_title;?></td>
                                        <td><?php echo $page_contents_about->pc_description;?></td>
                                        <td><?php echo $page_contents_about->pc_updatedate;?> <?php echo $page_contents_about->pc_updatetime;?></td>
                                        <td><a href="<?php echo base_url();?>update_controller/admin_update_page_contents_about_form/<?php echo $page_contents_about->pc_id;?>" class="btn btn-dark-purple" id="menu-toggle">Update</a></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="#">PAGINATION</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel-title">
                                    <h4><i class="fa fa-th" aria-hidden="true"></i> Content Services Section</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">    
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="15%">Module</th>
                                        <th width="15%">Title</th>
                                        <th width="40%">Content</th>
                                        <th width="15%">Update Date & Time</th>
                                        <th width="15%">Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($get_page_contents_s as $page_contents_ser) {?>
                                    <tr>
                                        <td><?php echo $page_contents_ser->pc_division;?></td>
                                        <td><?php echo $page_contents_ser->pc_title;?></td>
                                        <td><?php echo $page_contents_ser->pc_description;?></td>
                                        <td><?php echo $page_contents_ser->pc_updatedate;?> <?php echo $page_contents_ser->pc_updatetime;?></td>
                                        <td><a href="<?php echo base_url();?>update_controller/admin_update_page_contents_form/<?php echo $page_contents_ser->pc_id;?>" class="btn btn-dark-purple" id="menu-toggle">Update</a></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="#">PAGINATION</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel-title">
                                    <h4><i class="fa fa-th" aria-hidden="true"></i> Gallery Section</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">    
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="20%">Image</th>
                                        <th width="65%">Update Date & Time</th>
                                        <th width="15%">Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($get_page_contents_g as $page_contents) {?>
                                    <tr>
                                        <td>
                                            <?php if($page_contents->pc_img == NULL){
                                                echo "No File";
                                            } 
                                            else { ?>
                                            <img src="<?php echo base_url();?>/media/gallery/<?php echo $page_contents->pc_img;?>" width="80px" height="80px">
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $page_contents->pc_updatedate;?> <?php echo $page_contents->pc_updatetime;?></td>
                                        <td><a href="<?php echo base_url();?>update_controller/admin_update_page_pictures_gallery_form/<?php echo $page_contents->pc_id;?>" class="btn btn-dark-purple" id="menu-toggle">Update</a></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
    <!-- /#page-content-wrapper -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>

