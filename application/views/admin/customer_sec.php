<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Customer Section</title>

    <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">
<?php include('blocks/navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-users" aria-hidden="true"></i> Customer Section</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Customer Section</p>
            </div>
        </div>

        <?php if (isset($message)) { ?>
            <div class="row" id="alert-row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-check-circle" aria-hidden="true"></i> <strong><?php echo $message;?></strong> 
                    </div>
                </div>
            </div>
            <!-- /.row -->
        <?php } ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="panel-title">
                                    <h4><i class="fa fa-users" aria-hidden="true"></i> Customer Table</h4> 
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <a href="<?php echo base_url();?>add_controller/admin_add_customer_form_sec"><button type="button" class="btn btn-dark-purple btn-sm"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Customer</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">    
                        <div class="table-responsive">
                            <table class="table" id="customerTable">
                                <thead>
                                    <tr>
                                        
                                        <!-- <th>Customer ID</th> -->
                                        <th>#</th>
                                        <th>Name</th>
                                        <!-- <th>Middlename</th> -->
                                        <th width="20%">Address</th>
                                        <th>Email</th>
                                        <th>Contact Number</th>
                                        <th>Picture</th>
                                        <th>Birthdate</th>
                                        <th>Registry Date</th>
                                        <th>Registry Time</th>
                                        <th>Status</th>

                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $ctr=1; foreach ($get_customers as $customers) {?>
                                    <tr>
                                        
                                        <!-- <td><?php echo $customers->c_id;?></td> -->
                                        <td><?php echo $ctr;?></td>
                                        <td><?php echo $customers->c_firstname;?> <?php echo $customers->c_middlename;?> <?php echo $customers->c_lastname;?></td>

                                        <!-- <td></td> -->
                                        <td><?php echo $customers->c_address;?></td>
                                        <td><?php echo $customers->c_email;?></td>
                                        <td><?php echo $customers->c_contact_no;?></td>
                                        <td>
                                            <?php if($customers->c_picture == NULL){
                                                echo "No File";
                                            } 
                                            else { ?>
                                            <img src="<?php echo base_url();?>/media/customer/<?php echo $customers->c_picture;?>" width="80px" height="80px">
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $customers->c_birthdate;?></td>
                                        <td><?php echo $customers->c_registry_date;?></td>
                                        <td><?php echo $customers->c_registry_time;?></td>
                                                                                <td>
                                        <?php 
                                            $status = $customers->c_status;
                                            if($status == 'Disabled') {
                                            ?>
                                            <p style="color: red"><?php echo $status;?></p> 
                                            <?php } else { ?>
                                            <p style="color: green"><?php echo $status;?></p>
                                            <?php } ?>
                                        </td>
                                        <td><a href="<?php echo base_url();?>update_controller/admin_update_customer_form/<?php echo $customers->c_id;?>" class="btn btn-dark-purple" id="menu-toggle">Update</a></td>
                                    </tr>
                                    <?php $ctr++;}?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="#">PAGINATION</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <!-- /#page-content-wrapper -->

<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



