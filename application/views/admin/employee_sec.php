<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Employee Section</title>

	<?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('blocks/navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-id-card-o" aria-hidden="true"></i> Employee Section</h1>
            </div>
            
        </div>
        <?php if (isset($message)) { ?>
            <div class="row" id="alert-row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-check-circle" aria-hidden="true"></i> <strong><?php echo $message;?></strong> 
                    </div>
                </div>
            </div>
            <!-- /.row -->
        <?php } ?>
        <ul class="nav nav-pills nav-justified">
            <li class="active"><a data-toggle="tab" href="#active">Employees</a></li>
            <li><a data-toggle="tab" href="#inactive">Job Positions</a></li>
        </ul>
        <div class="tab-content">

        <div id="active" class="tab-pane fade in active">

            
        <div class="row">
            <div class="col-lg-12" style="margin-top: 5px;margin-bottom: 5px" align="right">
               <a href="#addemp" class="btn btn-default" id="menu-toggle" data-toggle="modal"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Add New Employee</a>
                                <!-- <a href="x"><button type="button" class="btn btn-dark-purple btn-sm"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Employee</button></a> -->
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="panel-title">
                                    <h4><i class="fa fa-id-card-o" aria-hidden="true"></i> Employees Table</h4> 
                                </div>
                            </div>
                            <!-- Modal -->
                            <div id="addemp" class="modal fade" role="dialog">
                               <div class="modal-dialog modal-lg">
                                  <form action="<?php echo base_url();?>add_controller/addSec_employee" method="post" enctype="multipart/form-data">  
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h3 align="center" class="modal-title title-color">
                                        Add A New Employee
                                        </h3>
                                      </div>
                                      <div class="modal-body">
                                        <div class="row">
                                            <div class="col-sm-12 form-group">
                                              <label>Photo</label>
                                             <div style="color:#111"><input type="file" required name="e_picture"></div> 
                                            </div>
                                        </div>
                                      <div class="row">
                                        <div class="col-sm-4 form-group">
                                            <label>First Name</label>
                                          <input type="text" required placeholder="First Name" class="form-control" name="e_firstname">
                                        </div>
                                        <div class="col-sm-4 form-group">
                                          <label>Middle Name</label>
                                          <input type="text" placeholder="Middle Name" class="form-control" name="e_middlename">
                                        </div>
                                        <div class="col-sm-4 form-group">
                                         <label>Last Name</label>
                                          <input type="text" required placeholder="Last Name" class="form-control" name="e_lastname">
                                        </div>
                                      </div> 
                                      <div class="row">
                                      <div class="col-sm-6 form-group">
                                          <label>Gender</label>
                                          <select class="form-control" name="e_gender" >
                                              <option value="Male">Male</option>
                                              <option value="Female">Female</option>
                                          </select>
                                        </div> 
                                        <div class="col-sm-6 form-group">
                                          <label>Birthdate</label>
                                          <input type="date" required placeholder="Date Of Birth" class="form-control" name="e_birthdate">
                                        </div> 
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea required placeholder="Enter Address Here.." rows="3" class="form-control"  style="resize: none;" name="e_address"></textarea>
                                          </div>  

                                                  
                                      <div class="row">
                                        <div class="col-sm-6 form-group">
                                          <label>Zip</label>
                                          <input type="number" required placeholder="Enter Your Zip Code Here.." class="form-control" name="e_zip">
                                        </div> 
                                      
                                        <div class="col-sm-6 form-group">
                                            <label>Contact Number</label>

                                          <div class="input-group">
                                            <span class="input-group-addon">+63</span><input type="number" maxlength="10" min="10" required placeholder="Enter Phone Number Here.." class="form-control" name="e_contact_no">
                                         </div> 
                                        </div>   
                                         
                                      </div> 
                                        <hr>
                                        <div class="row">
                                        <div class="col-sm-12 form-group">
                                          <label>Email Address</label>
                                          <input type="email" required placeholder="Enter Email Address Here.." class="form-control" name="e_email">
                                        </div> 
                                     </div>
                                     <div class="row">
                                        <div class="col-sm-12 form-group">
                                          <label>Position</label>
                                          <select class="form-control" name="e_position">
                                            <?php foreach ($get_jobpos as $jobpos) {?>
                                              <option><?php echo $jobpos->jp_name;?></option>
                                            <?php } ?>
                                          </select>
                                        </div>
                                      </div>
                                       <div class="row">
                                        <div class="col-sm-6 form-group">
                                          <label>Password</label>
                                          <input type="password" required placeholder="Enter Password Here.." class="form-control">
                                        </div>    
                                        <div class="col-sm-6 form-group">
                                          <label>Confirm Password</label>
                                          <input type="password" required placeholder="Enter Password Here.." class="form-control" name="e_password">
                                        </div>  
                                      </div> 
                                    </div>
                                    <div class="modal-footer">
                                       <div align="center"> <button type="submit" class="btn btn-dark-purple">Submit</button></div>
                                      </div>
                                  </div>
                              </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-striped" >
                            <table class="table" id="employeeTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee ID</th>
                                        <th>Picture</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Email</th>
                                        <th>Contact Number</th>
                                        <th>Birthdate</th>
                                        <th>Position</th>
                                        <th>Registry Date & Time</th>
                                        <th>Update Date & Time</th>
                                        <th>Options</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $ctr=1; foreach ($get_employees as $employees) {?>
                                    <tr>
                                        <td><?php echo $ctr;?></td>
                                        <td><?php echo $employees->e_id;?></td>
                                        <td>
                                            <?php if($employees->e_picture == NULL){
                                                echo "No File";
                                            } 
                                            else { ?>
                                            <img src="<?php echo base_url();?>/media/employee/<?php echo $employees->e_picture;?>" width="80px" height="80px">
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $employees->e_firstname;?> <?php echo $employees->e_middlename;?> <?php echo $employees->e_lastname;?></td>
                                        <td><?php echo $employees->e_address;?></td>
                                        <td><?php echo $employees->e_email;?></td>
                                        <td><?php echo $employees->e_contact_no;?></td>
                                        
                                        <td><?php echo $employees->e_birthdate;?></td>
                                        <td><?php echo $employees->e_position;?></td>
                                        <td><?php echo $employees->e_registry_date;?> <?php echo $employees->e_registry_time;?></td>
                                        <td><?php echo $employees->e_update_date;?> <?php echo $employees->e_update_time;?></td>
                                        <td><a href="<?php echo base_url();?>update_controller/admin_update_employee_form/<?php echo $employees->e_id;?>" class="btn btn-dark-purple" id="menu-toggle">Update</a></td>
                                    </tr>
                                    <?php $ctr++; }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="#">PAGINATION</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div id="inactive" class="tab-pane fade in">

        <div class="row">
            <div class="col-lg-12" style="margin-top: 5px;margin-bottom: 5px" align="right">
               <a href="#addjob" class="btn btn-default" id="menu-toggle" data-toggle="modal"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Add New Job Position</a>
                            <!-- <a href="x"><button type="button" class="btn btn-dark-purple btn-sm"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Employee</button></a> -->
            </div>

            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="panel-title">
                                    <h4><i class="fa fa-id-badge" aria-hidden="true"></i> Job Positions Table</h4> 
                                </div>
                            </div>
                            <div id="addjob" class="modal fade" role="dialog">
                               <div class="modal-dialog">
                                  <form action="<?php echo base_url();?>add_controller/adminSec_jobpos" method="post" enctype="multipart/form-data">  
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h3 align="center" class="modal-title title-color">
                                        Add A New Job Position
                                        </h3>
                                      </div>
                                      <div class="modal-body">
                                      <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <label>Job Position Name</label>
                                         <input type="text" placeholder="Enter Job Position Name Here.." class="form-control" name="jp_name">
                                        </div>
                                      </div> 
                                      <div class="row">
                                      <div class="col-sm-12 form-group">
                                          <label>Job Type</label>
                                            <select class="form-control" name="jp_type">
                                                <option value="Managerial">Managerial</option>
                                                <option value="Man Power">Man Power</option>
                                            </select>
                                          </div> 
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-12 form-group">
                                          <label>Salary</label>
                                          <input type="number" placeholder="Enter Salary Here.." class="form-control" name="jp_salary">
                                        </div> 
                                      </div> 
                                    </div>
                                    <div class="modal-footer">
                                       <div align="center"> <button type="submit" class="btn btn-dark-purple">Submit</button></div>
                                      </div>
                                  </div>
                              </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-striped">
                            <table class="table" id="jobposTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Job Position Name</th>
                                        <th>Job Type</th>
                                        <th>Salary</th>
                                        <th>Created Date & Time</th>
                                        <th>Created By</th>
                                        <th>Update Date & Time</th>
                                        <th>Updated By</th>
                                        <th>Options</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $ctr=1; foreach ($get_jobpos as $job_position) {?>
                                    <tr>
                                        <td><?php echo $ctr;?></td>
                                        <td><?php echo $job_position->jp_name;?></td>
                                        <td><?php echo $job_position->jp_type;?></td>
                                        <td><?php echo $job_position->jp_salary;?></td>
                                        <td><?php echo $job_position->jp_inputdate;?> <?php echo $job_position->jp_inputtime;?></td>
                                        <td><?php echo $job_position->jp_inputby;?></td>
                                        <td><?php echo $job_position->jp_updatedate;?> <?php echo $job_position->jp_updatetime;?></td>
                                        <td><?php echo $job_position->jp_updateby;?></td>
                                        <td><a href="<?php echo base_url();?>update_controller/admin_update_jobpos_form/<?php echo $job_position->jp_id;?>" class="btn btn-dark-purple" id="menu-toggle">Update</a></td>

                                    </tr>
                                    <?php $ctr++; }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="#">PAGINATION</a>
                        </div>
                    </div>
                </div>
            </div>
        </div></div></div>

    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



