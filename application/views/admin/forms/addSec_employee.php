<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Employee Registration</title>

  <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('C:\xampp\htdocs\sppm\application\views\admin\blocks\navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-scissors" aria-hidden="true"></i> Customer Registration</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Employee Section > Employee Registration</p>
            </div>
        </div>

        <div class="row">
          <form action="<?php echo base_url();?>add_controller/adminSec_add_employee" method="post">
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>First Name</label>
                  <input type="text" placeholder="Enter First Name Here.." class="form-control" name="e_firstname">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Middle Name</label>
                  <input type="text" placeholder="Enter Middle Name Here.." class="form-control" name="e_middlename">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Last Name</label>
                  <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="e_lastname">
                </div>
              </div>          
              <div class="form-group">
                <label>Address</label>
                <textarea placeholder="Enter Address Here.." rows="3" class="form-control"  style="resize: none;" name="e_address"></textarea>
              </div>  
              <div class="row">
                <div class="col-sm-3 form-group">
                  <label>City</label>
                  <input type="text" placeholder="Enter Your City Here.." class="form-control" name="e_city">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Region</label>
                  <input type="text" placeholder="Enter Your Region Here.." class="form-control" name="e_region">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Province</label>
                  <input type="text" placeholder="Enter Your Province Here.." class="form-control" name="e_province">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Zip</label>
                  <input type="text" placeholder="Enter Your Zip Code Here.." class="form-control" name="e_zip">
                </div>  
              </div>
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Gender</label>
                  <select class="form-control" name="e_gender">
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                  </select>
                </div>    
                <div class="col-sm-4 form-group">
                  <label>Birthdate</label>
                  <input type="date" placeholder="Date Of Birth" class="form-control" name="e_birthdate">
                </div>  
                <div class="col-sm-4 form-group">
                  <label>Photo</label>
                  <input type="file" class="form-control" name="e_picture">
                </div>
              </div>                
              <div class="row">
              <div class="col-sm-4 form-group">
                  <label>Position</label>
                  <select class="form-control" name="e_position">
                    <?php foreach ($get_jobpos as $jobpos) {?>
                      <option><?php echo $jobpos->jp_name;?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-sm-4 form-group">
                  <label>Contact Number</label>
                  <input type="text" placeholder="Enter Phone Number Here.." class="form-control" name="e_contact_no">
                </div>    
                <div class="col-sm-4 form-group">
                  <label>Email Address</label>
                  <input type="email" placeholder="Enter Email Address Here.." class="form-control" name="e_email">
                </div>  
              </div>  
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label>Password</label>
                  <input type="password" placeholder="Enter Password Here.." class="form-control">
                </div>    
                <div class="col-sm-6 form-group">
                  <label>Confirm Password</label>
                  <input type="password" placeholder="Enter Password Here.." class="form-control" name="e_password">
                </div>  
              </div>
              <input type="submit" class="btn btn-lg btn-dark-purple" name="submit">        
            </div>
          </form> 
        </div>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



