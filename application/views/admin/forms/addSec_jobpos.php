<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Add New Job Position</title>
  <?php include('design/css.php') ;?>
</head>

<body>
  <div id="wrapper">
  <?php include('C:\xampp\htdocs\sppm\application\views\admin\blocks\navigation.php') ;?>
  <!-- MAIN BODY -->
      <div class="container-fluid">
          <div class="row">
              <div class="col-lg-10">
                  <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-scissors" aria-hidden="true"></i> Add New Job Position</h1>
              </div>
              <div class="col-lg-2" style="margin-top: 2.4em">
                  <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Employee Section > Add New Job Position</p>
              </div>
          </div>
          <div class="row">
            <form action="<?php echo base_url();?>add_controller/adminSec_jobpos" method="post">
              <div class="col-sm-12">
                <div class="row"> 
                  <div class="col-sm-4 form-group">
                    <label>Job Position Name</label>
                    <input type="text" placeholder="Enter Job Position Name Here.." class="form-control" name="jp_name">
                  </div>
                  <div class="col-sm-4 form-group">
                    <label>Salary</label>
                    <input type="text" placeholder="Enter Salary Here.." class="form-control" name="jp_salary">
                  </div>
                  <div class="col-sm-4 form-group">
                    <label>Job Type</label>
                    <select class="form-control" name="jp_type">
                        <option value="Managerial">Managerial</option>
                        <option value="Man Power">Man Power</option>
                    </select>
                  </div>   
                </div>
                <input type="submit" class="btn btn-lg btn-dark-purple">  
              </div>
            </form> 
          </div>
      </div>
      <!-- /#page-content-wrapper -->
  <!-- END OF MAIN BODY -->
  </div>
<!-- /#wrapper -->
</body>
<?php include('design/js.php') ;?>
</html>



