<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Add New Product</title>

  <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('C:\xampp\htdocs\sppm\application\views\admin\blocks\navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-scissors" aria-hidden="true"></i> Add New Product</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Inventory Section > Add New Product</p>
            </div>
        </div>

        <div class="row">
          <form action="<?php echo base_url();?>add_controller/adminSec_add_product" method="post" enctype="multipart/form-data">
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-12">
                  <h3>Product Information</h3>
                  <hr>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Serial Number</label>
                  <input type="text" placeholder="Enter Serial Number Here.." class="form-control" name="i_serial_no">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Receipt Number</label>
                  <input type="text" placeholder="Enter Receipt Number Here.." class="form-control" name="i_receipt_no">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Price</label>
                  <input type="text" placeholder="Enter Price Here.." class="form-control" name="i_price">
                </div>
              </div>
              <div class="row"> 
                <div class="col-sm-4 form-group">
                  <label>Product Name</label>
                  <input type="text" placeholder="Enter Product Name Here.." class="form-control" name="i_product_name">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Brand Name</label>
                  <input type="text" placeholder="Enter Brand Name Here.." class="form-control" name="i_brand">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Photo</label>
                  <input type="file" class="form-control" name="i_img">
                </div>
              </div>          
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Category</label>
                  <input type="text" placeholder="Enter Category Here..(Scissors,Razor,Powder,...)" class="form-control" name="i_category">
                  <!-- <select class="form-control" name="i_category">
                      <option value="Scissors">Scissors</option>
                      <option>Powder</option>
                  </select> -->
                </div>  
                <div class="col-sm-4 form-group">
                  <label>Date Used</label>
                  <input type="date" class="form-control" name="i_date_used">
                </div>  
                <div class="col-sm-4 form-group">
                  <label>Status</label>
                  <select class="form-control" name="i_status">
                      <option value="Not Used">Not Used</option>
                      <option value="In Used">In Used</option>
                  </select>
                </div>  
              </div>
              <div class="row"> 
                <div class="col-sm-6 form-group">
                  <label>Bought Date</label>
                  <input type="date" class="form-control" name="i_bought_date">
                </div>
                <div class="col-sm-6 form-group">
                  <label>Bought Time</label>
                  <input type="time" class="form-control" name="i_bought_time">
                </div>
              </div>  
              <div class="row">
                <div class="col-sm-12">
                  <h3>Store Information</h3>
                  <hr>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-3 form-group">
                  <label>Store Name</label>
                  <input type="text" placeholder="Enter Store Name Here.." class="form-control" name="i_company_name">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Address</label>
                  <input type="text" placeholder="Enter Store Address Here.." class="form-control" name="i_address">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>City</label>
                  <input type="text" placeholder="Enter City Bought Here.." class="form-control" name="i_city">
                </div>
                <div class="col-sm-3 form-group">
                  <label>Store Contact Number</label>
                  <input type="text" placeholder="Enter Store Contact No. Here.." class="form-control" name=" i_contact_no">
                </div>  
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <h3>Buyers Information</h3> 
                  <hr>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label>First Name</label>
                  <input type="text" placeholder="Enter First Name Here.." class="form-control" name="  i_firstname">
                </div>
                <div class="col-sm-6 form-group">
                  <label>Last Name</label>
                  <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="i_lastname">
                </div>
              </div>  
              <input type="submit" class="btn btn-lg btn-dark-purple" name="submit">    
            </div>
          </form> 
        </div>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



