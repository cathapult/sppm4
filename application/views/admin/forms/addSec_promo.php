<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Add New Promo</title>

  <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('C:\xampp\htdocs\sppm\application\views\admin\blocks\navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-scissors" aria-hidden="true"></i> Add New Promo</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Services Section > Add Promo</p>
            </div>
        </div>

        <div class="row">
          <form action="<?php echo base_url();?>add_controller/adminSec_promo" method="post" enctype="multipart/form-data">
            <div class="col-sm-12">
              <div class="row"> 
                <div class="col-sm-4 form-group">
                  <label>Promo Name</label>
                  <input type="text" placeholder="Enter Promo Name Here.." class="form-control" name="pr_name">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Status</label>
                  <select class="form-control" name="pr_status">
                      <option value="Available">Available</option>
                      <option>Not Available</option>
                      <option>Soon</option>
                  </select>
                </div>
                <div class="col-sm-4 form-group">
                  <label>Price</label>
                  <input type="text" class="form-control" name="pr_price">
                </div>
              </div>          
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Promo Description</label>
                  <textarea placeholder="Enter Description Here.." rows="3" class="form-control"  style="resize: none;" name="pr_description"></textarea>
                </div>  
                <div class="col-sm-4 form-group">
                  <label>Promo Image</label>
                  <input type="file" name="pr_img">
                </div>  
              </div>
              <input type="submit" class="btn btn-lg btn-dark-purple" name="submit">  
            </div>
          </form> 
        </div>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



