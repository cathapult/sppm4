<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Add New Hair Color</title>

  <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('C:\xampp\htdocs\sppm\application\views\admin\blocks\navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Hair Color</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Service Section > Add New Hair Color</p>
            </div>
        </div>

        <div class="row">
          <form action="<?php echo base_url();?>add_controller/admin_hair_color" method="post" enctype="multipart/form-data">
            <div class="col-sm-12">
              <div class="row"> 
                <div class="col-sm-4 form-group">
                  <label>Color ID</label>
                  <input type="text" placeholder="Enter Hair Color ID Here.." class="form-control" name="hc_id">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Color Name</label>
                  <input type="text" placeholder="Enter Color Name Here.." class="form-control" name="hc_name">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Photo</label>
                  <input type="file" class="form-control" name="hc_photo">
                </div>   
              </div>
              <input type="submit" class="btn btn-lg btn-dark-purple" name="submit">  
            </div>
          </form> 
        </div>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



