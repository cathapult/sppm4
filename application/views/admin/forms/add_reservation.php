<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Add New Reservation</title>

  <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('C:\xampp\htdocs\sppm\application\views\admin\blocks\navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-scissors" aria-hidden="true"></i> Add New Reservation</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Add New Reservation</p>
            </div>
        </div>

        <div class="row">
          <form action="" method="post">
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-12">
                  <h3>Customer Information</h3>
                  <hr>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>First Name</label>
                  <input type="text" placeholder="Enter First Name Here.." class="form-control" name="tr_customername_fn">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Middle Name</label>
                  <input type="text" placeholder="Enter Middle Name Here.." class="form-control" name="tr_customername_mn">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Last Name</label>
                  <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="tr_customername_ln">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <h3>Services Information</h3>
                  <hr>
                </div>
              </div>
              <h3 class="text-center">Hairstyle</h3>
              <div class="row">
                <div class="col-sm-3 form-group">
                  <label>Store Name</label>
                  <input type="text" placeholder="Enter Store Name Here.." class="form-control" name="i_company_name">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Address</label>
                  <input type="text" placeholder="Enter Store Address Here.." class="form-control" name="i_address">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>City</label>
                  <input type="text" placeholder="Enter City Bought Here.." class="form-control" name="i_city">
                </div>
                <div class="col-sm-3 form-group">
                  <label>Store Contact Number</label>
                  <input type="text" placeholder="Enter Store Contact No. Here.." class="form-control" name=" i_contact_no">
                </div>  
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <h3>Buyers Information</h3> 
                  <hr>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label>First Name</label>
                  <input type="text" placeholder="Enter First Name Here.." class="form-control" name="i_firstname">
                </div>
                <div class="col-sm-6 form-group">
                  <label>Last Name</label>
                  <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="i_lastname">
                </div>
              </div>  
              <input type="submit" class="btn btn-lg btn-dark-purple">    
            </div>
          </form> 
        </div>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



