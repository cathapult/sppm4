<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Add New Service</title>

  <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('C:\xampp\htdocs\sppm\application\views\admin\blocks\navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-scissors" aria-hidden="true"></i> Add New Service</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Add New Service</p>
            </div>
        </div>

        <div class="row">
          <form action="<?php echo base_url();?>add_controller/admin_service" method="post" enctype="multipart/form-data">
            <div class="col-sm-12">
              <div class="row"> 
                <div class="col-sm-4 form-group">
                  <label>Service Name</label>
                  <input type="text" placeholder="Enter Product Name Here.." class="form-control" name="s_name">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Category Name</label>
                  <input type="text" class="form-control" name="s_category">
                  <!-- <select class="form-control" name="s_category">
                      <option value="Short Hairstyle">Short Hairstyle</option>
                      <option>Color</option>
                  </select> -->
                </div>
                <div class="col-sm-4 form-group">
                  <label>Price</label>
                  <input type="text" class="form-control" name="s_price">
                </div>
              </div>    
              <div class="form-group">
                <label>Service Description</label>
                <textarea placeholder="Enter Description Here.." rows="3" class="form-control"  style="resize: none;" name="s_description"></textarea>
              </div>      
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Status</label>
                  <select class="form-control" name="s_status">
                      <option value="Available">Available</option>
                      <option>Not Available</option>
                      <option>Soon</option>
                  </select>
                </div>  
                <div class="col-sm-4 form-group">
                  <label>Service Photo</label>
                  <input type="file" name="s_picture">
                </div>  
              </div>
              <input type="submit" class="btn btn-lg btn-dark-purple" name="submit">  
            </div>
          </form> 
        </div>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



