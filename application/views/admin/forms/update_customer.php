<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Update Customer Information</title>

  <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('C:\xampp\htdocs\sppm\application\views\admin\blocks\navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-scissors" aria-hidden="true"></i> Update Customer Information</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Customer Section > Update Cusomter Information</p>
            </div>
        </div>

        <div class="row">
          
          <?php foreach ($admin_get_customer_id as $customer) {?>
          <form action="<?php echo base_url();?>update_controller/admin_update_customer" method="post" enctype="multipart/form-data">
            <div class="col-sm-12">
            <input type="hidden" name="c_id" value="<?php echo $customer->c_id;?>">
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>First Name</label>
                  <input type="text" placeholder="Enter First Name Here.." class="form-control" name="c_firstname" value="<?php echo $customer->c_firstname;?>">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Middle Name</label>
                  <input type="text" placeholder="Enter Middle Name Here.." class="form-control" name="c_middlename" value="<?php echo $customer->c_middlename;?>">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Last Name</label>
                  <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="c_lastname" value="<?php echo $customer->c_lastname;?>">
                </div>
              </div>          
              <div class="form-group">
                <label>Address</label>
                <textarea placeholder="Enter Address Here.." rows="3" class="form-control"  style="resize: none;" name="c_address"><?php echo $customer->c_address;?></textarea>
              </div>  
              <div class="row">
                <div class="col-sm-3 form-group">
                  <label>City</label>
                  <input type="text" placeholder="Enter Your City Here.." class="form-control" name="c_city" value="<?php echo $customer->c_city;?>">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Region</label>
                  <input type="text" placeholder="Enter Your Region Here.." class="form-control" name="c_region" value="<?php echo $customer->c_region;?>">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Province</label>
                  <input type="text" placeholder="Enter Your Province Here.." class="form-control" name="c_province" value="<?php echo $customer->c_province;?>">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Zip</label>
                  <input type="text" placeholder="Enter Your Zip Code Here.." class="form-control" name="c_zip" value="<?php echo $customer->c_zip;?>">
                </div>  
              </div>
              <div class="row">
                <div class="col-sm-3 form-group">
                  <label>Gender</label>
                  <select class="form-control" name="c_gender" value="<?php echo $customer->c_gender;?>">
                      <option value="Male" <?php if($customer->c_gender == "Male") { echo'selected';} else {}?>>Male</option>
                      <option value="Female" <?php if($customer->c_gender == "Female") { echo'selected';} else {}?>>Female</option>
                  </select>
                </div>    
                <div class="col-sm-3 form-group">
                  <label>Birthdate</label>
                  <input type="date" placeholder="Date Of Birth" class="form-control" name="c_birthdate" value="<?php echo $customer->c_birthdate;?>">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Photo</label>
                  <input type="file" class="form-control" name="c_picture">
                </div>
                <div class="col-sm-3 form-group">
                  <label>Status</label>
                  <select class="form-control" name="c_status">
                      <option value="Active" <?php if($customer->c_status == "Active") { echo'selected';} else {}?>>Active</option>
                      <option value="Disabled" <?php if($customer->c_status == "Disabled") { echo'selected';} else {}?>>Disabled</option>
                  </select>
                </div>
              </div>                
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label>Contact Number</label>
                  <input type="text" placeholder="Enter Phone Number Here.." class="form-control" name="c_contact_no" value="<?php echo $customer->c_contact_no;?>">
                </div>    
                <div class="col-sm-6 form-group">
                  <label>Email Address</label>
                  <input type="email" placeholder="Enter Email Address Here.." class="form-control" name="c_email" value="<?php echo $customer->c_email;?>">
                </div>  
              </div>  
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label>Password</label>
                  <input type="password" placeholder="Enter Password Here.." class="form-control" value="<?php echo $customer->c_password;?>">
                </div>    
                <div class="col-sm-6 form-group">
                  <label>Confirm Password</label>
                  <input type="password" placeholder="Enter Password Here.." class="form-control" name="c_password" value="<?php echo $customer->c_password;?>">
                </div>  
              </div>
              <input type="submit" class="btn btn-lg btn-dark-purple" name="submit">       
            </div>
          </form>
          <?php }?> 
        </div>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



