<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Update Employee Information</title>

  <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('C:\xampp\htdocs\sppm\application\views\admin\blocks\navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-scissors" aria-hidden="true"></i> Update Employee Information</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Employee Section > Update Employee Information</p>
            </div>
        </div>

        <div class="row">
          <?php foreach ($admin_get_employee_id as $employee) {?>
          <form action="<?php echo base_url();?>update_controller/admin_update_employee" method="post" enctype="multipart/form-data">
            <div class="col-sm-12">
            <input type="hidden" name="e_id" value="<?php echo $employee->e_id;?>">
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>First Name</label>
                  <input type="text" placeholder="Enter First Name Here.." class="form-control" name="e_firstname" value="<?php echo $employee->e_firstname;?>">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Middle Name</label>
                  <input type="text" placeholder="Enter Middle Name Here.." class="form-control" name="e_middlename" value="<?php echo $employee->e_middlename;?>">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Last Name</label>
                  <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="e_lastname" value="<?php echo $employee->e_lastname;?>">
                </div>
              </div>          
              <div class="form-group">
                <label>Address</label>
                <textarea placeholder="Enter Address Here.." rows="3" class="form-control"  style="resize: none;" name="e_address"><?php echo $employee->e_address;?></textarea>
              </div>  
              <div class="row">
                <div class="col-sm-3 form-group">
                  <label>City</label>
                  <input type="text" placeholder="Enter Your City Here.." class="form-control" name="e_city" value="<?php echo $employee->e_city;?>">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Region</label>
                  <input type="text" placeholder="Enter Your Region Here.." class="form-control" name="e_region" value="<?php echo $employee->e_region;?>">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Province</label>
                  <input type="text" placeholder="Enter Your Province Here.." class="form-control" name="e_province" value="<?php echo $employee->e_province;?>">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Zip</label>
                  <input type="text" placeholder="Enter Your Zip Code Here.." class="form-control" name="e_zip" value="<?php echo $employee->e_zip;?>">
                </div>  
              </div>
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Gender</label>
                  <select class="form-control" name="e_gender">
                      <option value="Male" <?php if($employee->e_gender == "Male") { echo'selected';} else {}?>>Male</option>
                      <option value="Female" <?php if($employee->e_gender == "Female") { echo 'selected';} else {}?>>Female</option>
                  </select>
                </div>    
                <div class="col-sm-4 form-group">
                  <label>Birthdate</label>
                  <input type="date" placeholder="Date Of Birth" class="form-control" name="e_birthdate" value="<?php echo $employee->e_birthdate;?>">
                </div>  
                <div class="col-sm-4 form-group">
                  <label>Photo</label>
                  <input type="file" class="form-control" name="e_picture">
                </div>
              </div>                
              <div class="row">
              <div class="col-sm-4 form-group">
                  <label>Position</label>
                  <select class="form-control" name="e_position">
                    <?php foreach ($get_jobpos as $jobpos) {?>
                      <option value="<?php echo $jobpos->jp_name;?>" <?php if($jobpos->jp_name == $employee->e_position) { echo'selected';} else {}?>><?php echo $jobpos->jp_name;?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-sm-4 form-group">
                  <label>Contact Number</label>
                  <input type="text" placeholder="Enter Phone Number Here.." class="form-control" name="e_contact_no" value="<?php echo $employee->e_contact_no;?>">
                </div>    
                <div class="col-sm-4 form-group">
                  <label>Email Address</label>
                  <input type="email" placeholder="Enter Email Address Here.." class="form-control" name="e_email" value="<?php echo $employee->e_email;?>">
                </div>  
              </div>  
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label>Password</label>
                  <input type="password" placeholder="Enter Password Here.." class="form-control" value="<?php echo $employee->e_password;?>">
                </div>    
                <div class="col-sm-6 form-group">
                  <label>Confirm Password</label>
                  <input type="password" placeholder="Enter Password Here.." class="form-control" name="e_password" value="<?php echo $employee->e_password;?>">
                </div>  
              </div>
              <input type="submit" class="btn btn-lg btn-dark-purple" name="submit">        
            </div>
          </form> 
          <?php }?> 
        </div>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



