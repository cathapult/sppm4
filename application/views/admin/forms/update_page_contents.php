<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <?php foreach ($admin_get_page_content_id as $page_contents) {?>
  <title>Update <?php echo $page_contents->pc_title;?> Information</title>

  <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('C:\xampp\htdocs\sppm\application\views\admin\blocks\navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-scissors" aria-hidden="true"></i> Update <?php echo $page_contents->pc_title;?> Information</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Content Management Section > Update <?php echo $page_contents->pc_title;?> Information</p>
            </div>
        </div>

        <div class="row">
          <form action="<?php echo base_url();?>update_controller/admin_update_page_contents" method="post" enctype="multipart/form-data">
          <input type="hidden" name="pc_id" value="<?php echo $page_contents->pc_id;?>">
          <input type="hidden" name="pc_division" value="<?php echo $page_contents->pc_division;?>">
            <div class="col-sm-12">   
              <div class="form-group">
                <h2><?php echo $page_contents->pc_title;?></h2>
              </div> 
              <div class="form-group">
                <label>Description</label>
                <textarea placeholder="Enter Description Here.." rows="3" class="form-control"  style="resize: none;" name="pc_description"><?php echo $page_contents->pc_description;?></textarea>
              </div>       
              <div class="form-group">
                 <select name="pc_state" class="form-control">
                   <option value="SHOW">SHOW</option>
                   <option value="UNSHOW">UNSHOW</option>
                 </select>
              </div>
              <input type="submit" class="btn btn-lg btn-dark-purple" name="submit">  
            </div>
          </form> 
          <?php }?> 
        </div>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



