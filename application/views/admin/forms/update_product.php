<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Update Product Information</title>

  <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('C:\xampp\htdocs\sppm\application\views\admin\blocks\navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-scissors" aria-hidden="true"></i> Update Product</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Inventory Section > Update Product Information</p>
            </div>
        </div>
        strip_tags($_POST[]);
        <div class="row">
          <?php foreach ($admin_get_product_id as $product) { ?>
          <form action="<?php echo base_url();?>update_controller/admin_update_product" method="post" enctype="multipart/form-data">
            <div class="col-sm-12">
            <input type="hidden" name="i_serial_no" value="<?php echo $product->i_serial_no;?>">
              <div class="row">
                <div class="col-sm-12">
                  <h3>Product Information</h3>
                  <hr>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Serial Number</label>
                  <input type="text" placeholder="Enter Serial Number Here.." class="form-control" name="i_serial_no" value="<?php echo $product->i_serial_no;?>" disabled>
                </div>
                <div class="col-sm-4 form-group">
                  <label>Receipt Number</label>
                  <input type="text" placeholder="Enter Receipt Number Here.." class="form-control" name="i_receipt_no" value="<?php echo $product->i_receipt_no;?>">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Price</label>
                  <input type="text" placeholder="Enter Price Here.." class="form-control" name="i_price" value="<?php echo $product->i_price;?>">
                </div>
              </div>
              <div class="row"> 
                <div class="col-sm-4 form-group">
                  <label>Product Name</label>
                  <input type="text" placeholder="Enter Product Name Here.." class="form-control" name="i_product_name" value="<?php echo $product->i_product_name;?>">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Brand Name</label>
                  <input type="text" placeholder="Enter Brand Name Here.." class="form-control" name="i_brand" value="<?php echo $product->i_brand;?>">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Photo</label>
                  <input type="file" class="form-control" name="i_img">
                </div>
              </div>          
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Category</label>
                  <input type="text" placeholder="Enter Category Here..(Scissors,Razor,Powder,...)" class="form-control" name="i_category" value="<?php echo $product->i_category;?>">
                  <!-- <select class="form-control" name="i_category">
                      <option value="Scissors">Scissors</option>
                      <option>Powder</option>
                  </select> -->
                </div>  
                <div class="col-sm-4 form-group">
                  <label>Date Used</label>
                  <input type="date" class="form-control" name="i_date_used" value="<?php echo $product->i_date_used;?>">
                </div>  
                <div class="col-sm-4 form-group">
                  <label>Status</label>
                  <select class="form-control" name="i_status">
                      <option value="Not Used">Not Used</option>
                      <option value="In Used">In Used</option>
                      <option value="Out of Stock">Out of Stock</option>
                  </select>
                </div>  
              </div>
              <div class="row"> 
                <div class="col-sm-6 form-group">
                  <label>Bought Date</label>
                  <input type="date" class="form-control" name="i_bought_date" value="<?php echo $product->i_bought_date;?>">
                </div>
                <div class="col-sm-6 form-group">
                  <label>Bought Time</label>
                  <input type="time" class="form-control" name="i_bought_time" value="<?php echo $product->i_bought_time;?>">
                </div>
              </div>  
              <div class="row">
                <div class="col-sm-12">
                  <h3>Store Information</h3>
                  <hr>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-3 form-group">
                  <label>Store Name</label>
                  <input type="text" placeholder="Enter Store Name Here.." class="form-control" name="i_company_name" value="<?php echo $product->i_company_name;?>">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>Address</label>
                  <input type="text" placeholder="Enter Store Address Here.." class="form-control" name="i_address" value="<?php echo $product->i_address;?>">
                </div>  
                <div class="col-sm-3 form-group">
                  <label>City</label>
                  <input type="text" placeholder="Enter City Bought Here.." class="form-control" name="i_city" value="<?php echo $product->i_city;?>">
                </div>
                <div class="col-sm-3 form-group">
                  <label>Store Contact Number</label>
                  <input type="text" placeholder="Enter Store Contact No. Here.." class="form-control" name=" i_contact_no" value="<?php echo $product->i_contact_no;?>">
                </div>  
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <h3>Buyers Information</h3> 
                  <hr>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label>First Name</label>
                  <input type="text" placeholder="Enter First Name Here.." class="form-control" name="i_firstname" value="<?php echo $product->i_firstname;?>">
                </div>
                <div class="col-sm-6 form-group">
                  <label>Last Name</label>
                  <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="i_lastname" value="<?php echo $product->i_lastname;?>">
                </div>
              </div>  
              <input type="submit" class="btn btn-lg btn-dark-purple" name="submit">    
            </div>
          </form>
          <?php }?> 
        </div>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



