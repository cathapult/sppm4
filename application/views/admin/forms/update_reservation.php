<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Update Reservation</title>

  <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('C:\xampp\htdocs\sppm\application\views\admin\blocks\navigation.php') ;?>


<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-scissors" aria-hidden="true"></i> Update Reservation</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Update Reservation</p>
            </div>
        </div>
        <?php foreach ($get_transactions_id as $transaction) { ?>
        <div class="row">
          <form action="<?php echo base_url();?>update_controller/admin_update_reservation" method="post">
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-12">
                  <h3>Reservation Information</h3>
                  <hr>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label>Customer Name</label>
                  <p><?php echo $transaction->tr_customer_name;?></p>
                </div>
                <div class="col-sm-6 form-group">
                  <label>Checkin Date/Time</label>
                  <p><?php echo $transaction->tr_checkin;?></p>
                </div>
              </div>
              <div class="row"> 
                <div class="col-sm-4 form-group">
                  <label>Services</label>
                  <p><?php echo $transaction->tr_service_names;?></p>
                </div>
                <div class="col-sm-4 form-group">
                  <label>Price</label>
                  <p><?php echo $transaction->tr_prices;?></p>
                </div>
                <div class="col-sm-4 form-group">
                  <label>Total Price</label>
                  <p><?php echo $transaction->tr_total_price;?></p>
                </div>
              </div>          
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label>Category</label>
                  <select class="form-control" name="tr_status">
                      <option value="Reserved" <?php if($transaction->tr_status == "Reserved") { echo'selected';} else {}?>>Reserved</option>
                      <option value="Paid" <?php if($transaction->tr_status == "Paid") { echo'selected';} else {}?>>Paid</option>
                  </select>
                </div>  
                <div class="col-sm-6 form-group">
                  <label>Checkout Time</label>
                  <input type="time" placeholder="Enter Brand Name Here.." class="form-control" name="tr_checkout">
                </div>  
              </div>
              <input type="hidden" name="tr_id" value="<?php echo $transaction->tr_id;?>">
              <input type="submit" class="btn btn-lg btn-dark-purple">    
            </div>
          </form> 
        </div>
        <?php } ?>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



