<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Inventory Section</title>

    <?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('blocks/navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-cubes" aria-hidden="true"></i> Inventory Section</h1>
            </div>
            <div class="col-lg-2" align="right" style="margin-top: 2.4em">
                <a href="#addproduct" data-toggle="modal"><button type="button" class="btn btn-dark-purple btn-sm"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Product</button></a>
            </div>
        </div>
        <br>
        <?php if (isset($message)) { ?>
            <div class="row" id="alert-row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-check-circle" aria-hidden="true"></i> <strong><?php echo $message;?></strong> 
                    </div>
                </div>
            </div>
            <!-- /.row -->
        <?php } ?>

        <!-- <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-cubes fa-fw"></i> Most Items Used</h3>
                    </div>
                    <div class="panel-body">
                        <div id="PieChartItemsMostUsed" style="height: 400px; width: 100%;"></div>
                        <div class="text-right">
                            <a href="#">View Most Used Items <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <ul class="nav nav-pills nav-justified">
            <li class="active"><a data-toggle="tab" href="#active">Quantity</a></li>
            <li><a data-toggle="tab" href="#inactive">Details</a></li>
        </ul>
        <div class="tab-content">
            <div id="active" class="tab-pane fade in active">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-lg-10">
                                        <div class="panel-title">
                                            <h4><i class="fa fa-cubes" aria-hidden="true"></i> Inventory Table</h4> 
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table" id="quantityTable">
                                        <thead>
                                            <tr>
                                                <th width="10%">#</th>
                                                <th width="70%">Product Name</th>
                                                <th width="20%">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $ctr=1; foreach ($itemquantityandname as $goi) {?>
                                            <tr>
                                                <td><?php echo $ctr;?></td>
                                                <td><?php echo $goi['i_product_name'];?></td>
                                                <td style="padding-left:25px"><?php echo $goi['i_product_count'];?></td>
                                            </tr>
                                            <?php $ctr++;}?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="#">PAGINATION</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="inactive" class="tab-pane fade">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-lg-10">
                                        <div class="panel-title">
                                            <h4><i class="fa fa-cubes" aria-hidden="true"></i> Inventory Table</h4> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table" id="inventoryTable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Product Name</th>
                                                <th>Brand</th>
                                                <th>Price</th>
                                                <th>Category</th>
                                                <th>Date Used</th>
                                                <th>Status</th>
                                                <th>Options</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $ctr=1; foreach ($get_inventory as $inventory) {?>
                                            <tr>
                                                <td><?php echo $ctr;?></td>
                                                <td><?php echo $inventory->i_product_name;?></td>
                                                <td><?php echo $inventory->i_brand;?></td>
                                                <td>PHP <?php echo $inventory->i_price;?></td>
                                                <td><?php echo $inventory->i_category;?></td>
                                                <td><?php 
                                                        if(empty($inventory->i_date_used)) {
                                                            echo $inventory->i_date_used;
                                                        }
                                                        else {
                                                            echo 'Product Not Used';
                                                        }
                                                    ?>        
                                                </td>
                                                <td><?php echo $inventory->i_status;?></td>
                                                <td>
                                                    <ul class="table-options">
                                                        <li>
                                                            <a href="#view<?php echo $inventory->i_serial_no;?>" class="btn btn-default" id="menu-toggle" data-toggle="modal" style="color:#520e25">View</a>
                                                            <!-- Modal -->
                                                            <div id="view<?php echo $inventory->i_serial_no;?>" class="modal fade" role="dialog">
                                                              <div class="modal-dialog modal-lg">
                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                  <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h2 class="modal-title title-color">
                                                                    <?php if($inventory->i_img == NULL){
                                                                            echo "No File";
                                                                        } 
                                                                        else { ?>
                                                                        <img src="<?php echo base_url();?>/media/inventory/<?php echo $inventory->i_img;?>" class="img-rounded" width="40px" height="40px">
                                                                    <?php } ?>
                                                                    &nbsp; <?php echo $inventory->i_product_name;?>
                                                                    </h2>
                                                                  </div>
                                                                  <div class="modal-body">
                                                                      <div class="row">
                                                                        <div class="col-sm-12">
                                                                          <h3>Product Information</h3>
                                                                        </div>
                                                                      </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-6 form-group">
                                                                            <h5>Serial Number: <?php echo $inventory->i_serial_no;?></h5>
                                                                        </div>
                                                                        <div class="col-sm-6 form-group">
                                                                            <h5>Receipt Number: <?php echo $inventory->i_receipt_no;?></h5>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="row">
                                                                        <div class="col-sm-6 form-group">
                                                                            <h5>Price: PHP <?php echo $inventory->i_price;?></h5>
                                                                        </div>
                                                                          <div class="col-sm-6 form-group">
                                                                            <h5>Product Name: <?php echo $inventory->i_product_name;?></h5>
                                                                        </div>
                                                                    </div>  
                                                                    <div class="row">
                                                                      
                                                                        <div class="col-sm-6 form-group">
                                                                            <h5>Brand Name: <?php echo $inventory->i_brand;?></h5>
                                                                        </div>
                                                                        <div class="col-sm-6 form-group">
                                                                            <h5>Date Used: <?php echo $inventory->i_date_used;?></h5>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="row"> 
                                                                        <div class="col-sm-6 form-group">
                                                                          <h5>Bought Date: <?php echo $inventory->i_bought_date;?></h5>
                                                                        </div>
                                                                        <div class="col-sm-6 form-group">
                                                                          <h5>Bought Time: <?php echo $inventory->i_bought_time;?></h5>
                                                                        </div>
                                                                    </div>  
                                                                     <div class="row"> 
                                                                        <div class="col-sm-12 form-group">
                                                                            <h5>Status: <?php echo $inventory->i_status;?></h5>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                          <h3>Store Information</h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-6 form-group">
                                                                          <h5>Store Name: <?php echo $inventory->i_company_name;?></h5>
                                                                        </div>  
                                                                        <div class="col-sm-6 form-group">
                                                                          <h5>Store Contact Number: <?php echo $inventory->i_contact_no;?></h5>
                                                                        </div>  
                                                                    </div>  
                                                                    <div class="row">
                                                                        <div class="col-sm-12 form-group">
                                                                          <h5>Address: <?php echo $inventory->i_address;?></h5>
                                                                        </div> 
                                                                    </div>  
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <h3>Buyers Information</h3> 
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-6 form-group">
                                                                          <h5>First Name: <?php echo $inventory->i_firstname;?></h5>
                                                                        </div>
                                                                        <div class="col-sm-6 form-group">
                                                                          <h5>Last Name: <?php echo $inventory->i_lastname;?></h5>
                                                                        </div>
                                                                  </div>        
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <a href="#update<?php echo $inventory->i_serial_no;?>" class="btn btn-dark-purple" id="menu-toggle" data-toggle="modal">Update</a>
                                                            <!-- Modal -->
                                                            <div id="update<?php echo $inventory->i_serial_no;?>" class="modal fade" role="dialog">
                                                              <div class="modal-dialog modal-lg">
                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                  <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h2 class="modal-title title-color">
                                                                    <?php if($inventory->i_img == NULL){
                                                                            echo "No File";
                                                                        } 
                                                                        else { ?>
                                                                        <img src="<?php echo base_url();?>/media/inventory/<?php echo $inventory->i_img;?>" class="img-rounded" width="40px" height="40px">
                                                                    <?php } ?>
                                                                    &nbsp; <?php echo $inventory->i_product_name;?>
                                                                    </h2>
                                                                  </div>
                                                                  <form action="<?php echo base_url();?>update_controller/admin_update_product_form/<?php echo $inventory->i_serial_no;?>" method="post">
                                                                  <div class="modal-body">
                                                                      <div class="row">
                                                                        <div class="col-sm-12">
                                                                          <h4>Product Information</h4>
                                                                        </div>
                                                                      </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12 form-group">
                                                                            <label>Product Image</label>
                                                                            <input type="file" name="i_img" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-4 form-group">
                                                                            <label>Serial Number</label>
                                                                            <input type="text" placeholder="Enter Serial Number Here.." class="form-control" name="i_serial_no" value="<?php echo $inventory->i_serial_no;?>" disabled>
                                                                        </div>
                                                                        <div class="col-sm-4 form-group">
                                                                            <label>Receipt Number</label>
                                                                            <input type="text" placeholder="Enter Receipt Number Here.." class="form-control" name="i_receipt_no" value="<?php echo $inventory->i_receipt_no;?>" required>
                                                                        </div>
                                                                        <div class="col-sm-4 form-group">
                                                                            <label>Price</label>
                                                                            <input type="text" placeholder="Enter Price Here.." class="form-control" name="i_price" value="<?php echo $inventory->i_price;?>" required>
                                                                        </div>
                                                                    </div>  
                                                                    <div class="row">
                                                                        <div class="col-sm-4 form-group">
                                                                            <label>Product Name</label>
                                                                             <input type="text" placeholder="Enter Product Name Here.." class="form-control" name="i_product_name" value="<?php echo $inventory->i_product_name;?>" required>
                                                                        </div>
                                                                        <div class="col-sm-4 form-group">
                                                                            <label>Brand Name</label>
                                                                            <input type="text" placeholder="Enter Brand Name Here.." class="form-control" name="i_brand" value="<?php echo $inventory->i_brand;?>" required>
                                                                        </div>
                                                                        <div class="col-sm-4 form-group">
                                                                            <label>Status</label>
                                                                            <select class="form-control" name="i_status" required>
                                                                              <option value="Not Used" <?php if($inventory->i_status == 'Not Used') echo 'selected';?>>Not Used</option>
                                                                              <option value="In Used" <?php if($inventory->i_status == 'In Used') echo 'selected';?>>In Used</option>
                                                                              <option value="Out of Stock" <?php if($inventory->i_status == 'Out of Stock') echo 'selected';?>>Out of Stock</option>
                                                                          </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row"> 
                                                                        <div class="col-sm-4 form-group">
                                                                          <label>Bought Date</label>
                                                                          <input type="date" class="form-control" name="i_bought_date" value="<?php echo $inventory->i_bought_date;?>" required>
                                                                        </div>
                                                                        <div class="col-sm-4 form-group">
                                                                          <label>Bought Time</label>
                                                                          <input type="time" class="form-control" name="i_bought_time" value="<?php echo $inventory->i_bought_time;?>" required>
                                                                        </div>
                                                                        <div class="col-sm-4 form-group">
                                                                            <label>Date Used</label>
                                                                            <input type="date" class="form-control" name="i_date_used" value="<?php echo $inventory->i_date_used;?>">
                                                                        </div>
                                                                    </div>  
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                          <h4>Store Information</h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-4 form-group">
                                                                          <label>Store Name</label>
                                                                          <input type="text" placeholder="Enter Store Name Here.." class="form-control" name="i_company_name" value="<?php echo $inventory->i_company_name;?>" required>
                                                                        </div>  
                                                                        <div class="col-sm-4 form-group">
                                                                          <label>Address</label>
                                                                          <input type="text" placeholder="Enter Store Address Here.." class="form-control" name="i_address" value="<?php echo $inventory->i_address;?>" required>
                                                                        </div> 
                                                                        <div class="col-sm-4 form-group">
                                                                          <label>Store Contact Number</label>
                                                                          <input type="text" placeholder="Enter Store Contact No. Here.." class="form-control" name=" i_contact_no" value="<?php echo $inventory->i_contact_no;?>" required>
                                                                        </div>  
                                                                    </div>  
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <h4>Buyers Information</h4> 
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-6 form-group">
                                                                          <label>First Name</label>
                                                                          <input type="text" placeholder="Enter First Name Here.." class="form-control" name="i_firstname" value="<?php echo $inventory->i_firstname;?>" required>
                                                                        </div>
                                                                        <div class="col-sm-6 form-group">
                                                                          <label>Last Name</label>
                                                                          <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="i_lastname" value="<?php echo $inventory->i_lastname;?>" required>
                                                                        </div>
                                                                    </div>        
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                   <div align="center"> <input type="submit" class="btn btn-dark-purple" name="submit"></div> 
                                                                  </div>
                                                                  </form>
                                                                </div>
                                                              </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <?php $ctr++;}?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="#">PAGINATION</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="addproduct" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="modal-title title-color">
            Add A New Product
            </h3>
          </div>
          <form action="<?php echo base_url();?>update_controller/admin_update_product_form" method="post">
          <div class="modal-body">
              <div class="row">
                <div class="col-sm-12">
                  <h3>Product Information</h3>
                  
                </div>
              </div>
            <div class="row">
                <div class="col-sm-12 form-group">
                    <label>Product Image</label>
                    <input type="file" name="i_img" required>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label>Serial Number</label>
                    <input type="text" placeholder="Enter Serial Number Here.." class="form-control" name="i_serial_no" required>
                </div>
                <div class="col-sm-4 form-group">
                    <label>Receipt Number</label>
                    <input type="text" placeholder="Enter Receipt Number Here.." class="form-control" name="i_receipt_no" required>
                </div>
                <div class="col-sm-4 form-group">
                    <label>Price</label>
                    <input type="text" placeholder="Enter Price Here.." class="form-control" name="i_price" required>
                </div>
            </div>  
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label>Product Name</label>
                     <input type="text" placeholder="Enter Product Name Here.." class="form-control" name="i_product_name" required>
                </div>
                <div class="col-sm-4 form-group">
                    <label>Brand Name</label>
                    <input type="text" placeholder="Enter Brand Name Here.." class="form-control" name="i_brand" required>
                </div>
                <div class="col-sm-4 form-group">
                    <label>Status</label>
                    <select class="form-control" name="i_status" required>
                      <option value="Not Used">Not Used</option>
                      <option value="In Used">In Used</option>
                      <option value="Out of Stock">Out of Stock</option>
                  </select>
                </div>
            </div>
            <div class="row"> 
                <div class="col-sm-4 form-group">
                  <label>Bought Date</label>
                  <input type="date" class="form-control" name="i_bought_date" required>
                </div>
                <div class="col-sm-4 form-group">
                  <label>Bought Time</label>
                  <input type="time" class="form-control" name="i_bought_time" required>
                </div>
                <div class="col-sm-4 form-group">
                    <label>Date Used</label>
                    <input type="date" class="form-control" name="i_date_used">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                  <h3>Store Information</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Store Name</label>
                  <input type="text" placeholder="Enter Store Name Here.." class="form-control" name="i_company_name" required>
                </div>  
                <div class="col-sm-4 form-group">
                  <label>Address</label>
                  <input type="text" placeholder="Enter Store Address Here.." class="form-control" name="i_address" required>
                </div> 
                <div class="col-sm-4 form-group">
                  <label>Store Contact Number</label>
                  <input type="text" placeholder="Enter Store Contact No. Here.." class="form-control" name=" i_contact_no" required>
                </div>  
            </div>  
            <div class="row">
                <div class="col-sm-12">
                    <h3>Buyers Information</h3> 
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                  <label>First Name</label>
                  <input type="text" placeholder="Enter First Name Here.." class="form-control" name="i_firstname" required>
                </div>
                <div class="col-sm-6 form-group">
                  <label>Last Name</label>
                  <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="i_lastname" required>
                </div>
            </div>        
          </div>
          <div class="modal-footer" align="center">
            <div align="center"><input type="submit" class="btn btn-dark-purple btn-lg" name="submit">  </div>  
          </div>
          </form>
        </div>
      </div>
    </div>

    <script type="text/javascript">
        window.onload = function() {
            var chart = new CanvasJS.Chart("PieChartItemsMostUsed", {
                title:{
                    text: "Products Mostly Used",
                    fontColor: "white"
                },
                        animationEnabled: true,
                legend:{
                    verticalAlign: "center",
                    horizontalAlign: "left",
                    fontSize: 20,
                    fontColor: "white",
                    fontFamily: "Helvetica"        
                },
                theme: "theme1",
                backgroundColor: "#520e25",
                data: [
                {        
                    type: "pie",   
                    indexLabelFontColor: "white",    
                    indexLabelFontFamily: "Helvetica",       
                    indexLabelFontSize: 20,
                    indexLabel: "{label} {y}%",
                    startAngle:-20,      
                    showInLegend: true,
                    toolTipContent:"{legendText} {y}%",
                    dataPoints: [
                        {  y: 83.24, legendText:"Tresemme", label: "Tresemme" },
                        {  y: 8.16, legendText:"Generic Shampoo", label: "Generic Shampoo" },
                        {  y: 4.67, legendText:"Bigen", label: "Bigen" },
                        {  y: 1.67, legendText:"Matrix" , label: "Matrix"},       
                        {  y: 0.98, legendText:"Others" , label: "Others"}
                    ]
                }
                ]
            });
            chart.render();
        }
    </script>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



