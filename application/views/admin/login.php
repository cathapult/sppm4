<!DOCTYPE html>
<html>
<?php
if (isset($this->session->userdata['logged_in'])) {


header("location: http://localhost/sppm/Admin_Login/admin_login_process");
}
?>
<head>
  <title>Welcome to <?php echo $get_page_contents_storename[0]->pc_description;?> Management</title>
  <?php include('design/css.php') ;?>
  <style type="text/css">

div.well{
  height: 250px;
} 

.Absolute-Center {
  margin: auto;
  position: absolute;
  top: 0; left: 0; bottom: 0; right: 0;
}

.Absolute-Center.is-Responsive {
  width: 50%; 
  height: 50%;
  min-width: 200px;
  max-width: 400px;
  padding: 40px;
}

#logo-container{
  margin: auto;
  margin-bottom: 5px;
  width:200px;
  height:150px;
  /*background-image:url('<?php echo base_url();?>media/pagecontents/genericparlor.png');*/
}
  </style>
</head>

<body style="background-color: #311616">

<div class="container">
  <div class="row">
    <div class="Absolute-Center is-Responsive">
      <div id="logo-container"><img src="<?php echo base_url();?>media/pagecontents/<?php echo $get_page_contents_storelogo[0]->pc_img;?>" id="logo-container"></div>
      <?php if (isset($message)) { ?>
            <div class="row" id="alert-row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-check-circle" aria-hidden="true"></i> <strong><?php echo $message;?></strong> 
                    </div>
                </div>
            </div>
            <!-- /.row -->
        <?php } ?>
      <div class="col-sm-12 col-md-10 col-md-offset-1">
        <form action="<?php echo base_url();?>admin/admin_login_process" id="loginForm" method="POST">
          <div class="form-group input-group">
            <span class="input-group-addon dark-purple bg-purple"><i class="glyphicon glyphicon-user"></i></span>
            <input class="form-control" type="email" name='e_email' placeholder="email"/>          
          </div>
          <div class="form-group input-group">
            <span class="input-group-addon dark-purple bg-purple"><i class="glyphicon glyphicon-lock"></i></span>
            <input class="form-control" type="password" name='e_password' placeholder="password"/>     
          </div>
          <!-- <div class="checkbox">
            <label>
              <input type="checkbox"> I agree to the <a href="#">Terms and Conditions</a>
            </label>
          </div> -->
          <div class="form-group">
            <!-- <button type="button" class="btn btn-dark-purple btn-block">Login</button> -->
            <input type="submit" value="Login " name="submit" class="btn btn-dark-purple btn-block">
          </div>
          <!-- <div class="form-group text-center">
            <a href="#">Forgot Password</a>&nbsp;|&nbsp;<a href="#">Support</a>
          </div> -->
        </form>        
      </div>  
    </div>    
  </div>
</div>
</body>
<?php include('design/js.php') ;?>
</html>