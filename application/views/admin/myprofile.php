<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>My Profile</title>

	<?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('blocks/navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-user" aria-hidden="true"></i> My Profile</h1>
            </div>
          
        </div>



        <div class="row">
                <div class="col-lg-4">
                    <p>&nbsp;</p>
                </div>
            <div class="col-lg-4">
                <div class="panel panel-default" align="center" style="padding-top:10px; padding-bottom: 10px; border:none;">
                  <?php if($e_picture == NULL) { ?>
                <img src="<?php echo base_url();?>media/default-user.png" class=" img-circle text-center" width="200" height="200">
                <?php } elseif ($e_picture !== NULL) { ?>
                <img src="<?php echo base_url();?>media/employee/<?php echo $e_picture;?>" class="img-circle text-center" width="200" height="200">
                <?php } ?>  
                </div>
            </div>
                <div class="col-lg-4">
                    <p>&nbsp;</p>
              </div>
        </div>
        <div align="center">
            <div class="panel panel-default" align="center" style="border-radius:12px;width:80%;">
                <h3><?php echo $e_firstname; ?></h3>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
window.onload = function() {
    var chart = new CanvasJS.Chart("LineChartMonthlySale",
        {

            title:{
                text: "Sales For The Month of <?php echo date('F');?>",
                fontColor: "white",
                fontSize: 30
            },
                        animationEnabled: true,
            axisX:{

                gridColor: "Silver",
                tickColor: "silver",
                labelFontColor: "white",
                valueFormatString: "DD/MMM"

            },                        
                        toolTip:{
                          shared:true
                        },
            theme: "theme2",
            backgroundColor: "#520e25",
            axisY: {
                valueFormatString: "PHP 0",
                labelFontColor: "white",
                gridColor: "Silver",
                tickColor: "silver"
            },
            legend:{
                verticalAlign: "center",
                horizontalAlign: "right"
            },
            data: [
            {        
                type: "line",
                showInLegend: true,
                lineThickness: 2,
                name: "Sales",
                markerType: "square",
                color: "#F08080",
                dataPoints: [
                <?php 
                    foreach ($get_sales as $sales) { 
                        $year = substr($sales->s_date,0,4);
                        $month = substr($sales->s_date,5,2);
                        $day = substr($sales->s_date,8,2);
                        if (date('m') == substr($sales->s_date,5,2)) { ?>
                { x: new Date(<?php echo $year;?>,0,<?php echo $day;?>), y: <?php echo $sales->s_sale;?> },
                <?php } }?>
                ]
            }

            
            ],
          legend:{
            cursor:"pointer",
            itemclick:function(e){
              if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
              }
              else{
                e.dataSeries.visible = true;
              }
              chart.render();
            }
          }
        });
        chart.render();
}
</script>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



