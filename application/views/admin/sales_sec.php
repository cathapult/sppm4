<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Sales Section</title>

	<?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('blocks/navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-line-chart" aria-hidden="true"></i> Sales Section</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Sales Section</p>
            </div>
        </div>
        <?php if (isset($message)) { ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="fa fa-info-circle"></i>  <strong><?php echo $message;?></strong> 
                </div>
            </div>
        </div>
        <!-- /.row -->
        <?php } ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Transactions Table For The Month of <?php echo date('F');?></h3>
                    </div>
                    <div class="panel-body">
                        <div id="LineChartMonthlySale" style="height: 400px; width: 100%;"></div>
                        <div class="text-right">
                            <!-- <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
               <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Transactions Table for <?php echo date('l, M-d');?></h3>
                    </div>
                    <div class="panel-body">
                    <?php if(empty($get_transactions_date[0]->tr_date)) { ?>
                        <h1 class="text-center">No Current Transactions Yet!</h1>
                        <?php } else { ?>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Options</th>
                                        <th>Date/Time</th>
                                        <th>Customer Name</th>
                                        <th>Services</th>
                                        <th>Stylist</th>
                                        <th>Price</th>
                                        <th>Total Price</th>
                                        <th>Status</th>
                                        <th>Checkin</th>
                                        <th>Checkout</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($get_transactions as $tr){?>
                                    <tr>
                                        <td><a href="<?php echo base_url();?>update_controller/admin_update_reservation_form/<?php echo $tr->tr_id;?>" class="btn btn-purple" id="menu-toggle">Update</a></td>
                                        <td><?php echo $tr->tr_date;?>/<?php echo $tr->tr_time;?></td>
                                        <td><?php echo $tr->tr_customer_name;?></td>
                                        <td><?php echo $tr->tr_service_names;?></td>
                                        <td><?php echo $tr->tr_employee_names;?></td>
                                        <td>PHP <?php echo $tr->tr_prices;?></td>
                                        <td>PHP <?php echo $tr->tr_total_price;?></td>
                                        <td>
                                            <?php 
                                                $status = $tr->tr_status;
                                                if($status == 'Reserved') {
                                            ?>
                                            <p style="color: orange"><?php echo $status;?></p> 
                                            <?php } else { ?>
                                            <p style="color: green"><?php echo $status;?></p>
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $tr->tr_checkin;?></td>
                                        <td><?php echo $tr->tr_checkout;?></td>
                                    </tr>
                                    <?php } ?> 
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <?php 
                                $tr_date = $get_transactions[0]->tr_date;
                                if(empty($tr_date)) {
                                $total_sales = number_format($add_transactions[0]->tr_total_price);
                                if(date('Y-m-d') !== $tr_date) { ?>
                            <form action="<?php echo base_url();?>add_controller/admin_tr_to_sales" method="post">
                                <input type="hidden" name="s_date" value="<?php echo $tr_date; ?>">
                                <input type="hidden" name="s_sale" value="<?php echo $total_sales; ?>">
                                <input type="submit" value="Submit to Sales" class="btn btn-purple">    
                            </form>
                            <?php } } ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
               <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Total Expenditures Table</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Expenditures</td>
                                        <?php $total_expenditures_inventory = number_format($total_expenditures_inventory[0]->i_price); ?>
                                        <td>PHP <?php echo $total_expenditures_inventory;?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
window.onload = function() {
    var chart = new CanvasJS.Chart("LineChartMonthlySale",
        {

            title:{
                text: "Sales For The Month of <?php echo date('F');?>",
                fontColor: "white",
                fontSize: 30
            },
                        animationEnabled: true,
            axisX:{

                gridColor: "Silver",
                tickColor: "silver",
                labelFontColor: "white",
                valueFormatString: "DD/MMM"

            },                        
                        toolTip:{
                          shared:true
                        },
            theme: "theme2",
            backgroundColor: "#520e25",
            axisY: {
                valueFormatString: "PHP 0",
                labelFontColor: "white",
                gridColor: "Silver",
                tickColor: "silver"
            },
            legend:{
                verticalAlign: "center",
                horizontalAlign: "right"
            },
            data: [
            {        
                type: "line",
                showInLegend: true,
                lineThickness: 2,
                name: "Sales",
                markerType: "square",
                color: "#F08080",
                dataPoints: [
                <?php 
                    foreach ($get_sales as $sales) { 
                        $year = substr($sales->s_date,0,4);
                        $month = substr($sales->s_date,5,2);
                        $day = substr($sales->s_date,8,2);
                        if (date('m') == substr($sales->s_date,5,2)) { ?>
                { x: new Date(<?php echo $year;?>,0,<?php echo $day;?>), y: <?php echo $sales->s_sale;?> },
                <?php } }?>
                ]
            }

            
            ],
          legend:{
            cursor:"pointer",
            itemclick:function(e){
              if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
              }
              else{
                e.dataSeries.visible = true;
              }
              chart.render();
            }
          }
        });
        chart.render();
}
</script>
    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



