<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Services Section</title>

	<?php include('design/css.php') ;?>

</head>

<body>

<div id="wrapper">

<?php include('blocks/navigation.php') ;?>

<!-- MAIN BODY -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <h1><a href="#menu-toggle" class="btn btn-dark-purple btn-sidebad-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>  <i class="fa fa-scissors" aria-hidden="true"></i> Services Section</h1>
            </div>
            <div class="col-lg-2" style="margin-top: 2.4em">
                <p><?php echo $get_page_contents_storename[0]->pc_description;?> > Services Section</p>
            </div>
        </div>

        <?php if (isset($message)) { ?>
            <div class="row" id="alert-row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-check-circle" aria-hidden="true"></i> <strong><?php echo $message;?></strong> 
                    </div>
                </div>
            </div>
            <!-- /.row -->
        <?php } ?>
        <ul class="nav nav-pills nav-justified">
            <li class="active"><a data-toggle="tab" href="#services">Services</a></li>
            <li><a data-toggle="tab" href="#haircolor">Hair Color</a></li>
            <li><a data-toggle="tab" href="#promos">Promos</a></li>
        </ul>
    <div class="tab-content">
        <div id="services" class="tab-pane fade in active">
        <div class="row">
            <div class="col-lg-12">
                <div ="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="panel-title">
                                    <h4><i class="fa fa-scissors" aria-hidden="true"></i> Services Table</h4> 
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <a href="<?php echo base_url();?>add_controller/admin_add_service_form_sec"><button type="button" class="btn btn-dark-purple btn-sm"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Service</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table-striped" id="serviceTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Category</th>
                                        <th>Price</th>
                                        <th>Picture</th>
                                        <th>Status</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $ctr=1; foreach ($get_services as $services) {?>
                                    <tr>
                                        <td><?php echo $ctr;?></td>
                                        <td><?php echo $services->s_name;?></td>
                                        <td><?php echo $services->s_description;?></td>
                                        <td><?php echo $services->s_category;?></td>
                                        <td><?php echo $services->s_price;?></td>
                                        <td>
                                            <?php if($services->s_picture == NULL){
                                                echo "No File";
                                            } 
                                            else { ?>
                                            <img src="<?php echo base_url();?>/media/service/<?php echo $services->s_picture;?>" width="80px" height="80px">
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $services->s_status;?></td>
                                        <td><a href="<?php echo base_url();?>update_controller/admin_update_service_form/<?php echo $services->s_id;?>" class="btn btn-dark-purple" id="menu-toggle">Update</a></td>
                                    </tr>
                                    <?php $ctr++; }?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div></div>
        <div id="haircolor" class="tab-pane fade in">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="panel-title">
                                    <h4><i class="fa fa-tint" aria-hidden="true"></i> Hair Color Table</h4> 
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <a href="<?php echo base_url();?>add_controller/admin_add_hair_color_form_sec"><button type="button" class="btn btn-dark-purple btn-sm"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Hair Color</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table-striped" id="hairColor">
                                <thead>
                                    <tr>
                                        <th>Color ID</th>
                                        <th>Color Name</th>
                                        <th>Image</th>
                                        <th width="10%">Options</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($get_haircolor as $haircolor) {?>
                                    <tr>
                                        
                                        <td><?php echo $haircolor->hc_id;?></td>
                                        <td><?php echo $haircolor->hc_name;?></td>
                                        <td>
                                            <?php if($haircolor->hc_photo == NULL){
                                                echo "No File";
                                            } 
                                            else { ?>
                                            <img src="<?php echo base_url();?>/media/haircolor/<?php echo $haircolor->hc_photo;?>" width="80px" height="80px">
                                            <?php } ?>
                                        </td>
                                        <td><a href="<?php echo base_url();?>update_controller/admin_update_haircolor_form/<?php echo $haircolor->hc_id;?>" class="btn btn-dark-purple" id="menu-toggle">Update</a></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="#">PAGINATION</i></a>
                        </div>
                    </div>
                </div></div>
            </div>
            </div>
            <div id="promos" class="tab-pane fade in">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="panel-title">
                                    <h4><i class="fa fa-star-half-o" aria-hidden="true"></i> Promos Table</h4> 
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <a href="<?php echo base_url();?>add_controller/admin_add_promo_form_sec"><button type="button" class="btn btn-dark-purple btn-sm"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Promo</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table-striped" id="promosTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Promo Name</th>
                                        <th>Price</th>
                                        <th>Image</th>
                                        <th>Date & Time Created</th>

                                        <th>Status</th>
                                        <th width="10%">Options</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $ctr=1; foreach ($get_promos as $promos) {?>
                                    <tr>
                                        <td><?php echo $ctr;?></td>
                                        <td><?php echo $promos->pr_name;?></td>
                                        <td><?php echo $promos->pr_price;?></td>
                                        <td>
                                            <?php if($promos->pr_img == NULL){
                                                echo "No File";
                                            } 
                                            else { ?>
                                            <img src="<?php echo base_url();?>/media/promo/<?php echo $promos->pr_img;?>" width="80px" height="80px">
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $promos->pr_inputdate;?> <?php echo $promos->pr_inputtime;?></td>

                                        <td><?php echo $promos->pr_status;?></td>
                                        <td><a href="<?php echo base_url();?>update_controller/admin_update_promo_form/<?php echo $promos->pr_id;?>" class="btn btn-dark-purple" id="menu-toggle">Update</a></td>
                                    </tr>
                                    <?php $ctr++; }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="#">PAGINATION</a>
                        </div>
                    </div>
                </div>        
            </div>
            </div>
            </div>
        </div>

    </div>
    <!-- /#page-content-wrapper -->
<!-- END OF MAIN BODY -->
</div>
<!-- /#wrapper -->

</body>
<?php include('design/js.php') ;?>
</html>



