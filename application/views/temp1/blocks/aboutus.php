<!-- About Us Section -->
<section id="aboutus"  style="background-color: #262626;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading title-color">About Us</h2>
                <hr>
                <!-- <h3 class="section-subheading">Lorem ipsum dolor sit amet consectetur.</h3> -->
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                <span class="fa-stack fa-4x aboutus-logo-anim">
                    <i class="fa fa-circle-thin fa-stack-2x aboutus-logo"></i>
                    <i class="fa fa-lightbulb-o fa-stack-1x fa-inverse aboutus-logo"></i>
                </span>
                <?php foreach ($get_aboutus_v as $v) {?>
                <h4 class="service-heading"><?php echo $v->pc_title;?></h4>
                <p class="text-muted"><?php echo $v->pc_description;?></p>
                <?php }?>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x aboutus-logo-anim">
                    <i class="fa fa-circle-thin fa-stack-2x aboutus-logo"></i>
                    <i class="fa fa-info fa-stack-1x fa-inverse aboutus-logo"></i>
                </span>
                <?php foreach ($get_aboutus_cp as $cp) {?>
                <h4 class="service-heading"><?php echo $cp->pc_title;?></h4>
                <p class="text-muted"><?php echo $cp->pc_description;?></p>
                <?php }?>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x aboutus-logo-anim">
                    <i class="fa fa-circle-thin fa-stack-2x aboutus-logo"></i>
                    <i class="fa fa-handshake-o fa-stack-1x fa-inverse aboutus-logo"></i>
                </span>
                <?php foreach ($get_aboutus_m as $m) {?>
                <h4 class="service-heading"><?php echo $m->pc_title;?></h4>
                <p class="text-muted"><?php echo $m->pc_description;?></p>
                <?php }?>
            </div>
        </div>
    </div>
</section>