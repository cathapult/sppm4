<!-- Contact Section -->
<section id="contactus">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading title-color">Contact Us</h2>
                <hr>
                <!-- <h3 class="section-subheading">Lorem ipsum dolor sit amet consectetur.</h3> -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-6">
                            <iframe style="height:410px;width:530px;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Salvador+Estate,+Parañaque,+NCR,+Philippines&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe>
                        </div>
                        <div class="col-md-6">
                            <h4 class="title-color"><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;&nbsp; Principal Address:</h4>
							<p>193 Maximina drive Balong Bato Quezon City, 1100 Metro Manila</p>
							<h4 class="title-color"><i class="fa fa-phone" aria-hidden="true"></i> &nbsp;&nbsp; Feel free to call us:</h4>
							<p>(09) 123-4567</p>
							<h4 class="title-color"><i class="fa fa-sun-o" aria-hidden="true"></i>&nbsp;&nbsp; Open Hour:</h4>
							<p>9AM - 6PM; Monday - Sunday</p>
                        </div>
                        <div class="clearfix"></div>
                        <!-- <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button type="submit" class="btn btn-xl">Send Message</button>
                        </div> -->
                    </div>
            </div>
        </div>
    </div>
</section>