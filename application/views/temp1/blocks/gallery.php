<!-- Gallery Section -->
<section id="gallery" class="bg-light-gray" style="background-color: #262626;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading title-color">Gallery</h2>
                <hr>
                <!-- <h3 class="section-subheading">Lorem ipsum dolor sit amet consectetur.</h3> -->
            </div>
        </div>
        <div class="row">
        <?php foreach ($get_page_contents_g as $gallery) { ?>
            <div class="chocolat-parent" data-chocolat-title="Salon Images">
                <div class="col-md-4 col-sm-6 gallery-item">
                    <a class="chocolat-image gallery-link" href="<?php echo base_url();?>media/gallery/<?php echo $gallery->pc_img;?>" title="caption image 1">
                        <div class="gallery-hover">
                            <div class="gallery-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-circle" src="<?php echo base_url();?>media/gallery/<?php echo $gallery->pc_img;?>"  alt="" height="400" width="370">
                    </a>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
</section> 