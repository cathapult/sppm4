<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><?php echo $get_page_contents_storename[0]->pc_description;?></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#aboutus">About Us</a>
                </li>
                <li>
                    <a href="#services">Services</a>
                </li>
                <?php if(!isset($this->session->userdata['logged_in'])) { ?>
                <li>
                    <a href="#gallery">Gallery</a>
                </li>
                <?php } ?>
                <li>
                    <a href="#contactus">Contact Us</a>
                </li>
                <?php if(isset($this->session->userdata['logged_in'])) { ?>
                <li>
                    <a href="#reserve">Reserve Now</a>
                </li>
                <?php } ?>
            </ul>
            <?php if(isset($this->session->userdata['logged_in'])) { ?>
            <ul class="nav navbar-nav navbar-right">
              <li style="margin-top: 10px; margin-right: 6px">
                  <?php if($c_picture == NULL) { ?>
                    <img src="<?php echo base_url();?>media/default-user.png" class="img-circle" width="33" height="33">&nbsp;
                    Hello&nbsp;<i class="title-color title-font"><?php echo $c_firstname;?></i>&nbsp;!
                  <?php } elseif ($c_picture !== NULL) { ?>
                    <img src="<?php echo base_url();?>media/customer/<?php echo $c_picture;?>" class="img-circle" width="33" height="33">&nbsp;
                    Hello&nbsp;<i class="title-color title-font"><?php echo $c_firstname;?></i>&nbsp;!
                  <?php } ?>
              </li>
              <li><a href="<?php echo base_url();?>customer_ui/logout" data-toggle="modal"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
            </ul>
            <?php } else { ?> 
            <ul class="nav navbar-nav navbar-right">
		      <li><a href="#registeration" data-toggle="modal"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
		      <li><a href="#loginmodal" data-toggle="modal"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
		    </ul>
            <?php } ?>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<!-- Modal -->
<div id="loginmodal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="background-color: #262626;">

    <!-- Modal content-->
    <div class="modal-content" style="background-color: #262626;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title-color"><h1 class="text-center">Login</h1></h4>
      </div>
      <div class="modal-body text-center">
        <form action="<?php echo base_url();?>customer_ui/login_process" id="loginForm" method="POST">
            <div class="form-group">
                <input type="text" class="form-control" name="c_email" placeholder="Email" style="background-color: #262626; color: #fff" required>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="c_password" placeholder="Password" style="background-color: #262626; color: #fff" required>
            </div>
            <div class="form-group">
                <input type="submit" class="form-control" name="login" class="login loginmodal-submit btn-temp" value="Login">
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="registeration" class="modal fade" role="dialog">
  <div class="modal-dialog" style="background-color: #262626;">

    <!-- Modal content-->
    <div class="modal-content" style="background-color: #262626;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title-color"><h1 class="text-center">Sign Up</h1></h4>
      </div>
      <div class="text-center">
        <form action="<?php echo base_url();?>customer_ui/register_customer" method="post" enctype="multipart/form-data">
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-4 form-group text-left">
                  <label><p style="color: white">First Name</p></label>
                  <input type="text" placeholder="Enter First Name Here.." class="form-control" name="c_firstname" required>
                </div>
                <div class="col-sm-4 form-group text-left">
                  <label><p style="color: white">Middle Name</p></label>
                  <input type="text" placeholder="Enter Middle Name Here.." class="form-control" name="c_middlename" required>
                </div>
                <div class="col-sm-4 form-group text-left</p>">
                  <label><p style="color: white">Last Name</p></label>
                  <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="c_lastname" required>
                </div>
              </div>          
              <div class="form-group text-left">
                <label><p style="color: white">Address</p></label>
                <textarea placeholder="Enter Address Here.." rows="3" class="form-control"  style="resize: none;" name="c_address" required></textarea>
              </div>  
              <div class="row">
                <div class="col-sm-3 form-group text-left">
                  <label><p style="color: white">City</p></label>
                  <input type="text" placeholder="Enter Your City Here.." class="form-control" name="c_city" required>
                </div>  
                <div class="col-sm-3 form-group text-left">
                  <label><p style="color: white">Region</p></label>
                  <input type="text" placeholder="Enter Your Region Here.." class="form-control" name="c_region" required>
                </div>  
                <div class="col-sm-3 form-group text-left">
                  <label><p style="color: white">Province</p></label>
                  <input type="text" placeholder="Enter Your Province Here.." class="form-control" name="c_province" required>
                </div>  
                <div class="col-sm-3 form-group text-left">
                  <label><p style="color: white">Zip</p></label>
                  <input type="text" placeholder="Enter Your Zip Code Here.." class="form-control" name="c_zip" required>
                </div>  
              </div>
              <div class="row">
                <div class="col-sm-4 form-group text-left">
                  <label><p style="color: white">Gender</p></label>
                  <select class="form-control" name="c_gender" required>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                  </select>
                </div>    
                <div class="col-sm-4 form-group text-left">
                  <label><p style="color: white">Birthdate</p></label>
                  <input type="date" placeholder="Date Of Birth" class="form-control" name="c_birthdate" required>
                </div>  
                <div class="col-sm-4 form-group text-left">
                  <label><p style="color: white">Photo</p></label>
                  <input type="file" class="form-control" name="c_picture">
                </div>
              </div>                
              <div class="row">
                <div class="col-sm-6 form-group text-left">
                  <label><p style="color: white">Contact Number</p></label>
                  <input type="text" placeholder="Enter Phone Number Here.." class="form-control" name="c_contact_no" required>
                </div>    
                <div class="col-sm-6 form-group text-left">
                  <label><p style="color: white">Email Address</p></label>
                  <input type="email" placeholder="Enter Email Address Here.." class="form-control" name="c_email" required>
                </div>  
              </div>  
              <div class="row">
                <div class="col-sm-6 form-group text-left">
                  <label><p style="color: white">Password</p></label>
                  <input type="password" placeholder="Enter Password Here.." class="form-control" required>
                </div>    
                <div class="col-sm-6 form-group text-left">
                  <label><p style="color: white">Confirm Password</p></label>
                  <input type="password" placeholder="Enter Password Here.." class="form-control" name="c_password" required>
                </div>  
              </div>
              <div class="form-group">
                <input type="submit" class="form-control" name="submit" class="login loginmodal-submit btn-temp" value="Submit">
              </div>
              <!-- <input type="submit" class="btn btn-lg btn-dark-purple" name="submit">        -->
            </div>
          </form> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>