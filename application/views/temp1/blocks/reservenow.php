<!-- Reserve Now Section -->
<section id="reserve"  style="background-color: #262626;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading title-color">Reserve Now</h2>
                <hr>
                <!-- <h3 class="section-subheading">Lorem ipsum dolor sit amet consectetur.</h3> -->
            </div>
        </div>
        <div class="row text-center">
            <form action="<?php echo base_url();?>add_controller/admin_add_reservation" method="post">
                <input type="hidden" name="tr_customer_id" value="<?php echo $c_id;?>">
                <input type="hidden" name="tr_customer_name" value="<?php echo $c_firstname;?> <?php echo $c_middlename;?> <?php echo $c_lastname;?>">
                <div class="row">
                    <div class="col-lg-12">
                    <div class="col-md-4">
                        <label for="bday" class="textdate"><i style="color: #fff">Date:</i></label>
                        <input type="date" class="form-control" name="checkindate" id="bday" required>
                    </div>
                    <div class="col-md-4">
                        <label for="time" class="texttime"><i style="color: #fff">Time:</i></label>
                        <input type="time" class="form-control" name="checkintime" id="time"  required>
                    </div> 
                    <div class="col-md-4">
                        <label for="submit" class="texttime"><i style="color: #fff">Action:</i></label>
                        <input type="submit" class="btn form-control" name="submit" value="RESERVE NOW">    
                    </div> 
                    </div>
                </div>
                <?php foreach ($get_services_info as $services_info) {?>

                    <?php if ($services_info->pc_title == 'Hair Cut') { ?> 
                        <div class="col-md-2">
                            <h4 class="service-heading">Hair Cut</h4>
                            <?php foreach ($get_hairstyle as $hairstyle_info) {?>
                            <div class="checkbox text-left">
                              <label><input type="radio" name="reserveval[]" value="<?php echo $hairstyle_info->s_id;?>"><i style="color: #fff"><?php echo $hairstyle_info->s_name;?> PHP<?php echo $hairstyle_info->s_price;?></i></label>
                            </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if ($services_info->pc_title == 'Color') { ?> 
                        <div class="col-md-2">
                            <h4 class="service-heading">Hair Color</h4>
                            <?php foreach ($get_color as $color_info) {?>
                            <div class="checkbox text-left">
                              <label><input type="radio" name="reserveval[]" value="<?php echo $color_info->hc_id;?>"><i style="color: #fff"><?php echo $color_info->hc_name;?></i></label>
                            </div>
                            <?php } ?>
                        </div>
                    <?php } ?>

                    <?php if ($services_info->pc_title == 'Form') { ?> 
                        <div class="col-md-2">
                            <h4 class="service-heading">Form</h4>
                            <?php foreach ($get_other_services as $form_info) {?>
                            <?php if($form_info->s_category == 'Form') { ?>
                            <div class="checkbox text-left">
                              <label><input type="checkbox" name="reserveval[]" value="<?php echo $form_info->s_id;?>"><i style="color: #fff"><?php echo $form_info->s_name;?> PHP<?php echo $form_info->s_price;?></i></label>
                            </div>
                            <?php } } ?>
                        </div>
                    <?php } ?>

                    <?php if ($services_info->pc_title == 'Hair Styling') { ?> 
                        <div class="col-md-2">
                            <h4 class="service-heading">Hair Styling</h4>
                            <?php foreach ($get_other_services as $hairstyling_info) {?>
                            <?php if($hairstyling_info->s_category == 'Hair Styling') { ?>
                            <div class="checkbox text-left">
                              <label><input type="checkbox" name="reserveval[]" value="<?php echo $hairstyling_info->s_id;?>"><i style="color: #fff"><?php echo $hairstyling_info->s_name;?> PHP<?php echo $hairstyling_info->s_price;?></i></label>
                            </div>
                            <?php } } ?>
                        </div>
                    <?php } ?>

                    <?php if ($services_info->pc_title == 'Make up') { ?> 
                        <div class="col-md-2">
                            <h4 class="service-heading">Make Up</h4>
                            <?php foreach ($get_other_services as $makeup_info) {?>
                            <?php if($makeup_info->s_category == 'Make up') { ?>
                            <div class="checkbox text-left">
                              <label><input type="checkbox" name="reserveval[]" value="<?php echo $makeup_info->s_id;?>"><i style="color: #fff"><?php echo $makeup_info->s_name;?> PHP<?php echo $makeup_info->s_price;?></i></label>
                            </div>
                            <?php } } ?>
                        </div>
                    <?php } ?>

                    <?php if ($services_info->pc_title == 'Massage and Spa') { ?> 
                        <div class="col-md-2">
                            <h4 class="service-heading">Massage and Spa</h4>
                            <?php foreach ($get_other_services as $Massageandspa_info) {?>
                            <?php if($Massageandspa_info->s_category == 'Massage and Spa') { ?>
                            <div class="checkbox text-left">
                              <label><input type="checkbox" name="reserveval[]" value="<?php echo $Massageandspa_info->s_id;?>"><i style="color: #fff"><?php echo $Massageandspa_info->s_name;?> PHP<?php echo $Massageandspa_info->s_price;?></i></label>
                            </div>
                            <?php } } ?>
                        </div>
                    <?php } ?>

                    <?php if ($services_info->pc_title == 'Nails') { ?> 
                        <div class="col-md-2">
                            <h4 class="service-heading">Nails</h4>
                            <?php foreach ($get_other_services as $Nails_info) {?>
                            <?php if($Nails_info->s_category == 'Nails') { ?>
                            <div class="checkbox text-left">
                              <label><input type="checkbox" name="reserveval[]" value="<?php echo $Nails_info->s_id;?>"><i style="color: #fff"><?php echo $Nails_info->s_name;?> PHP<?php echo $Nails_info->s_price;?></i></label>
                            </div>
                            <?php } } ?>
                        </div>
                    <?php } ?>

                    <?php if ($services_info->pc_title == 'Treatment') { ?> 
                        <div class="col-md-2">
                            <h4 class="service-heading">Treatment</h4>
                            <?php foreach ($get_other_services as $Treatment_info) {?>
                            <?php if($Treatment_info->s_category == 'Treatment') { ?>
                            <div class="checkbox text-left">
                              <label><input type="checkbox" name="reserveval[]" value="<?php echo $Treatment_info->s_id;?>"><i style="color: #fff"><?php echo $Treatment_info->s_name;?> PHP<?php echo $Treatment_info->s_price;?></i></label>
                            </div>
                            <?php } } ?>
                        </div>
                    <?php } ?>

                <?php } ?>
            </form>

            
      
    </div>
</section>