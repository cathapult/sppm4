<!-- Services Section -->
<section id="services">
    <div class="container">
        <div class="row">
        	<div class="col-lg-12 text-center">
                <h2 class="section-heading title-color">Services</h2>
                
                <!-- <h3 class="section-subheading">Lorem ipsum dolor sit amet consectetur.</h3> -->
            </div>
            <div class="row text-center services-menu">
            	<div class="col-lg-12">
            	<hr>
                	<ul>
                      <?php foreach ($get_services_title as $title) {?>
                      <li><a href="javascript:showonlyone('<?php echo $title->pc_id;?>C');" id="s-<?php echo $title->pc_id;?>" class="btn btn-temp"><?php echo $title->pc_title;?></a></li>
                      <?php }?>
					</ul>
				<hr>
				</div>
            </div>
            <?php foreach ($get_services_info as $info) {?>
            <div class="services-content" id="<?php echo $info->pc_id;?>C" style="display: none;">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading title-color"><?php echo $info->pc_title;?></h2>
                    <h3 class="section-subheading"><?php echo $info->pc_description;?></h3>

                    <?php if ($info->pc_title == 'Hair Cut') { ?>
                    <?php foreach ($get_hairstyle as $hairstyle) {?>
                    <div class="col-md-4 col-sm-6 hairstyles">
                        <img class="img-circle" src="<?php echo base_url();?>media/service/<?php echo $hairstyle->s_picture;?>" width="236" height="291">
                        <p class="hairstyle-title"><?php echo $hairstyle->s_name;?> - PHP <?php echo $hairstyle->s_price;?></p>
                    </div>
                    <?php } }?>

                    <?php if ($info->pc_title == 'Color') { ?>
                    <?php foreach ($get_color as $colors) {?>
                    <div class="col-md-4 col-sm-6 hairstyles">
                        <img class="img-circle" src="<?php echo base_url();?>media/haircolor/<?php echo $colors->hc_photo;?>">
                        <p class="hairstyle-title"><?php echo $colors->hc_name;?></p>
                    </div>
                    <?php } }?>

                    <?php if ($info->pc_title == 'Form') { ?>
                    <?php foreach ($get_other_services as $form) {?>
                    <div class="col-md-4 col-sm-6 hairstyles">
                        <?php if($form->s_category == 'Form') { ?>
                        <img class="img-circle" src="<?php echo base_url();?>media/service/<?php echo $form->s_picture;?>" width="236" height="291">
                        <p class="hairstyle-title"><?php echo $form->s_name;?></p>
                        <?php } ?>
                    </div>
                    <?php } }?>

                    <?php if ($info->pc_title == 'Hair Styling') { ?>
                    <?php foreach ($get_other_services as $hairstyling) {?>
                    <div class="col-md-4 col-sm-6 hairstyles">
                        <?php if($hairstyling->s_category == 'Hair Styling') { ?>
                        <img class="img-circle" src="<?php echo base_url();?>media/service/<?php echo $hairstyling->s_picture;?>"  width="236" height="291">
                        <p class="hairstyle-title"><?php echo $hairstyling->s_name;?></p>
                        <?php } ?>
                    </div>
                    <?php } }?>

                    <?php if ($info->pc_title == 'Make up') { ?>
                    <?php foreach ($get_other_services as $makeup) {?>
                    <div class="col-md-4 col-sm-6 hairstyles">
                        <?php if($makeup->s_category == 'Make up') { ?>
                        <img class="img-circle" src="<?php echo base_url();?>media/service/<?php echo $makeup->s_picture;?>" width="236" height="291">
                        <p class="hairstyle-title"><?php echo $makeup->s_name;?></p>
                        <?php } ?>
                    </div>
                    <?php } }?>

                    <?php if ($info->pc_title == 'Massage and Spa') { ?>
                    <?php foreach ($get_other_services as $Massageandspa) {?>
                    <div class="col-md-4 col-sm-6 hairstyles">
                        <?php if($Massageandspa->s_category == 'Massage and Spa') { ?>
                        <img class="img-circle" src="<?php echo base_url();?>media/service/<?php echo $Massageandspa->s_picture;?>" width="236" height="291">
                        <p class="hairstyle-title"><?php echo $Massageandspa->s_name;?></p>
                        <?php } ?>
                    </div>
                    <?php } }?>

                    <?php if ($info->pc_title == 'Nails') { ?>
                    <?php foreach ($get_other_services as $Nails) {?>
                    <div class="col-md-4 col-sm-6 hairstyles">
                        <?php if($Nails->s_category == 'Nails') { ?>
                        <img class="img-circle" src="<?php echo base_url();?>media/service/<?php echo $Nails->s_picture;?>" width="236" height="291">
                        <p class="hairstyle-title"><?php echo $Nails->s_name;?></p>
                        <?php } ?>
                    </div>
                    <?php } }?>

                    <?php if ($info->pc_title == 'Treatment') { ?>
                    <?php foreach ($get_other_services as $Treatment) {?>
                    <div class="col-md-4 col-sm-6 hairstyles">
                        <?php if($Treatment->s_category == 'Treatment') { ?>
                        <img class="img-circle" src="<?php echo base_url();?>media/service/<?php echo $Treatment->s_picture;?>" width="236" height="291">
                        <p class="hairstyle-title"><?php echo $Treatment->s_name;?></p>
                        <?php } ?>
                    </div>
                    <?php } }?>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</section>