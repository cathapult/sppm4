<!DOCTYPE html>

<html>
<?php
if (isset($this->session->userdata['logged_in'])) {
    $c_id = ($this->session->userdata['logged_in']['c_id']);
    $c_firstname = ($this->session->userdata['logged_in']['c_firstname']);
    $c_middlename = ($this->session->userdata['logged_in']['c_middlename']);
    $c_lastname = ($this->session->userdata['logged_in']['c_lastname']);
    $c_picture = ($this->session->userdata['logged_in']['c_picture']);
}
else {
    header("location: ");
}
?>

<head>
	<title>Welcome to <?php echo $get_page_contents_storename[0]->pc_description;?>!</title>
    <?php include('design/css_ui.php') ;?>

	<style type="text/css">
		header {
  			background: url('<?php echo base_url();?>media/pagecontents/<?php echo $get_page_contents_header_bg[0]->pc_img;?>') no-repeat center center fixed;
  		}
	</style>
	

<style> 
#panel, #flip {
    padding: 5px;
    text-align: center;
    background-color: #e5eecc;
    border: solid 1px #c3c3c3;
}

#haircut {
    padding: 50px;
    display: none;
}
#color {
    padding: 50px;
    display: none;
}
</style>
</head>
<body>
<?php include('blocks/navigation.php') ;?>
<!-- <?php 
foreach ($get_services_name as $row)
{
        echo $row['s_name'];

}?> -->
<!-- Header -->
<header>
    <div class="container">
        <div class="intro-text">
            <!-- <div class="intro-lead-in">Welcome To Generic Parlor!</div> -->
            <div class="intro-heading"><?php echo $get_page_contents_storename[0]->pc_description;?></div>
            <a href="#aboutus">
            <span class="fa-stack fa-4x">
                <i class="fa fa-circle-thin fa-stack-2x aboutus-logo"></i>
                <i class="fa fa-angle-double-down fa-stack-1x fa-inverse"></i>
            </span>
            </a>
            <!-- <a href="#aboutus" class="page-scroll btn btn-temp btn-lg">Explore</a> -->
        </div>
    </div>
</header>
<?php if (isset($message)) { ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="fa fa-info-circle"></i>  <strong><?php echo $message;?></strong> 
                </div>
            </div>
        </div>
        <!-- /.row -->
        <?php } ?>
<?php include('blocks/aboutus.php') ;?>
<?php include('blocks/services.php') ;?>
<?php 
    if(isset($this->session->userdata['logged_in'])) { 
        include('blocks/reservenow.php'); 
    }
    else {
        include('blocks/gallery.php');
    }
?>
<?php include('blocks/contactus.php') ;?>
<?php include('blocks/footer.php') ;?>

</body>
<!-- jQuery -->
<?php include('design/js_ui.php') ;?>
</html>