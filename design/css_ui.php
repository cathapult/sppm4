<?php date_default_timezone_set('Asia/Kuala_Lumpur');?>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>design/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<?php echo base_url();?>design/css/font-awesome.css" rel="stylesheet" type="text/css">

<!-- Custom CSS -->
<link href="<?php echo base_url();?>design/css/ui/css.css" rel="stylesheet">