<!-- jQuery -->
<script src="<?php echo base_url();?>design/js/jquery-3.1.1.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>design/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>design/datatables/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url();?>design/js/canvasjs.min.js"></script>
<!-- Custom JS -->
<script>
	// Menu Toggle Script
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    // Alert Division Hide
    $(document).ready( function() {
        $('#alert-row').delay(3000).fadeOut();
    });
    $(document).ready(function(){
    $('#inventoryTable').DataTable();
	});
$(document).ready(function(){
    $('#employeeTable').DataTable();
	});
	$(document).ready(function(){
    $('#quantityTable').DataTable();
	});
	$(document).ready(function(){
    $('#serviceTable').DataTable();
	});
	$(document).ready(function(){
    $('#hairColor').DataTable();
	});
	$(document).ready(function(){
    $('#promosTable').DataTable();
	});
	$(document).ready(function(){
    $('#customerTable').DataTable();
	});
    $(document).ready(function(){
    $('#jobposTable').DataTable();
    });
	
</script>