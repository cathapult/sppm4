-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2017 at 01:00 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sp_salon_mgt`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `br_id` varchar(50) NOT NULL,
  `br_address` varchar(500) NOT NULL,
  `br_city` varchar(50) NOT NULL,
  `br_zip` varchar(10) NOT NULL,
  `br_province` varchar(100) NOT NULL,
  `br_region` varchar(100) NOT NULL,
  `br_inputdate` varchar(20) NOT NULL,
  `br_inputtime` varchar(20) NOT NULL,
  `br_inputby` varchar(20) NOT NULL,
  `br_updatedate` varchar(20) NOT NULL,
  `br_updatetime` varchar(20) NOT NULL,
  `br_updateby` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`br_id`, `br_address`, `br_city`, `br_zip`, `br_province`, `br_region`, `br_inputdate`, `br_inputtime`, `br_inputby`, `br_updatedate`, `br_updatetime`, `br_updateby`) VALUES
('BRA20160102230432', '193 Maximina drive Balong Bato', 'Quezon City', '1100', 'Metro Manila', 'NCR', '2016-01-02', '23:04:32', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `c_id` varchar(500) NOT NULL,
  `c_firstname` varchar(100) NOT NULL,
  `c_middlename` varchar(50) NOT NULL,
  `c_lastname` varchar(50) NOT NULL,
  `c_address` varchar(500) NOT NULL,
  `c_city` varchar(100) NOT NULL,
  `c_region` varchar(50) NOT NULL,
  `c_province` varchar(50) NOT NULL,
  `c_zip` varchar(50) NOT NULL,
  `c_gender` varchar(7) NOT NULL,
  `c_email` varchar(250) NOT NULL,
  `c_contact_no` varchar(12) NOT NULL,
  `c_password` varchar(50) NOT NULL,
  `c_birthdate` varchar(20) NOT NULL,
  `c_picture` varchar(500) NOT NULL,
  `c_status` varchar(20) NOT NULL,
  `c_registry_date` varchar(20) NOT NULL,
  `c_registry_time` varchar(20) NOT NULL,
  `c_update_date` varchar(20) NOT NULL,
  `c_update_time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`c_id`, `c_firstname`, `c_middlename`, `c_lastname`, `c_address`, `c_city`, `c_region`, `c_province`, `c_zip`, `c_gender`, `c_email`, `c_contact_no`, `c_password`, `c_birthdate`, `c_picture`, `c_status`, `c_registry_date`, `c_registry_time`, `c_update_date`, `c_update_time`) VALUES
('CUST170102102956', 'Zenric', 'Rey', 'Padua', '1203 Perez St. Apartment F. Paco ', 'Manila', 'NCR', 'Metro Manila', '911', 'Male', 'zenricpadua@gmail.com', '09169334629', '12345', '1996-10-09', 'CUST170102102956.jpg', 'Active', '2017-01-02', '10:29:56', '2017-02-01', '02:06:29'),
('CUST170102103056', 'Joanne', 'Bayson', 'Bauto', 'Greenhills Subdv.', 'San Juan', 'NCR', 'Metro Manila', '1425', 'Female', 'jam.bayson@gmail.com', '09056683402', '12345', '1997-01-22', 'CUST170102103056.JPG', 'Active', '2017-01-02', '10:30:56', '2017-02-01', '02:08:57'),
('CUST170206060059', 'Rogiemarc', 'Bilen', 'Patino', 'Ayala Blvrd., Ermita', 'Manila', 'NCR', 'Region', '1456', 'Male', 'rogiepatino@gmail.com', '09968971256', '12345', '1996-06-28', 'CUST170206060059.jpg', 'Disabled', '2017-02-06', '06:00:59', '', ''),
('CUST20161229050020', 'lpuddo', 'Hakata', 'Ramen', 'SM Megamall Mega Fashion Hall Buildiung D, EDSA Cor Julia Vargas Avenue, Ortigas Center', 'Mandaluyong', 'NCR', 'Metro Manila', '1550', 'Male', 'ippudo@gmail.com', '024701837', '(02) 470 1837', '1985-10-16', 'CUST20161229050020.JPG', 'Active', '2017-01-01', '11:05:32', '2017-02-01', '02:09:39'),
('CUST20161229054630', 'Miguel Eduardo', 'Quijano', 'Quintos', '2929 Rizal Ave. Ext. Sta. Cruz', 'Manila', 'NCR', 'Metro Manila', '1425', 'Male', 'eduardo.miguel@yahoo.com', '09465689123', '12345', '1996-05-10', 'CUST20161229054630.JPG', 'Active', '2016-12-29', '05:46:30', '2017-02-01', '02:11:11'),
('sample1', 'Catherine Mae', 'Hipolito', 'Liwag', 'MEM3', 'Mandaluyong', 'NCR', 'Metro Manila', '1471', 'Female', 'kat@gmail.com', '12345678901', '12345', '1996-11-26', 'sample1.JPG', 'Disabled', '21061112', '20161112', '2017-02-01', '02:13:39');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `e_id` varchar(500) NOT NULL,
  `e_firstname` varchar(100) NOT NULL,
  `e_middlename` varchar(50) NOT NULL,
  `e_lastname` varchar(50) NOT NULL,
  `e_address` varchar(500) NOT NULL,
  `e_city` varchar(100) NOT NULL,
  `e_region` varchar(50) NOT NULL,
  `e_province` varchar(50) NOT NULL,
  `e_zip` varchar(50) NOT NULL,
  `e_gender` varchar(7) NOT NULL,
  `e_email` varchar(250) NOT NULL,
  `e_contact_no` varchar(12) NOT NULL,
  `e_picture` varchar(50) NOT NULL,
  `e_password` varchar(50) NOT NULL,
  `e_birthdate` varchar(20) NOT NULL,
  `e_position` varchar(50) NOT NULL,
  `e_registry_date` varchar(20) NOT NULL,
  `e_registry_time` varchar(20) NOT NULL,
  `e_update_date` varchar(20) NOT NULL,
  `e_update_time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`e_id`, `e_firstname`, `e_middlename`, `e_lastname`, `e_address`, `e_city`, `e_region`, `e_province`, `e_zip`, `e_gender`, `e_email`, `e_contact_no`, `e_picture`, `e_password`, `e_birthdate`, `e_position`, `e_registry_date`, `e_registry_time`, `e_update_date`, `e_update_time`) VALUES
('EMP170102092134', 'Emilio Aguinalds', 'Garapon', 'Jacinto', '1 Tondo St. Brgy. 317', 'Malabon', 'NCR', 'Metro Manila', '1524', 'Male', 'emiliojacinto82@yahoo.com', '09465689122', 'EMP170102092134.jpg', '12345', '1982-04-05', 'OWNER', '2017-01-02', '09:45:06', '2017-02-06', '06:14:24'),
('EMP170102094859', 'Ububwe', 'Onantenye ', 'Osas', '34 Aguilar St. Brgy Nagtahan', 'Manila', 'NCR', 'Metro Manila', '2456', 'Male', 'ububweosas89@yahoo.com', '09234542134', 'EMP170102094859.jpg', '12345', '1989-04-26', 'Junior Hairstylist', '2017-01-02', '09:48:59', '2017-02-01', '01:55:40'),
('EMP170131073740', 'Leng', 'Jollibee', 'Mcdo', '1 San Juan St. ', 'Malabon', 'NCR', 'Metro Manila', '1471', 'Female', 'leng@gmail.com', '09991234564', 'EMP170131073740.jpg', '12345', '1989-12-31', 'Manicurist', '2017-01-31', '07:37:40', '2017-02-01', '01:55:53'),
('EMP170131073925', 'Ellen', 'Rene', 'Dupree', '32 Alvarez St.', 'Manila', 'NCR', 'Metro Manila', '1712', 'Female', 'ellen@email.com', '09871248765', 'EMP170131073925.jpg', '12345', '1987-05-05', 'Manicurist', '2017-01-31', '07:39:25', '2017-02-01', '01:56:08'),
('EMP170131074209', 'Maria', 'Eleonor', 'Mata', '12 Mactan St. Brgy 123', 'Manila', 'NCR', 'Metro Manila', '1321', 'Female', 'maria@email.com', '09671234589', 'EMP170131074209.jpg', '12345', '1990-03-12', 'Manicurist', '2017-01-31', '07:42:09', '2017-02-01', '01:56:23'),
('EMP170131074419', 'Glen', 'Ton', 'Do', '12 Malabon St. Brgy. Paco', 'Manila', 'NCR', 'Metro Manila', '1456', 'Female', 'glen@email.com', '09763421234', 'EMP170131074419.jpg', '12345', '1987-02-12', 'Therapist ', '2017-01-31', '07:44:19', '2017-02-01', '02:30:12'),
('EMP170131074615', 'Sam', 'France', 'America', '145 Tondo St. Brgy. Balot', 'Manila', 'NCR', 'Metro Manila', '1423', 'Female', 'sam@email.com', '09876543145', 'EMP170131074615.jpg', '12345', '1989-05-31', 'Therapist ', '2017-01-31', '07:46:15', '2017-02-01', '02:29:57'),
('EMP170131074835', 'Don', 'Emilio', 'Cruz', '1456 Sta.Cruz St. Brgy. San Roque', 'Navotas', 'NCR', 'Metro Manila', '1432', 'Male', 'don@email.com', '09876543145', 'EMP170131074835.jpg', '12345', '1987-02-27', 'Helper', '2017-01-31', '07:48:35', '2017-02-01', '01:57:01'),
('EMP201612014532', 'Lex', 'Superman', 'Luthor', '21 Craig St. Blumentrit', 'Vigan', 'Region I', 'Ilocos Norte', '1231', 'Male', 'luthorlex90@gmail.com', '09954817963', 'EMP201612014532.jpg', '12345', '1990-10-06', 'Salon Manager', '2016-12-01', '17:53:32', '2017-02-01', '01:57:16'),
('EMP201612293456', 'Melchor', 'Gaspar', 'Baltazar', '43 Basilio St. Brgy. Mayon', 'Manila', 'NCR', 'Bicol', '1491', 'Male', 'melgaspar@gmail.com', '09257890123', 'EMP201612293456.JPG', '12345', '1985-01-12', 'Senior Hairstylist', '2016-12-29', '05:50:36', '2017-02-01', '01:58:53');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE IF NOT EXISTS `feedbacks` (
  `fb_id` varchar(50) NOT NULL,
  `fb_name` varchar(50) NOT NULL,
  `fb_email` varchar(50) NOT NULL,
  `fb_contactno` varchar(15) NOT NULL,
  `fb_message` varchar(500) NOT NULL,
  `fb_status` varchar(20) NOT NULL,
  `fb_inputdate` varchar(20) NOT NULL,
  `fb_inputtime` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`fb_id`, `fb_name`, `fb_email`, `fb_contactno`, `fb_message`, `fb_status`, `fb_inputdate`, `fb_inputtime`) VALUES
('FB2017012809321345', 'Zenric Padua', 'zenricpadua@gmail.com', '09169334629', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id aliquet erat, at faucibus lorem. Phasellus congue finibus tincidunt. Maecenas non bibendum est, quis pellentesque mi. Donec dignissim elit augue, eu iaculis sapien interdum in. Vestibulum tellus lorem, elementum vitae tincidunt id, viverra eu dolor. Aenean odio quam, blandit at luctus sed, pretium quis nulla.', 'Not Read', '2017-01-28', '09:32:1345'),
('FB2017012809354532', 'Joanne Bauto', 'jam.bayson@gmail.com', '09056683402', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id aliquet erat, at faucibus lorem. Phasellus congue finibus tincidunt. Maecenas non bibendum est, quis pellentesque mi. Donec dignissim elit augue, eu iaculis sapien interdum in. Vestibulum tellus lorem, elementum vitae tincidunt id, viverra eu dolor. Aenean odio quam, blandit at luctus sed, pretium quis nulla.', 'Read', '2017-01-28', '09:35:4532');

-- --------------------------------------------------------

--
-- Table structure for table `hair_colors`
--

CREATE TABLE IF NOT EXISTS `hair_colors` (
  `hc_id` varchar(100) NOT NULL,
  `hc_name` varchar(500) NOT NULL,
  `hc_photo` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hair_colors`
--

INSERT INTO `hair_colors` (`hc_id`, `hc_name`, `hc_photo`) VALUES
('04', 'Ultra Light Natural Blonde', '04.png'),
('05', 'Ultra Light Ash Blonde', '05.png'),
('10', 'Black', '10.png'),
('11', 'Soft Black', '11.png'),
('12', 'Natural Blue Black', '12.png'),
('20', 'Brown Black', '20.png'),
('27', 'Deep Rich Brown', '27.png'),
('30', 'Dark Brown', '30.png'),
('31', 'Dark Auburn', '31.png'),
('32', 'Dark Mahogany Brown', '32.png'),
('33', 'Dark Soft Brown', '33.png'),
('34', 'Deep Burgundy', '34.png'),
('35', 'Vibrant Red', '35.png'),
('37', 'Dark Golden Brown', '37.png'),
('40', 'Medium Ash Brown', '40.png'),
('41', 'Medium Brown', '41.png'),
('42', 'Medium Auburn', '42.png'),
('43', 'Medium Golden Brown', '43.png'),
('44', 'Medium Reddish Brown', '44.png'),
('45', 'Bright Auburn', '45.png'),
('46', 'Medium Golden Chestnut Brown', '46.png'),
('47', 'Medium Rich Brown', '47.png'),
('48', 'Burgundy', '48.png'),
('49', 'Auburn Brown', '49.png'),
('50', 'Light Ash Brown', '50.png'),
('51', 'Light Brown', '51.png'),
('53', 'Light Auburn', '53.png'),
('54', 'Light Golden Brown', '54.png'),
('55', 'Light Reddish Brown', '55.png'),
('57', 'Lightest Golden Brown', '57.png'),
('60', 'Dark Ash Blonde', '60.png'),
('61', 'Dark Blonde', '61.png'),
('70', 'Medium Ash Blonde', '70.png'),
('71', 'Golden Blonde', '71.png'),
('73', 'Champagne Blonde', '73.png'),
('74', 'Medium Blonde', '74.png'),
('75', 'Warm Golden Blonde', '75.png'),
('80', 'Light Ash Blonde', '80.png'),
('81', 'Light Blonde', '81.png'),
('95', 'Light Sun Blonde', '95.png');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE IF NOT EXISTS `inventory` (
  `i_serial_no` varchar(500) NOT NULL,
  `i_product_name` varchar(500) NOT NULL,
  `i_brand` varchar(200) NOT NULL,
  `i_price` varchar(20) NOT NULL,
  `i_category` varchar(50) NOT NULL,
  `i_date_used` varchar(20) NOT NULL,
  `i_status` varchar(50) NOT NULL,
  `i_receipt_no` varchar(500) NOT NULL,
  `i_bought_date` varchar(20) NOT NULL,
  `i_bought_time` varchar(20) NOT NULL,
  `i_company_name` varchar(250) NOT NULL,
  `i_address` varchar(500) NOT NULL,
  `i_city` varchar(150) NOT NULL,
  `i_contact_no` varchar(12) NOT NULL,
  `i_firstname` varchar(100) NOT NULL,
  `i_lastname` varchar(100) NOT NULL,
  `i_input_date` varchar(20) NOT NULL,
  `i_input_time` varchar(20) NOT NULL,
  `i_update_date` varchar(20) NOT NULL,
  `i_update_time` varchar(20) NOT NULL,
  `i_img` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`i_serial_no`, `i_product_name`, `i_brand`, `i_price`, `i_category`, `i_date_used`, `i_status`, `i_receipt_no`, `i_bought_date`, `i_bought_time`, `i_company_name`, `i_address`, `i_city`, `i_contact_no`, `i_firstname`, `i_lastname`, `i_input_date`, `i_input_time`, `i_update_date`, `i_update_time`, `i_img`) VALUES
('6914161222017', 'Ionic Keratin Treatment', 'Shine Moist', '120', 'Keratin Treatment', '', 'Not Used', '0123456', '2016-12-20', '15:11', 'Watson''s Generics SM San Lazaro', 'Address of SM San Lazaro', 'Manila', '2314512', 'Gordon', 'Ramsey', '2017-01-16', '04:21:30', '2017-02-01', '01:59:21', '6914161222017.JPG'),
('6914161222018', 'Ionic Keratin Treatment', 'Shine Moist', '120', 'Keratin Treatment', '', 'Not Used', '0123456', '2016-12-20', '15:11', 'Watson''s Generics SM San Lazaro', 'Address of SM San Lazaro', 'Manila', '2314512', 'Gordon', 'Ramsey', '2017-01-16', '04:32:07', '2017-02-01', '01:59:30', '6914161222018.JPG'),
('6914161222019', 'Ionic Keratin Treatment', 'Shine Moist', '120', 'Keratin Treatment', '', 'Not Used', '0123456', '2016-12-20', '15:11', 'Watson''s Generics SM San Lazaro', 'Address of SM San Lazaro', 'Manila', '2314512', 'Gordon', 'Ramsey', '2017-01-16', '04:37:37', '2017-02-01', '02:00:10', '6914161222019.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `job_positions`
--

CREATE TABLE IF NOT EXISTS `job_positions` (
  `jp_id` varchar(100) NOT NULL,
  `jp_name` varchar(500) NOT NULL,
  `jp_type` varchar(100) NOT NULL,
  `jp_salary` varchar(20) NOT NULL,
  `jp_inputdate` varchar(20) NOT NULL,
  `jp_inputtime` varchar(20) NOT NULL,
  `jp_inputby` varchar(20) NOT NULL,
  `jp_updatedate` varchar(20) NOT NULL,
  `jp_updatetime` varchar(20) NOT NULL,
  `jp_updateby` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_positions`
--

INSERT INTO `job_positions` (`jp_id`, `jp_name`, `jp_type`, `jp_salary`, `jp_inputdate`, `jp_inputtime`, `jp_inputby`, `jp_updatedate`, `jp_updatetime`, `jp_updateby`) VALUES
('JPOS162912155236', 'OWNER', 'Managerial', 'NONE', '2017-01-31', '07:56:43', '', '', '', ''),
('JPOS170102095106', 'Salon Manager', 'Managerial', '8000', '2017-01-31', '07:57:41', '', '', '', ''),
('JPOS170116044358', 'Senior Hairstylist', 'Man Power', '5000', '2017-01-31', '07:57:47', '', '', '', ''),
('JPOS170116044420', 'Receptionist', 'Managerial', '1200', '2017-01-31', '07:58:03', '', '', '', ''),
('JPOS170116044448', 'Junior Hairstylist', 'Man Power', '3500', '2017-01-31', '07:58:09', '', '', '', ''),
('JPOS170131073307', 'Therapist ', 'Man Power', '1200', '2017-01-31', '07:58:17', '', '', '', ''),
('JPOS170131073418', 'Helper', 'Managerial', '1000', '2017-01-31', '07:58:31', '', '', '', ''),
('JPOS170131073443', 'Manicurist', 'Man Power', '1200', '2017-01-31', '07:58:36', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `overall_inventory`
--

CREATE TABLE IF NOT EXISTS `overall_inventory` (
  `oi_id` varchar(40) NOT NULL,
  `oi_name` varchar(100) NOT NULL,
  `oi_notused_qty` varchar(20) NOT NULL,
  `oi_used_qty` varchar(20) NOT NULL,
  `oi_disposed_qty` varchar(20) NOT NULL,
  `oi_updatedate` varchar(20) NOT NULL,
  `oi_updatetime` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `overall_inventory`
--

INSERT INTO `overall_inventory` (`oi_id`, `oi_name`, `oi_notused_qty`, `oi_used_qty`, `oi_disposed_qty`, `oi_updatedate`, `oi_updatetime`) VALUES
('OI20170207032800', 'Ionic Keratin Treatment', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `page_contents`
--

CREATE TABLE IF NOT EXISTS `page_contents` (
  `pc_id` varchar(50) NOT NULL,
  `pc_title` varchar(100) NOT NULL,
  `pc_description` varchar(1000) NOT NULL,
  `pc_img` varchar(50) NOT NULL,
  `pc_division` varchar(50) NOT NULL,
  `pc_state` varchar(50) NOT NULL,
  `pc_updatedate` varchar(20) NOT NULL,
  `pc_updatetime` varchar(20) NOT NULL,
  `pc_updateby` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_contents`
--

INSERT INTO `page_contents` (`pc_id`, `pc_title`, `pc_description`, `pc_img`, `pc_division`, `pc_state`, `pc_updatedate`, `pc_updatetime`, `pc_updateby`) VALUES
('PCA01', 'Company Profile', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id aliquet erat, at faucibus lorem. Phasellus congue finibus tincidunt. Maecenas non bibendum est, quis pellentesque mi. Donec dignissim elit augue, eu iaculis sapien interdum in. Vestibulum tellus lorem, elementum vitae tincidunt id, viverra eu dolor. Aenean odio quam, blandit at luctus sed, pretium quis nulla.', '', 'ABOUT US', 'SHOW', '2017-02-01', '03:30:03', ''),
('PCA02', 'Mission', 'Our mission is to consistently bring you the freshest styles and\r\nideas, by keeping up with the current trends and constantly educating\r\nourselves about the best practices. To ensure that our clients are\r\nalways valued individuals who are respected and treated in a\r\nconsistent and professional manner at all times.', '', 'ABOUT US', 'SHOW', '2017-01-30', '04:43:47', ''),
('PCA03', 'Vision', 'To relentlessly pursue growth, by continually improving performance,\r\nproducts, and business efficiency in every area.', '', 'ABOUT US', 'SHOW', '2017-01-30', '04:43:56', ''),
('PCC01', 'Logo', 'Logo', 'PCC01.png', 'Logo', 'SHOW', '2017-02-02', '03:18:47', ''),
('PCC02', 'Parlor name', 'Salon Management', '', 'ALL', 'SHOW', '2017-02-02', '02:53:52', ''),
('PCC03', 'Header Background', 'Select an image for header background', 'PCC03.jpg', 'ALL', 'SHOW', '2017-02-02', '06:23:29', ''),
('PCG01', 'Gallery 1', '', 'PCG01.jpg', 'GALLERY', 'SHOW', '2017-02-02', '06:13:23', ''),
('PCG02', 'Gallery 2', '', 'PCG02.jpg', 'GALLERY', 'SHOW', '2017-02-02', '06:26:41', ''),
('PCG03', 'Gallery 3', '', 'PCG03.jpg', 'GALLERY', 'SHOW', '2017-02-02', '06:13:37', ''),
('PCG04', 'Gallery 4', '', 'PCG04.jpg', 'GALLERY', 'SHOW', '2017-02-02', '06:15:02', ''),
('PCG05', 'Gallery 5', '', 'PCG05.jpg', 'GALLERY', 'SHOW', '2017-02-02', '06:15:09', ''),
('PCG06', 'Gallery 6', '', 'PCG06.jpg', 'GALLERY', 'SHOW', '2017-02-02', '06:15:15', ''),
('PCS01', 'Hair Cut', 'Innovative therapies, hair design and styling customized to your look, mood and lifestyle for healthy, beautiful hair you love. All hair services include a personal consultation, shampoo, blow dry, except conditioning treatment and special occassion styling.', '', 'SERVICES', 'SHOW', '2017-02-01', '03:32:10', ''),
('PCS02', 'Color', 'Whether you''re looking for a completely new look or just want to breathe fresh life into your current color, we''ve rounded up some of the best hair colors and ideas of the year.', '', 'SERVICES', 'SHOW', '', '', ''),
('PCS03', 'Treatment', 'Treat abused, dull or out-of-condition scalps and tresses to nutrient-rich herbal blends to bring the top of your head back to its natural best. Give your hair what it really needs: a nutritious intervention.', '', 'SERVICES', 'SHOW', '', '', ''),
('PCS04', 'Form', 'We are giving you out the best hair form trends in the nation. Curly or straight, we''ll make sure that it will suit you taste on hair forms.', '', 'SERVICES', 'SHOW', '', '', ''),
('PCS05', 'Hair Styling', 'We are giving you out the best styles in the nation. We''ll make sure that it will suit you taste on hair forms.', '', 'SERVICES', 'SHOW', '', '', ''),
('PCS06', 'Nails', 'Nail art is a creative way to paint, decorate, enhance, and embellish the nails. It is a type of artwork that can be done on fingernails and toenails, usually after manicures or pedicures.', '', 'SERVICES', 'UNSHOW', '', '', ''),
('PCS07', 'Massage and Spa', 'Massage therapy is manual manipulation of soft body tissues (muscle, connective tissue, tendons and ligaments) to enhance a person''s health and well-being. There are dozens of types of massage therapy methods (also called modalities).', '', 'SERVICES', 'UNSHOW', '2017-01-31', '03:11:54', ''),
('PCS08', 'Make up', 'Cosmetics, also known as make-up, are substances or products used to enhance the appearance or fragrance of the body.', '', 'SERVICES', 'SHOW', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `promos`
--

CREATE TABLE IF NOT EXISTS `promos` (
  `pr_id` varchar(100) NOT NULL,
  `pr_name` varchar(300) NOT NULL,
  `pr_description` varchar(500) NOT NULL,
  `pr_price` varchar(10) NOT NULL,
  `pr_status` varchar(50) NOT NULL,
  `pr_img` varchar(100) NOT NULL,
  `pr_inputdate` varchar(20) NOT NULL,
  `pr_inputtime` varchar(20) NOT NULL,
  `pr_inputby` varchar(50) NOT NULL,
  `pr_updatedate` varchar(20) NOT NULL,
  `pr_updatetime` varchar(20) NOT NULL,
  `pr_updateby` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promos`
--

INSERT INTO `promos` (`pr_id`, `pr_name`, `pr_description`, `pr_price`, `pr_status`, `pr_img`, `pr_inputdate`, `pr_inputtime`, `pr_inputby`, `pr_updatedate`, `pr_updatetime`, `pr_updateby`) VALUES
('PRO170102085215', '50%OFF All Haircuts', 'Platinum-blonde Michelle Williams might be our favorite version of Michelle Williams, like ever; here her signature crop is flat-ironed like a damn satin sheet and tucked behind both ears. Sweet and chic.', '100', 'Available', 'PRO170102085215.jpg', '2017-01-02', '08:52:15', 'EMP', '2017-02-01', '03:27:33', 'EMP'),
('PRO170102085428', '25% OFF JULY', 'Lorem ipsum', '560', 'Available', 'PRO170102085428.jpg', '2017-01-02', '08:54:28', 'EMP', '2017-02-01', '03:27:47', 'EMP'),
('promo1 ', 'promo 1', 'promos', '100', 'Not Available', 'promo1 .', '', '', '', '2017-02-01', '03:27:58', 'EMP');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `s_date` varchar(20) NOT NULL,
  `s_inputtime` varchar(20) NOT NULL,
  `s_sale` varchar(20) NOT NULL,
  `s_updatedate` varchar(20) NOT NULL,
  `s_updatetime` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`s_date`, `s_inputtime`, `s_sale`, `s_updatedate`, `s_updatetime`) VALUES
('2016-11-12', '0000000', '1200', '', ''),
('2016-11-13', '0000000', '1300', '', ''),
('2016-11-14', '0000000', '1100', '', ''),
('2016-11-15', '0000000', '1300', '', ''),
('2016-11-16', '0000000', '2100', '', ''),
('2016-11-17', '0000000', '2700', '', ''),
('2016-11-18', '0000000', '4000', '', ''),
('2016-11-19', '0000000', '3200', '', ''),
('2016-11-20', '0000000', '1300', '', ''),
('2016-11-21', '0000000', '2300', '', ''),
('2016-11-22', '0000000', '1200', '', ''),
('2016-11-23', '0000000', '5000', '', ''),
('2016-11-24', '0000000', '2000', '', ''),
('2016-11-25', '0000000', '3000', '', ''),
('2016-11-26', '0000000', '4300', '', ''),
('2016-11-27', '0000000', '3250', '', ''),
('2016-11-28', '00000000', '1250', '', ''),
('2016-11-29', '00000000', '3400', '', ''),
('2016-11-30', '00000000', '5000', '', ''),
('2016-12-01', '00000000', '7890', '', ''),
('2016-12-02', '00000000', '3562', '', ''),
('2016-12-03', '00000000', '1235', '', ''),
('2016-12-04', '00000000', '4560', '', ''),
('2016-12-05', '00000000', '1568', '', ''),
('2016-12-06', '00000000', '1250', '', ''),
('2016-12-07', '00000000', '4523', '', ''),
('2016-12-08', '00000000', '3414', '', ''),
('2016-12-09', '00000000', '4500', '', ''),
('2016-12-10', '00000000', '5100', '', ''),
('2016-12-11', '00000000', '2600', '', ''),
('2016-12-12', '00000000', '3400', '', ''),
('2016-12-13', '00000000', '1800', '', ''),
('2016-12-14', '00000000', '1990', '', ''),
('2016-12-15', '00000000', '1125', '', ''),
('2016-12-16', '00000000', '1356', '', ''),
('2016-12-17', '00000000', '5100', '', ''),
('2016-12-18', '06:37:33', '500', '', ''),
('2016-12-19', '00000000', '1400', '', ''),
('2016-12-20', '00000000', '7800', '', ''),
('2016-12-21', '00000000', '5990', '', ''),
('2016-12-22', '00000000', '2125', '', ''),
('2016-12-23', '00000000', '1256', '', ''),
('2016-12-26', '00000000', '2400', '', ''),
('2016-12-27', '00000000', '3800', '', ''),
('2016-12-28', '00000000', '1990', '', ''),
('2016-12-29', '00000000', '2425', '', ''),
('2016-12-30', '00000000', '4256', '', ''),
('2017-01-03', '00000000', '6400', '', ''),
('2017-01-04', '00000000', '7000', '', ''),
('2017-01-05', '00000000', '3890', '', ''),
('2017-01-06', '00000000', '5562', '', ''),
('2017-01-07', '00000000', '1235', '', ''),
('2017-01-08', '00000000', '1560', '', ''),
('2017-01-09', '00000000', '3568', '', ''),
('2017-01-10', '00000000', '2250', '', ''),
('2017-01-11', '00000000', '7523', '', ''),
('2017-01-12', '00000000', '9414', '', ''),
('2017-01-13', '00000000', '1500', '', ''),
('2017-01-14', '00000000', '4100', '', ''),
('2017-01-15', '00000000', '3600', '', ''),
('2017-01-16', '00000000', '2400', '', ''),
('2017-01-17', '00000000', '6800', '', ''),
('2017-01-18', '00000000', '9990', '', ''),
('2017-01-19', '00000000', '7125', '', ''),
('2017-01-20', '00000000', '2356', '', ''),
('2017-01-21', '00000000', '4100', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `s_id` varchar(500) NOT NULL,
  `s_name` varchar(150) NOT NULL,
  `s_description` varchar(500) NOT NULL,
  `s_category` varchar(100) NOT NULL,
  `s_price` varchar(20) NOT NULL,
  `s_picture` varchar(150) NOT NULL,
  `s_status` varchar(50) NOT NULL,
  `s_inputdate` varchar(20) NOT NULL,
  `s_inputtime` varchar(20) NOT NULL,
  `s_inputby` varchar(50) NOT NULL,
  `s_updatedate` varchar(20) NOT NULL,
  `s_updatetime` varchar(20) NOT NULL,
  `s_updateby` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`s_id`, `s_name`, `s_description`, `s_category`, `s_price`, `s_picture`, `s_status`, `s_inputdate`, `s_inputtime`, `s_inputby`, `s_updatedate`, `s_updatetime`, `s_updateby`) VALUES
('sampleid1', 'Layered', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id aliquet erat, at faucibus lorem. Phasellus congue finibus tincidunt. Maecenas non bibendum est, quis pellentesque mi. Donec dignissim elit augue, eu iaculis sapien interdum in. Vestibulum tellus lorem, elementum vitae tincidunt id, viverra eu dolor. Aenean odio quam, blandit at luctus sed, pretium quis nulla.', 'Long Hairstyle', '250', 'sampleid1.jpg', 'Available', '20161112', '120000', 'sample employee name', '2017-02-01', '04:09:22', ''),
('SER170117022745', 'Manicure', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id aliquet erat, at faucibus lorem. Phasellus congue finibus tincidunt. Maecenas non bibendum est, quis pellentesque mi. Donec dignissim elit augue, eu iaculis sapien interdum in. Vestibulum tellus lorem, elementum vitae tincidunt id, viverra eu dolor. Aenean odio quam, blandit at luctus sed, pretium quis nulla.', 'Nails', '100', 'SER170117022745.jpg', 'Available', '2017-01-17', '02:27:45', '', '2017-02-01', '04:17:27', ''),
('SER170117023134', 'Pedicure', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id aliquet erat, at faucibus lorem. Phasellus congue finibus tincidunt. Maecenas non bibendum est, quis pellentesque mi. Donec dignissim elit augue, eu iaculis sapien interdum in. Vestibulum tellus lorem, elementum vitae tincidunt id, viverra eu dolor. Aenean odio quam, blandit at luctus sed, pretium quis nulla.', 'Nails', '100', 'SER170117023134.jpg', 'Available', '2017-01-17', '02:31:34', '', '2017-02-01', '04:17:40', ''),
('SER170117023411', 'Oil Massage', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id aliquet erat, at faucibus lorem. Phasellus congue finibus tincidunt. Maecenas non bibendum est, quis pellentesque mi. Donec dignissim elit augue, eu iaculis sapien interdum in. Vestibulum tellus lorem, elementum vitae tincidunt id, viverra eu dolor. Aenean odio quam, blandit at luctus sed, pretium quis nulla.', 'Massage and Spa', '400', 'SER170117023411.jpg', 'Available', '2017-01-17', '02:34:11', '', '2017-02-01', '04:16:31', ''),
('SER170131032943', 'Tint (Root)', 'Whether you''re looking for a completely new look or just want to breathe fresh life into your current color, we''ve rounded up some of the best hair colors and ideas of the year.', 'Color', '500', 'SER170131032943.', 'Available', '2017-01-31', '03:29:43', '', '2017-02-06', '09:15:35', ''),
('SER170131033003', 'Tint (Full)', 'Whether you''re looking for a completely new look or just want to breathe fresh life into your current color, we''ve rounded up some of the best hair colors and ideas of the year.', 'Color', '400', '', 'Available', '2017-01-31', '03:30:03', '', '', '', ''),
('SER170131033039', 'Conditioning', 'Whether you''re looking for a completely new look or just want to breathe fresh life into your current color, we''ve rounded up some of the best hair colors and ideas of the year.', 'Color', '400', '', 'Available', '2017-01-31', '03:30:39', '', '', '', ''),
('SER170131033055', 'Highlights', 'Whether you''re looking for a completely new look or just want to breathe fresh life into your current color, we''ve rounded up some of the best hair colors and ideas of the year.', 'Color', '400', '', 'Available', '2017-01-31', '03:30:55', '', '', '', ''),
('SER170131033129', 'Hot Oil', 'Whether you''re looking for a completely new look or just want to breathe fresh life into your current color, we''ve rounded up some of the best hair colors and ideas of the year.', 'Treatment', '350', 'SER170131033129.jpg', 'Available', '2017-01-31', '03:31:29', '', '2017-02-01', '04:19:12', ''),
('SER170131033151', 'Hair Spa', 'Whether you''re looking for a completely new look or just want to breathe fresh life into your current color, we''ve rounded up some of the best hair colors and ideas of the year.', 'Treatment', '600', 'SER170131033151.jpg', 'Available', '2017-01-31', '03:31:51', '', '2017-02-01', '04:19:28', ''),
('SER170131033212', 'Powerdose', 'Whether you''re looking for a completely new look or just want to breathe fresh life into your current color, we''ve rounded up some of the best hair colors and ideas of the year.', 'Treatment', '350', 'SER170131033212.jpg', 'Available', '2017-01-31', '03:32:12', '', '2017-02-01', '04:19:41', ''),
('SER170131033243', 'Keratherpy', 'Whether you''re looking for a completely new look or just want to breathe fresh life into your current color, we''ve rounded up some of the best hair colors and ideas of the year.', 'Treatment', '250', 'SER170131033243.jpg', 'Available', '2017-01-31', '03:32:43', '', '2017-02-01', '04:19:52', ''),
('SER170131092414', 'Selena Gomez''s Satiny Strands', 'Sometimes you don''t feel like using a curling iron to shake up your bone-straight hair, so it''s best to just work with what you''ve got. We love Selena''s satiny-smooth strands here, that are super easy to recreate with a straightening iron (or just your natural texture) and a meticulous middle part. ', 'Long Hairstyle', '200', 'SER170131092414.jpg', 'Available', '2017-01-31', '09:24:14', '', '2017-02-01', '02:01:03', ''),
('SER170131092507', 'Rosie Huntington-Whiteley''s Feathery Ends', 'Phoning a friend: "How can we get bouncy ends like Rosie Huntington-Whiteley''s?" Our best guess is a round brush and some volumizing spray, plus blow-drying hair away from the face instead of curled in toward the chin. ', 'Long Hairstyle', '200', 'SER170131092507.jpg', 'Available', '2017-01-31', '09:25:07', '', '2017-02-01', '02:01:20', ''),
('SER170131092659', 'Poppy Delevingne''s Beachy Waves', 'Long strands = all the better to wear  in perfect beachy waves like these. Ugh, the Delevingne sisters are too #blessed in the hair department. ', 'Long Hairstyle', '200', 'SER170131092659.jpg', 'Available', '2017-01-31', '09:26:59', '', '2017-02-01', '02:01:32', ''),
('SER170131093741', 'Perm', 'We are giving you out the best hair form trends in the nation. Curly or straight, we''ll make sure that it will suit you taste on hair forms.', 'Form', '300', 'SER170131093741.jpg', 'Available', '2017-01-31', '09:37:41', '', '2017-02-01', '04:14:53', ''),
('SER170131093800', 'Hair Relax', 'We are giving you out the best hair form trends in the nation. Curly or straight, we''ll make sure that it will suit you taste on hair forms.', 'Form', '350', 'SER170131093800.jpg', 'Available', '2017-01-31', '09:38:00', '', '2017-02-01', '04:15:05', ''),
('SER170131093832', 'Hair Rebond', 'We are giving you out the best hair form trends in the nation. Curly or straight, we''ll make sure that it will suit you taste on hair forms.', 'Form', '250', 'SER170131093832.jpg', 'Available', '2017-01-31', '09:38:32', '', '2017-02-01', '04:15:12', ''),
('SER170131093916', 'Shampoo & Blowdrying', 'We are giving you out the best styles in the nation. We''ll make sure that it will suit you taste on hair forms.', 'Hair Styling', '150', 'SER170131093916.jpg', 'Available', '2017-01-31', '09:39:16', '', '2017-02-01', '04:15:33', ''),
('SER170131093940', 'Iron & Curling', 'We are giving you out the best styles in the nation. We''ll make sure that it will suit you taste on hair forms.', 'Hair Styling', '150', 'SER170131093940.jpg', 'Available', '2017-01-31', '09:39:40', '', '2017-02-01', '04:15:41', ''),
('SER170131094343', 'Nail Art', 'Nail art is a creative way to paint, decorate, enhance, and embellish the nails. It is a type of artwork that can be done on fingernails and toenails, usually after manicures or pedicures.', 'Nails', '100', 'SER170131094343.jpg', 'Available', '2017-01-31', '09:43:43', '', '2017-02-01', '04:18:44', ''),
('SER170131094742', 'Gel Polish', 'Nail art is a creative way to paint, decorate, enhance, and embellish the nails. It is a type of artwork that can be done on fingernails and toenails, usually after manicures or pedicures.', 'Nails', '100', 'SER170131094742.jpg', 'Available', '2017-01-31', '09:47:42', '', '2017-02-01', '04:19:00', ''),
('SER170131094913', 'Stone Massage', 'Massage therapy is manual manipulation of soft body tissues (muscle, connective tissue, tendons and ligaments) to enhance a person''s health and well-being. There are dozens of types of massage therapy methods (also called modalities).', 'Massage and Spa', '350', 'SER170131094913.jpg', 'Available', '2017-01-31', '09:49:13', '', '2017-02-01', '04:16:49', ''),
('SER170131094938', 'Ventosa Massage', 'Massage therapy is manual manipulation of soft body tissues (muscle, connective tissue, tendons and ligaments) to enhance a person''s health and well-being. There are dozens of types of massage therapy methods (also called modalities).', 'Massage and Spa', '400', 'SER170131094938.jpg', 'Available', '2017-01-31', '09:49:38', '', '2017-02-01', '04:16:59', ''),
('SER170131094955', 'Foot Spa', 'Massage therapy is manual manipulation of soft body tissues (muscle, connective tissue, tendons and ligaments) to enhance a person''s health and well-being. There are dozens of types of massage therapy methods (also called modalities).', 'Massage and Spa', '250', 'SER170131094955.jpg', 'Available', '2017-01-31', '09:49:55', '', '2017-02-01', '04:17:16', ''),
('SER170131095040', 'Eye Makeup', 'Cosmetics, also known as make-up, are substances or products used to enhance the appearance or fragrance of the body.', 'Make up', '150', 'SER170131095040.jpg', 'Available', '2017-01-31', '09:50:40', '', '2017-02-01', '04:15:49', ''),
('SER170131095058', 'Full Makeup', 'Cosmetics, also known as make-up, are substances or products used to enhance the appearance or fragrance of the body.', 'Make up', '400', 'SER170131095058.jpg', 'Available', '2017-01-31', '09:50:58', '', '2017-02-01', '04:16:10', ''),
('SER170131095557', 'Airbrush Makeup', 'Cosmetics, also known as make-up, are substances or products used to enhance the appearance or fragrance of the body.', 'Make up', '350', 'SER170131095557.jpg', 'Available', '2017-01-31', '09:55:57', '', '2017-02-01', '04:16:20', ''),
('SER170131095914', 'Julia Garner', 'Curly haired gals, we''ve got you: use a diffuser when you''re drying hair to boost volume at the rooms. Or, look for a half-inch curler if you want to create your own corkscrew curls.', 'Medium Cut Hairstyle', '250', 'SER170131095914.jpg', 'Available', '2017-01-31', '09:59:14', '', '2017-02-01', '02:01:46', ''),
('SER170131100018', 'Emma Stone', 'The Goldilocks length—not too long, not too short—hits right between the shoulders and the chin.', 'Medium Cut Hairstyle', '250', 'SER170131100018.jpg', 'Available', '2017-01-31', '10:00:18', '', '2017-02-01', '02:01:56', ''),
('SER170131100137', 'Ruth Negga', 'The flapper pin-curls of the ''20s are still just as glamorous today, as shown by Loving star Ruth Negga at the Golden Globes.  ', 'Short Hairstyle', '200', 'SER170131100137.jpg', 'Available', '2017-01-31', '10:01:37', '', '2017-02-01', '02:02:18', ''),
('SER170131100206', 'Michelle Williams', 'Platinum-blonde Michelle Williams might be our favorite version of Michelle Williams, like ever; here her signature crop is flat-ironed like a damn satin sheet and tucked behind both ears. Sweet and chic.', 'Short Hairstyle', '200', 'SER170131100206.jpg', 'Available', '2017-01-31', '10:02:06', '', '2017-02-01', '02:02:34', ''),
('SER170131101030', 'Classic Crew', 'It is hard to go wrong with a classic. For pure simplicity and style, there is the classic crew cut. A staple at any barber shop, this all-American look is the pure vintage style at its best.', 'Short Hairstyle', '200', 'SER170131101030.jpg', 'Available', '2017-01-31', '10:10:30', '', '2017-02-01', '02:03:38', ''),
('SER170131101119', 'High & Tight Men’s Hair Style', 'The high & tight works well on most face shapes and look good no matter if you are wearing a suit or relaxing in jeans and a T on the weekend.', 'Short Hairstyle', '200', 'SER170131101119.jpg', 'Available', '2017-01-31', '10:11:19', '', '2017-02-01', '02:03:15', ''),
('SER20161229061355', 'Undercut', 'The Undercut is one of the most popular styles of men’s hair on the scene today. You can do a lot of variations of the undercut, and it is great for reducing volume for those with thick hair.', 'Short Hairstyle', '200', 'SER20161229061355.jpg', 'Available', '2016-12-29', '06:13:55', '', '2017-02-01', '02:04:05', '');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `tr_id` varchar(500) NOT NULL,
  `tr_date` varchar(20) NOT NULL,
  `tr_time` varchar(20) NOT NULL,
  `tr_customer_id` varchar(1000) NOT NULL,
  `tr_customer_name` varchar(1000) NOT NULL,
  `tr_service_ids` varchar(1000) NOT NULL,
  `tr_service_names` varchar(1000) NOT NULL,
  `tr_employee_ids` varchar(1000) NOT NULL,
  `tr_employee_names` varchar(1000) NOT NULL,
  `tr_prices` varchar(1000) NOT NULL,
  `tr_total_price` varchar(10) NOT NULL,
  `tr_status` varchar(50) NOT NULL,
  `tr_checkin` varchar(20) NOT NULL,
  `tr_checkout` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`tr_id`, `tr_date`, `tr_time`, `tr_customer_id`, `tr_customer_name`, `tr_service_ids`, `tr_service_names`, `tr_employee_ids`, `tr_employee_names`, `tr_prices`, `tr_total_price`, `tr_status`, `tr_checkin`, `tr_checkout`) VALUES
('TR170203043732', '2017-02-06', '04:37:32', 'CUST170102102956', 'Zenric Rey Padua', 'sampleid1,SER170131033129,SER170131093741,SER170131093916,SER170131095040', 'Layered,Hot Oil,Perm,Shampoo & Blowdrying,Eye Makeup', '', '', '250,350,300,150,150', '1200', 'Paid', '2017-02-03 01:00', '2017-02-06 01:00'),
('TR170203044736', '2017-02-06', '04:47:36', 'CUST170102102956', 'Zenric Padua', 'sampleid1,SER170131033129,SER170131093741,SER170131093916,SER170131095040', 'Layered,Hot Oil,Perm,Shampoo & Blowdrying,Eye Makeup', '', '', '250,350,300,150,150', '1200', 'Reserved', '2017-02-03 01:00', ''),
('TR170203061930', '2017-02-03', '06:19:30', 'CUST170102102956', 'Zenric Rey Padua', 'SER170131033129', 'Hot Oil', '', '', '350', '350', 'Reserved', '2017-02-08 13:56', ''),
('TR170206090705', '2017-02-06', '09:07:05', 'CUST170102102956', 'Zenric Rey Padua', 'sampleid1', 'Layered', '', '', '250', '250', 'Reserved', '2017-02-06 09:00', ''),
('TR170206090737', '2017-02-06', '09:07:37', 'CUST170102102956', 'Zenric Rey Padua', 'sampleid1', 'Layered', '', '', '250', '250', 'Reserved', '2017-02-06 09:00', ''),
('TR2016121811170000', '2016-01-31', '11:17:0000', 'CUST170102103056', 'Joanne B. Bauto', 'SER170117023411', 'Oil Massage', 'EMP201612293456', 'Melchor G. Baltazar', '400', '400', 'Reserved', '', ''),
('TR2016121811190000', '2016-12-18', '11:19:0000', 'CUST170102102956', 'Zenric R. Padua', 'SER170117022745', 'Manicure\r\n', 'EMP170102094859', 'Ububwe O. Osas', '100', '100', 'Paid', '12:10:0000', '12:45:0000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`br_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`fb_id`);

--
-- Indexes for table `hair_colors`
--
ALTER TABLE `hair_colors`
  ADD PRIMARY KEY (`hc_id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`i_serial_no`);

--
-- Indexes for table `job_positions`
--
ALTER TABLE `job_positions`
  ADD PRIMARY KEY (`jp_id`);

--
-- Indexes for table `overall_inventory`
--
ALTER TABLE `overall_inventory`
  ADD PRIMARY KEY (`oi_id`);

--
-- Indexes for table `page_contents`
--
ALTER TABLE `page_contents`
  ADD PRIMARY KEY (`pc_id`);

--
-- Indexes for table `promos`
--
ALTER TABLE `promos`
  ADD PRIMARY KEY (`pr_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`s_date`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD UNIQUE KEY `tr_id` (`tr_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
